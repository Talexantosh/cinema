<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Admin\BottomSladerGallery;

class BottomSladerGalleryFactory extends Factory
{
     /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = BottomSladerGallery::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
            return [
                'image_location' => '',
                'url' => 'dist/img/logo.jpg',
                'text' => ''
             ];
    }
}
