<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('banners')->insert([
            'top_slader_width' => 1000,
            'top_slader_height' => 190,
            'top_slader_status' => 1,
            'top_slader_speed' => 0,
            'top_slader_count' => 5,
            'back_image_width' => 2000,
            'back_image_height' => 3000,
            'back_image_location' => null,
            'back_image_status' => 0,
            'bottom_slader_width' => 1000,
            'bottom_slader_height' => 190,
            'bottom_slader_status' => 1,
            'bottom_slader_speed' => 0,
            'bottom_slader_count' => 5
        ]);
    }
}
