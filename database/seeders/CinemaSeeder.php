<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\Admin\Text;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;

class CinemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++)
        {
            $name = Text::create();
            $description = Multitext::create();
            $conditions = Multitext::create();
            $seoblock = SeoBlock::create();

            DB::table('cinemas')->insert(
                [
                    'name' => $name->id,
                    'description' => $description->id,
                    'conditions' => $conditions->id,
                    'seoblock_id' => $seoblock->id
                ]
            );
        }
    }
}
