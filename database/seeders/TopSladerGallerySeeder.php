<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\TopSladerGallery;


class TopSladerGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       TopSladerGallery::factory()->count(5)->create();
    }
}
