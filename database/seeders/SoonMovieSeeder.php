<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SoonMovieSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 9; $i <= 12; $i++)
        {
            DB::table('soon_movies')->insert([
                'movie_id' => $i
            ]);
        }
    }
}
