<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Admin\BottomSladerGallery;

class BottomSladerGallerySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BottomSladerGallery::factory()->count(5)->create();
    }
}
