<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class HallSchemaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('cinema_halls')->orderBy('id')->each(function($hall){
            DB::table('cinema_halls')->where('id', $hall->id)
            ->update(
                [
                    'hall_schema' => json_encode(
                        [
                            '1' => array_fill(0, 10, 0),
                            '2' => array_fill(0, 16, 0),
                            '3' => array_fill(0, 16, 0),
                            '4' => array_fill(0, 16, 0), 
                            '5' => array_fill(0, 20, 0),
                        ])
                ]);
        });
    }
}
