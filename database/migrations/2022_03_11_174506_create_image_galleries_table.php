<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImageGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('image_galleries', function (Blueprint $table) {
            $table->id();
            $table->string('item_location_1')->default('');
            $table->string('item_url_1')->default('');
            $table->string('item_location_2')->default('');
            $table->string('item_url_2')->default('');
            $table->string('item_location_3')->default('');
            $table->string('item_url_3')->default('');
            $table->string('item_location_4')->default('');
            $table->string('item_url_4')->default('');
            $table->string('item_location_5')->default('');
            $table->string('item_url_5')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('image_galleries');
    }
}
