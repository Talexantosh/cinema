<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ad_pages', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('enabled')->default(0);
            $table->unsignedTinyInteger('system')->default(0);
            $table->unsignedInteger('cinema')->default(0);
            $table->unsignedInteger('name')->default(0);
            $table->unsignedInteger('description')->default(0);
            $table->string('main_image_location')->default('');
            $table->string('main_image_url')->default('');
            $table->unsignedInteger('address')->default(0);
            $table->unsignedInteger('gallery')->default(0);
            $table->unsignedInteger('seo_block')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ad_pages');
    }
}
