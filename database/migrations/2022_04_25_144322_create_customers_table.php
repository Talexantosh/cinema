<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->id();
            $table->string('firstname')->default('');
            $table->string('lastname')->default('');
            $table->string('login');
            $table->string('email')->unique();
            $table->string('address')->default('');
            $table->string('password');
            $table->string('card_number')->default('');
            $table->string('language')->default('ru');
            $table->string('gender')->default('');
            $table->date('dob')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
