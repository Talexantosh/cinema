<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMovieSchedulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movie_schedules', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('cinema_id')->default(0);
            $table->unsignedInteger('hall_id')->default(0);
            $table->unsignedInteger('movie_id')->default(0);
            $table->unsignedTinyInteger('offset_from_today')->default(0);
            $table->string('begin_time')->default('');
            $table->string('end_time')->default('');
            $table->string('price_ticket')->default('');
            $table->string('technology_movie')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movie_schedules');
    }
}
