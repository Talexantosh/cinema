<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('disabled')->default(1);
            $table->unsignedTinyInteger('lang')->default(0);
            $table->unsignedInteger('name')->default(0);
            $table->unsignedInteger('description')->default(0);
            $table->string('main_image')->default('');
            $table->string('main_image_url')->default('');
            $table->string('trailer_url')->default('');
            $table->unsignedTinyInteger('type_3d')->default(0);
            $table->unsignedTinyInteger('type_2d')->default(0);
            $table->unsignedTinyInteger('type_imax')->default(0);
            $table->unsignedInteger('seo_block')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
