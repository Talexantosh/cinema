<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCinemaHallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cinema_halls', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('cinema_id')->default(0);
            $table->unsignedInteger('name')->default(0);
            $table->unsignedInteger('description')->default(0);
            $table->string('schema_location')->default('');
            $table->string('schema_url')->default('');
            $table->string('img_location')->default('');
            $table->string('img_url')->default('');
            $table->unsignedInteger('seoblock_id')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cinema_halls');
    }
}
