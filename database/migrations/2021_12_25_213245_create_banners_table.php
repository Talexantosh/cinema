<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banners', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('top_slader_width');
            $table->unsignedSmallInteger('top_slader_height');
            $table->unsignedTinyInteger('top_slader_status');
            $table->unsignedTinyInteger('top_slader_speed');
            $table->unsignedTinyInteger('top_slader_count');
            $table->unsignedSmallInteger('back_image_width');
            $table->unsignedSmallInteger('back_image_height');
            $table->string('back_image_location');
            $table->string('back_image_url');
            $table->unsignedTinyInteger('back_image_status');
            $table->unsignedSmallInteger('bottom_slader_width');
            $table->unsignedSmallInteger('bottom_slader_height');
            $table->unsignedTinyInteger('bottom_slader_status');
            $table->unsignedTinyInteger('bottom_slader_speed');
            $table->unsignedTinyInteger('bottom_slader_count');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banners');
    }
}
