<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\DB;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('enabled')->default(0);
            $table->unsignedInteger('name')->default(0);
            $table->unsignedInteger('description')->default(0);
            $table->date('date_publish')->default(now());
            $table->string('main_image_location')->default('');
            $table->string('main_image_url')->default('');
            $table->string('video')->default('');
            $table->unsignedInteger('gallery')->default(0);
            $table->unsignedInteger('seo_block')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
    }
}
