<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddressCinemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('address_cinemas', function (Blueprint $table) {
            $table->id();
            $table->unsignedTinyInteger('enabled')->default(0);
            $table->unsignedInteger('name')->default(0);
            $table->unsignedInteger('description')->default(0);
            $table->string('main_image_location')->default('');
            $table->string('main_image_url')->default('');
            $table->string('cinema_logo_location')->default('');
            $table->string('cinema_logo_url')->default('');
            $table->string('address')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('address_cinemas');
    }
}
