<?php

namespace App\Mail;

use App\Models\Admin\Mailing;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class MailingList extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $content;

    /**
     * @var string
     */
    public $advance; 


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(?string $content, ?string $advance = null)
    {
        //
        $this->content = $content;
        $this->advance = $advance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('admin.mail.mail-template');
    }
}
