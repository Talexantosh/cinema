<?php

namespace App\Models\Helpers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\ViewErrorBag;
use Illuminate\Support\MessageBag;

class Helper{

    /**
     * 
     */
    public function __construct($error_msg = null, $key = 'dafault')
    {
        if ($error_msg != null)
            $this->add_error($error_msg, $key);
    }
    /*
    * Add an error to Laravel session $errors
    * @author Pavel Lint
    * @param string $key
    * @param string $error_msg
    */
    function add_error($error_msg, $key = 'default') {
        $errors = Session::get('errors', new ViewErrorBag);

        if (! $errors instanceof ViewErrorBag) {
            $errors = new ViewErrorBag;
        }

        $bag = $errors->getBags()['default'] ?? new MessageBag;
        $bag->add($key, $error_msg);

        Session::flash(
            'errors', $errors->put('default', $bag)
        );
    }

    /**
     * Function calculate $date1 - $date2 in days
     * @param string $date1 = "2016-07-31"
     * @param string $date2 = "now"
     * @return int
     */
    static public function dateDiff(string $date1, string $date2 = "today")
    {
        $date1_ts = strtotime($date1);
        $date2_ts = strtotime($date2);
        $diff = $date1_ts - $date2_ts;
        return (int)floor($diff / 86400);
    }
}