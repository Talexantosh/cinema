<?php

namespace App\Models;

use App\Models\Admin\MovieSchedule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Client\Request as ClientRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class Booking extends Model
{
    use HasFactory;

    protected $fillable = [
        'schedule_id',
        'booking_schema',
        'book_ticket',
        'enable'
    ];
    
    public static function bookTicket(Request $request, int $schedule)
    {
        $book = MovieSchedule::find($schedule)->beginBooking();
        $schema = json_decode($book->booking_schema, true);
        foreach($request->keys() as $key){
            if($key == '_token')
                continue;
            if (is_array($request->input($key))){
                foreach($request->input($key) as $place => $value){
                    $schema[$key][$place - 1] = Auth::guard('customer')->id();
                }  
            }  
        }
        $booking_schema = json_encode($schema);
        $book->booking_schema =  $booking_schema;
        $book->save();
    }

    public static function validateSchedule(Request $request, int $schedule)
    {
        $begin_time = optional(MovieSchedule::find($schedule))->begin_time;
        if (isset($request->user_id) && 
        Auth::guard('customer')->id() == $request->user_id &&
        strtotime($begin_time) - time() > 9000){
            return true;
        }
        return false;
    }
}
