<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class Customer extends Model
{
    use HasFactory;

    protected $fillable = [
        'firstname',
        'lastname',
        'login',
        'email',
        'address',
        'password',
        'card_number',
        'language',
        'gender',
        'dob',
        'phone',
        'city',
        'user_id'
    ];

    /**
     * 
     */
    public static function searchCustomers(string $search)
    {
        $searchResult = self::where('id', '=', $search)
                            ->orWhere('firstname', 'LIKE', "%{$search}%")
                            ->orWhere('email', 'LIKE', "%{$search}%")
                            ->orWhere('lastname', 'LIKE', "%{$search}%")
                            ->orWhere('login', 'LIKE', "%{$search}%")
                            ->orWhere('address', 'LIKE', "%{$search}%")
                            ->orWhere('card_number', 'LIKE', "%{$search}%")
                            ->orWhere('gender', 'LIKE', "%{$search}%")
                            ->orWhere('dob', 'LIKE', "%{$search}%")
                            ->orWhere('city', 'LIKE', "%{$search}%")
                            ->orWhere('phone', 'LIKE', "%{$search}%")
                            ->paginate(10);
        return $searchResult;
    }

    public static function saveCustomer(Request $request, int $id)
    {
        $model = self::find($id);
        foreach($request->keys() as $key){
            if (isset($model[$key])){
                if($key == 'password'){
                    if($request->input('password') != ''){
                        $model['password'] = Hash::make($request->input('password'));
                    }
                    continue;
                }
                $model[$key]= ($request->input($key))?: '';
            }
        }
        $model->save();

        return $model;        
    }
}
