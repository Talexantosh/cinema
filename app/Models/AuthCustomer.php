<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Auth;

class AuthCustomer extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'firstname', 
        'lastname', 
        'login', 
        'email', 
        'password', 
        'remember_token'
    ];

    public static function changeCustomer(int $customerId)
    {
        $customer = Customer::find($customerId);
        self::find($customer->user_id)->update(
            [
                'email' => $customer->email,
                'login' => $customer->login,
                'password' => $customer->password
            ]);
    }
}
