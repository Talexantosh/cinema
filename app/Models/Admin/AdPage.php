<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use PhpParser\Parser\Multiple;
use App\Traits\ActiveModel;

class AdPage extends Model
{
    use HasFactory,
        ActiveModel;

   protected $fillable = [
        'enabled',
        'name',
        'description',
        'main_image_location',
        'main_image_url',
        'gallery',
        'seo_block'
    ]; 

    /**
     * 
     */
    public function _name(){
        return $this->hasOne(Text::class, 'id', 'name');
    }

    /**
     * 
     */
    public function _description(){
        return $this->hasOne(WysiwygContent::class, 'id', 'description');
    }

    /**
     * 
     */
    public function _descriptionText(){
        return $this->hasOne(Multitext::class, 'id', 'description');
    }

    /**
     * 
     */
    public function seoblock(){
        return $this->hasOne(SeoBlock::class, 'id', 'seo_block');
    }

    /**
     * 
     */
    public function _gallery(){
        return $this->hasOne(ImageGallery::class, 'id', 'gallery');
    }

    /**
     * Используется модель кинотеатра, из которой
     * получаем главную картинку, название и логотип.
     * Отношение один к одному.
     * @todo Добавить в контроллер создания кинотеатра
     */
    public function _cinema(){
        return $this->hasOne(Cinema::class, 'id', 'cinema');
    }

    /**
     * Адрес - псевдоним для описания контакта
     * Устанавивает отношение один к одному 
     */
    public function _address(){
        return $this->hasOne(Multitext::class, 'id', 'address');
    }

    /**
     * Обработать изображения, чтобы src указывал на blob.
     * Как оказалось, это ни к чему. В БД сохраняются 
     * двоичные данные, не смотря на то что src указывает
     * на адрес blob:http://... 
     * @param string $htmlContent
     * @return string
     */
    public function handleImageDescription ($htmlContent){
        $result = preg_replace('/src\s*=\s*\"blob:http:\/\/[\d.:\/a-f-]*\"/mi', 'alt=""', $htmlContent);
        $result = preg_replace('/alt\s*=\s*\"data:image\//mi', 'src="data:image/', $result);
        return $result ?? $htmlContent;
    }

    /**
     * 
     */
    public static function saveMainAdPage(\Illuminate\Http\Request $request, int $id){

        try{
            $adPage = self::find($id);
 
            $adPage->enabled = ($request->input('isEnable') == 'on')? 1: 0;
            $adPage->main_image_location = $request->input('phoneOne', '');
            $adPage->main_image_url = $request->input('phoneTwo', '');
            $adPage->_description->content_rus = $request->input('seoRus', '');
            $adPage->_description->content_ua = $request->input('seoUa', '');
            $adPage->seoblock->url = $request->input('seoUrl', '');
            $adPage->seoblock->title = $request->input('seoTitle', '');
            $adPage->seoblock->keywords = $request->input('seoKeywords', '');
            $adPage->seoblock->description = $request->input('seoDescription','');
    
            $adPage->_description->save();
            $adPage->seoblock->save();
            $adPage->save();
            
        }
        catch(\Exception $e){
            throw $e;
        }

        return $adPage;
    }

    /**
     * 
     */
    public static function createAddress(){
        try {
            $record = self::create();
            $record->createText()
                    ->createMultitext()
                    ->createAddressContent()
                    ->createImageGallery();
            $record->cinema = 1; // признак адреса
            $record->save();
            return $record;
        }
        catch(\Exception $e){
            throw $e;
        }
        
    }

    public static function getAllAddresses()
    {
        $addresses = self::where('cinema', '=', 1)->get();

        return $addresses;
    }

}
