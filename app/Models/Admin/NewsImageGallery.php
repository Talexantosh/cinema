<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NewsImageGallery extends Model
{
    use HasFactory;
    /**
     * 
     */
    protected $guard = [];

    /**
     * 
     */
    protected $fillable = [
        'id',
        'item',
        'news_id',
        'image_location',
        'url',
        'updated_at'
    ];

    /**
     * 
     */
    public function news()
    {
        return $this->belongsTo(News::class, 'id', 'news_id');
    }

    /**
     * 
     */
    static public function createIfNotExist(int $newsId)
    {
        $gallery = NewsImageGallery::where('news_id', $newsId)->get();

        if (!count($gallery))
        {
            for ($i = 1; $i <= 5; $i++)
            {
                (new NewsImageGallery(
                    [
                        'item' => $i,
                        'news_id' => $newsId
                    ]
                ))->save();
            }

            return NewsImageGallery::where('news_id', $newsId)->get();
        }

        return $gallery;
    }
}
