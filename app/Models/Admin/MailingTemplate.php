<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class MailingTemplate extends Model
{
    use HasFactory;

    protected $fillable = [
        'file',
        'name',
        'content',
        'select'
    ];

    public static function addTemplateFile(Request $request)
    {
            $file = $request->file('file')->getClientOriginalName();
            $content = $request->file('file')->get();

            self::resetSelectTemplates();

            $model = self::create([
                'file' => $file,
                'name' => $file,
                'content' => $content,
                'select' => 1
            ]);

            return $model;
    }

    public static function resetSelectTemplates()
    {
        $models = self::where('select', '=', 1)->get();
        
        foreach ($models as $model){
             $model->select = 0;
             $model->save();
        }       
    }
}
