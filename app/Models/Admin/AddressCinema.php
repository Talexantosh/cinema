<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AddressCinema extends Model
{
    use HasFactory;

    protected $fillable = [
        'enabled',
        'name',
        'description',
        'main_image_location',
        'main_image_url',
        'cinema_logo_location',
        'cinema_logo_url',
        'address'
    ];
}
