<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Cinema extends Model
{
    use HasFactory;

    const CONDITION_SAMPLE = "<!-- HTML-блок для секции условий: -->\n<!-- Заголовок -->\n<h5 class=\"text-primary\"></h5>\n<!-- Абзац. -->\n<p class=\"text-secondary\"></p>";

    /**
     * 
     */
    protected $fillable = [
        'name',
        'description',
        'conditions',
        'logo_location',
        'logo_url',
        'img_location',
        'img_url',
        'seoblock_id'
    ];

    /**
     * 
     */
    public function _name()
    {
        return $this->hasOne(Text::class, 'id', 'name');
    }

    /**
     * 
     */
    public function _description()
    {
        return $this->hasOne(Multitext::class, 'id', 'description');
    }

    /**
     * 
     */
    public function _conditions()
    {
        return $this->hasOne(Multitext::class, 'id', 'conditions');
    }

    /**
     * 
     */
    public function gallery()
    {
        return $this->hasMany(CinemaImageGallery::class, 'cinema_id', 'id');
    }

    /**
     * 
     */
    public function halls()
    {
        return $this->hasMany(CinemaHall::class, 'cinema_id', 'id');
    }

    /**
     * 
     */
    public function seoblock()
    {
        return $this->hasOne(SeoBlock::class, 'id', 'seoblock_id');
    }

    /**
     * 
     */
    public function clearCinema()
    {
        
        $this->name->string_ua = '';
        $this->name->string_ua = '';
        $this->description->text_rus = '';
        $this->description->text_ua = '';
        $this->conditions->text_rus = '';
        $this->conditions->text_ua = '';
        $this->seoblock->url = '';
        $this->seoblock->title = '';
        $this->seoblock->keywords = '';
        $this->seoblock->description = '';
        Storage::delete($this->logo_location);
        $this->logo_location = '';
        $this->logo_url = '';
        Storage::delete($this->img_location);
        $this->img_location = '';
        $this->img_url = '';
        $images = $this->gallery;
        foreach( $images as $image){
            Storage::delete($image->image_location);
            $image->image_location = '';
            $image->url = '';
        }
        $this->halls->delete();

        $images->save();
        $this->name->save();
        $this->description->save();
        $this->conditions->save();
        $seoBlock = $this->seoblock->save();
        $this->seoblock_id = $seoBlock->id;
        $this->save();
    }

    /**
     * 
     */
    public function deleteCinema()
    {
        $this->name->delete();
        $this->description->delete();
        $this->conditions->delete();
        $this->seoblock->delete();
        Storage::delete($this->logo_location);
        Storage::delete($this->img_location);
        $images = $this->gallery;
        foreach( $images as $image){
            Storage::delete($image->image_location);
        }
        $images->delete();
        $this->halls->delete();
        $this->delete();
    }
}
