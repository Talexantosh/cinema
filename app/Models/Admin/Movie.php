<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;

class Movie extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'lang',
        'name',
        'description',
        'main_image',
        'main_image_url',
        'trailer_url',
        'type_3d',
        'type_2d',
        'type_imax',
        'seo_block'
    ];

    //protected $guard = [];

    public function seoblock()
    {
        return $this->hasOne(SeoBlock::class, 'id', 'seo_block');
    }

    public function text()
    {
        return $this->hasOne(Text::class, 'id', 'name');
    }

    public function multitext()
    {
        return $this->hasOne(Multitext::class, 'id', 'description');
    }

    public function gallery()
    {
        return $this->hasMany(MovieImageGallery::class, 'movie_id');
    }

    public function currentMovies()
    {
        return $this->hasOne(CurrentMovie::class, 'movie_id', 'id');
    }

    public function soonMovies()
    {
        return $this->hasOne(SoonMovie::class, 'movie_id', 'id');
    }

    /**
     * 
     */
    public function parseDescriptionRus(){
        $rus = [
            'main' => [],
            'additional' => [],
            'release' => []
        ];
        try{
            $text = $this->multitext->text_rus;
            $rus = parse_ini_string($text, true);
        }
        catch(\Exception $e){
            Log::error('Ошибка при парсинге text_rus описания фильма: ' . $e->getMessage());
        }
        return $rus;
    }

     /**
     * 
     */
    public function parseDescriptionUa(){
        $ua = [
            'main' => [],
            'additional' => [],
            'release' => []
        ];
        try{
            $text = $this->multitext->text_ua;
            $ua = parse_ini_string($text, true);
        }
        catch(\Exception $e){
            Log::error('Ошибка при парсинге text_ua описания фильма: ' . $e->getMessage());
        }
        return $ua;
    }
}
