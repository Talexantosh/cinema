<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImageGallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'item_location_1',
        'item_url_1',
        'item_location_2',
        'item_url_2',
        'item_location_3',
        'item_url_3',
        'item_location_4',
        'item_url_4',
        'item_location_5',
        'item_url_5'
    ];
}
