<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HallImageGallery extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $guard = [];

    protected $fillable = [
        'id',
        'item',
        'hall_id',
        'image_location',
        'url',
        'updated_at'
    ];

    /**
     * 
     */
    public function hall()
    {
        return $this->belongsTo(CinemaHall::class, 'id', 'hall_id');
    }

    /**
     * 
     */
    static public function createIfNotExist(int $hallId)
    {
        $gallery = HallImageGallery::where('hall_id', $hallId)->get();

        if (!count($gallery))
        {
            for ($i = 1; $i <= 5; $i++)
            {
                (new HallImageGallery(
                    [
                        'item' => $i,
                        'hall_id' => $hallId
                    ]
                ))->save();
            }

            return HallImageGallery::where('hall_id', $hallId)->get();
        }

        return $gallery;
    }
}
