<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovieImageGallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'id',
        'item',
        'movie_id',
        'image_location',
        'url'
    ];

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'id');
    }
}
