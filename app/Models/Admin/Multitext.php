<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Multitext extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * 
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'description', 'id');
    }

    /**
     * 
     */
    public function cinemaDescription()
    {
        return $this->belongsTo(Cinema::class, 'description', 'id');
    }

    /**
     * 
     */
    public function hall()
    {
        return $this->belongsTo(CinemaHall::class, 'description', 'id');
    }

    /**
     * 
     */
    public function cinemaConditions()
    {
        return $this->belongsTo(Cinema::class, 'conditions', 'id');
    }
}
