<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WysiwygContent extends Model
{
    use HasFactory;

    protected $fillable = [
        'content_rus',
        'content_ua'
    ];
}
