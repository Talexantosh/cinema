<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Text extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * 
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'name', 'id');
    }

    /**
     * 
     */
    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'name', 'id');
    }


    /**
     * 
     */
    public function hall()
    {
        return $this->belongsTo(CinemaHall::class, 'name', 'id');
    }
}
