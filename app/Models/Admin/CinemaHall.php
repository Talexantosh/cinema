<?php

namespace App\Models\Admin;

use GrahamCampbell\ResultType\Result;
use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use illuminate\Support\Facades\Log;
use Illuminate\Http\Request as HttpRequest;
use App\Models\Admin\MovieSchedule;

class CinemaHall extends Model
{
    use HasFactory;

    const HALL_DESCRIPTION_SAMPLE_UA = ';Секция описания, при добавлении новой - увеличить на единицу
[content1]
title = "..." ;Заголовок
paragraph[ ] = "...";Абзац. Повторить для каждого абзаца';
    
    const HALL_DESCRIPTION_SAMPLE_RUS = ';Секция описания, при добавлении новой - увеличить на единицу
[content1]
title = "..." ;Заголовок
paragraph[ ] = "...";Абзац. Повторить для каждого абзаца
; Расписание сеансов
[0]
; сегодня
;Первый сеанс
fir[time] = "";Время 14:00
fir[price] = "";Цена билета 75 грн.
fir[movie] = "";Id фильма
fir[type] = "";Тип 2D 3D IMAX
;Второй сеанс
sec[time] = ""
sec[price] = ""
sec[movie] = ""
sec[type] = ""
;Третий сеанс
thi[time] = ""
thi[price] = ""
thi[movie] = ""
thi[type] = ""
;Можно продолжить...
[1]
; + 1 день
fir[time] = ""
fir[price] = ""
fir[movie] = ""
fir[type] = ""
sec[time] = ""
sec[price] = ""
sec[movie] = ""
sec[type] = ""
thi[time] = ""
thi[price] = ""
thi[movie] = ""
thi[type] = ""
[2]
; + 2 дня
fir[time] = ""
fir[price] = ""
fir[movie] = ""
fir[type] = ""
sec[time] = ""
sec[price] = ""
sec[movie] = ""
sec[type] = ""
thi[time] = ""
thi[price] = ""
thi[movie] = ""
thi[type] = ""
[3]
; + 3 дня
fir[time] = ""
fir[price] = ""
fir[movie] = ""
fir[type] = ""
sec[time] = ""
sec[price] =""
sec[movie] = ""
sec[type] = ""
thi[time] = ""
thi[price] = ""
thi[movie] = ""
thi[type] = ""
;Остальное по аналогии';

    /**
     * 
     */
    protected $fillable = [
        'name',
        'description',
        'seoblock_id',
        'cinema_id',
        'hall_schema' 
    ];

    /**
     * 
     */
    public function _name()
    {
        return $this->hasOne(Text::class, 'id', 'name');
    }

    /**
     * 
     */
    public function _description()
    {
        return $this->hasOne(Multitext::class, 'id', 'description');
    }

    /**
     * 
     */
    public function gallery()
    {
        return $this->hasMany(HallImageGallery::class, 'hall_id', 'id');
    }

    /**
     * 
     */
    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'cinema_id', 'id');
    }

    /**
     * 
     */
    public function seoblock()
    {
        return $this->hasOne(SeoBlock::class, 'id', 'seoblock_id');
    }

    /**
     * @return string[]
     */
    public function getDescriptions()
    {
        $result["rus"]["content1"]['h'] = [];
        $result["rus"]["content1"]['p'] = [];
        $result["ua"]["content1"]['h'] = [];
        $result["ua"]["content1"]['p'] = [];

        $rawString = $this->_description->text_rus;
        $rawResult = parse_ini_string($rawString, true);
        if (!$rawResult)
            throw new \Exception('Ошибка при парсинге описания');
        $index = 1;
        foreach($rawResult as $key => $elem)
        {
            if ( $key === "content$index"){
                $result["rus"]["content$index"]["h"] = (array_key_exists("title", $elem))? $elem["title"]: "";
                $result["rus"]["content$index"]["p"] = (array_key_exists("paragraph", $elem))? $elem["paragraph"]: "";
                $index++;
            }
        }

        $rawString = $this->_description->text_ua;
        $rawResult = parse_ini_string($rawString, true);
        if (!$rawResult)
            throw new \Exception('Ошибка при парсинге описания');
        $index = 1;
        foreach($rawResult as $key => $elem)
        {
            if ( $key === "content$index"){
                $result["ua"]["content$index"]["h"] = (array_key_exists("title", $elem))? $elem["title"]: "";
                $result["ua"]["content$index"]["p"] = (array_key_exists("paragraph", $elem))? $elem["paragraph"]: "";
                $index++;
            }
        }
        
        return $result;
    }

    /**
     * @return string[]
     */
    public  function populateShowtime()
    {
        $rawString = $this->_description->text_rus;
        $result = [];

        $rawResult = parse_ini_string($rawString, true);

        if(!$rawResult)
            throw new \Exception('Ошибка при парсинге');
           
        $index = 0;
        foreach($rawResult[0] as $row)
        {
            $movieNameUa = $movieNameRus = "???";
            $movie = Movie::find($row["movie"]);
            if ($movie){
                $movieNameRus = $movie->text->string_rus;
                $movieNameUa = $movie->text->string_ua;
            }
            
            $result[$index]["time"] = $row["time"];
            $result[$index]["nameRus"] = $movieNameRus;
            $result[$index]["nameUa"] = $movieNameUa;
            $result[$index]["movie"] = $row["movie"];
            $result[$index]["price"] = $row["price"];
            $result[$index]['type'] = (array_key_exists('type', $row) && $row['type'] != '')? $row['type']: '2D';
            
            $index++;
        }

        return $result;
    }

    /**
     * @return array &$array
     * 
     */
    public function mergeSearchMovieInSchedule(array &$array, int $movieId, string $date, int $type2D, int $type3D, int $typeImax)
    {
        $rawString = $this->_description->text_rus;
        $result = [];

        $rawResult = parse_ini_string($rawString, true);

        if(!$rawResult){
            Log::error('Ошибка при парсинге расписания');
            return;
        }
        
        try{
            $searchDay = $this->dateDiff($date);
        }
        catch(\Exception $e){
            Log::error("Ошибка при поиске даты при поиске фильма: " . $e->getMessage());
            return;
        }

        if (!array_key_exists($searchDay, $rawResult))
            return;

        foreach($rawResult[$searchDay] as $row)
        {
            if ($row["movie"] == $movieId){
                // Сегодня сеанс уже окончен
                 if (!$searchDay && (strtotime("now") > strtotime($row["time"])))
                    continue; 

                if(!array_key_exists('type', $row)){
                    if( $type2D){
                        $array[] = $this->getData($movieId, $row);
                        continue;
                    }
                    continue;
                }                                            
                    
                if ($type3D && "3D" == strtoupper($row["type"])){
                    $array[] = $this->getData($movieId, $row);     
                }
                if ($typeImax && "IMAX" == strtoupper($row["type"])){
                    $array[] = $this->getData($movieId, $row);     
                }
                if ($type2D && "2D" == strtoupper($row["type"])){
                    $array[] = $this->getData($movieId, $row);     
                } 
            }
        }
    }

    /**
     * Function calculate $date1 - $date2 in days
     * @param string $date1 = "2016-07-31"
     * @param string $date2 = "now"
     * @return int
     */
    public function dateDiff(string $date1, string $date2 = "today")
    {
        $date1_ts = strtotime($date1);
        $date2_ts = strtotime($date2);
        $diff = $date1_ts - $date2_ts;
        return (int)floor($diff / 86400);
    }

    /**
     * @return array
     */
    protected function getData($movieId, $array, $offsetToday = 0)
    {
        $result = [];
        $obj = Movie::find($movieId);
        
        $result['offsetToday'] = $offsetToday;
        $result['cinemaId'] = $this->cinema->id;
        $result['cinemaRus'] = $this->cinema->_name->string_rus;
        $result['cinemaUa'] = $this->cinema->_name->string_ua;
        $result['hallId'] = $this->id;
        $result['hallRus'] = $this->_name->string_rus;
        $result['hallUa'] = $this->_name->string_ua;
        $result['movieId'] = $movieId;
        if($obj){
            $result["movieRus"] = $obj->text->string_rus;
            $result["movieUa"] = $obj->text->string_ua;
        }
        else{
            $result["movieRus"] = '???';
            $result["movieUa"] = '???';
        }
        $result["time"] = $array["time"];
        $result["price"] = $array["price"];
        $result["type"] = (array_key_exists('type', $array) && $array["type"] != '')? $array['type']: '2D';
        
        return $result;
    }

    /**
     * 
     */
    public function saveHall(HttpRequest $request)
    {
        $this->_name->string_rus = $request->input('nameRus', '');
        $this->_name->string_ua = $request->input('nameUa', '');
        $this->_description->text_rus = $request->input('descriptionRus', '');
        $this->_description->text_ua = $request->input('descriptionUa', '');
        $this->seoblock->url = $request->input('seoUrl', '');
        $this->seoblock->title = $request->input('seoTitle', '');
        $this->seoblock->keywords = $request->input('seoKeywords', '');
        $this->seoblock->description = $request->input('seoDescription','');

        $this->saveSchedule($request);

        $this->_name->save();
        $this->_description->save();
        $seoBlock = $this->seoblock->save();
        $this->save();
    }

    /**
     * @return array
     */
    protected function parse_schedule (string $description)
    {
        $result = [];

        $rawResult = parse_ini_string($description, true);

        if(!$rawResult){
            throw new \Exception('Ошибка при парсинге расписания');
        }

        for ($i = 0; $i < 7; $i++){
            if (!array_key_exists($i, $rawResult))
                continue;
                foreach($rawResult[$i] as $row)
                    $result[] = $this->getData($row['movie'], $row, $i);
        }
        
        return $result;
    }

    /**
     * @return bool
     */
    public function saveSchedule (HttpRequest $request)
    {
        $dataSchedule = $this->parse_schedule($request->input('descriptionRus'));

        foreach ($dataSchedule as $data){
            if (!count($data))
                continue;
            $schedule = MovieSchedule::firstOrCreate(
                [
                    'hall_id' => $this->id,
                    'offset_from_today' => $data['offsetToday'],
                    'begin_time' => $data["time"],
                ]
            );

            $schedule->update([
                'cinema_id' => $data['cinemaId'],
                'movie_id' => $data['movieId'],
                'price_ticket' => $data["price"],
                'technology_movie' => $data["type"]
            ]);
        }

        return true; 
    }
}
