<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class Promotion extends Model
{
    use HasFactory;

    /**
     * 
     */
    const DESCRIPTION_TEMPLATE = '
;Причастные организации
org[ ] = "";Массив. Повторить для отображения перечня организаций
; Само описание акции
par[] = "";Параграф. Можно использовать html';

    protected $fillable = [
        'enabled',
        'name',
        'description',
        'date_publish',
        'main_image',
        'video',
        'gallery',
        'seo_block'
    ];

    /**
     * 
     */
    public function _name(){
        return $this->hasOne(Text::class, 'id', 'name');
    }

    /**
     * 
     */
    public function _description(){
        return $this->hasOne(Multitext::class, 'id', 'description');
    }

    /**
     * 
     */
    public function seoblock(){
        return $this->hasOne(SeoBlock::class, 'id', 'seo_block');
    }

    /**
     * 
     */
    public function _gallery(){
        return $this->hasOne(ImageGallery::class, 'id', 'gallery');
    }

    /**
     * @param string $description
     * @throws \Exception
     */
    public function validateDescription(string $description){
        
        $textRus = $this->_description->text_rus;
        $textUa = $this->_description->text_ua;

        $resultRus = parse_ini_string($textRus);
        $resultUa = parse_ini_string($textUa);
            
        if (!$resultRus || !$resultUa)
            throw new \Exception('Parse ini string return false');
    }

    /**
     * Вернуть массив из поля description после 
     * парсинга содержимого ini-строки
     * перечнем организаций поддержавших акцию. 
     * @return string[]
     */
    public function getOrgs(){

        $result = [
            'rus' => [],
            'ua' => []
        ];

        try{
            $textRus = $this->_description->text_rus;
            $textUa = $this->_description->text_ua;

            $resultRus = parse_ini_string($textRus);
            $resultUa = parse_ini_string($textUa);

            $result['rus'] = (array_key_exists('org', $resultRus))? $resultRus['org']: [];
            $result['ua'] = (array_key_exists('org', $resultUa))? $resultUa['org']: [];
        }
        catch(\Exception $e){
            Log::error('Ошибка при парсинге описания причастных к акции компаний: ' . $e->getMessage());
        }

        return $result;
    }

    /**
     * Вернуть массив параграфов из поля description
     * после парсинга содержимого ini-строки
     * @return string[]
     */
    public function getDescriptionContent(){
        
        $result = [
            'rus' => [],
            'ua' => []
        ];

        try{
            $textRus = $this->_description->text_rus;
            $textUa = $this->_description->text_ua;

            $resultRus = parse_ini_string($textRus);
            $resultUa = parse_ini_string($textUa);

            $result['rus'] = (array_key_exists('par', $resultRus))? $resultRus['par']: [];
            $result['ua'] = (array_key_exists('par', $resultUa))? $resultUa['par']: [];
        }
        catch(\Exception $e){
            Log::error('Ошибка при парсинге описания акции: ' . $e->getMessage());
        }

        return $result;
    }
}
