<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MailingCustomer extends Model
{
    use HasFactory;

    protected $fillable = [
        'customer',
        'progress'
    ];

    public static function deleteAll()
    {
        foreach(self::all() as $item){
            $item->delete();
        }
    }

    public static function createCustomers(array $customers)
    {
        foreach($customers as $customer => $value){
            self::create(
                [
                    'customer' => $value
                ]
                );
        }
        return self::all();
    }

}
