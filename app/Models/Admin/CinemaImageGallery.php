<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CinemaImageGallery extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $guard = [];

    protected $fillable = [
        'id',
        'item',
        'cinema_id',
        'image_location',
        'url',
        'updated_at'
    ];

    /**
     * 
     */
    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'id', 'cinema_id');
    }

    /**
     * 
     */
    static public function createIfNotExist(int $cinemaId)
    {
        $gallery = CinemaImageGallery::where('cinema_id', $cinemaId)->get();

        if (!count($gallery))
        {
            for ($i = 1; $i <= 5; $i++)
            {
                (new CinemaImageGallery(
                    [
                        'item' => $i,
                        'cinema_id' => $cinemaId
                    ]
                ))->save();
            }

            return CinemaImageGallery::where('cinema_id', $cinemaId)->get();
        }

        return $gallery;
    }
}
