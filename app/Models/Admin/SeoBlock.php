<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeoBlock extends Model
{
    use HasFactory;

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
   
    /**
     * 
     */
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'seo_block', 'id');
    }

    /**
     * 
     */
    public function cinema()
    {
        return $this->belongsTo(Cinema::class, 'seoblock_id', 'id');
    }

    /**
     * 
     */
    public function hall()
    {
        return $this->belongsTo(CinemaHall::class, 'seoblock_id', 'id');
    }
}
