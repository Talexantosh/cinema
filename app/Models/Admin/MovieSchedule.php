<?php

namespace App\Models\Admin;

use App\Models\Booking;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Helpers\Helper;
use Illuminate\Database\Eloquent\Relations\HasOne;

class MovieSchedule extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $fillable = [
        'cinema_id',
        'hall_id',
        'movie_id',
        'offset_from_today',
        'begin_time',
        'end_time',
        'price_ticket',
        'technology_movie'
    ];

    /**
     *
     */
    static public function findMovieSchedule(string $cinema = "all", 
        string $beginDate = "today", 
        string $endDate = 'today',
        string $movie = "all",
        string $hall = "all",
        string $type2D = '1',
        string $type3D = '0',
        string $typeImax = '0'
    )
    {
        $query = [];
        if ($cinema !== "all")
            $query[] = ['cinema_id', '=', $cinema];
        if ($movie !== "all")
            $query[] = ['movie_id', '=', $movie];
        if ($hall !== "all")
            $query[] = ['hall_id', '=', $hall];

        $sql =static::select('*')->
        where($query)->
        whereBetween('offset_from_today', 
            [Helper::dateDiff($beginDate), 
            Helper::dateDiff($endDate)])->
        where(function($query) use ($type2D, $type3D, $typeImax) {
            $query->where('technology_movie', '=', ($type2D === '1')? '2D': '')->
            orWhere('technology_movie', '=', ($type3D === '1')? '3D': '')->
            orWhere('technology_movie', '=', ($typeImax === '1')? 'IMAX': '');
        })->
        groupBy('offset_from_today')->toSql();

        $result = static::select('*')->
            where($query)->
            whereBetween('offset_from_today', 
                [Helper::dateDiff($beginDate), 
                Helper::dateDiff($endDate)])->
            where(function($query) use ($type2D, $type3D, $typeImax) {
                $query->where('technology_movie', '=', ($type2D === '1')? '2D': '')->
                orWhere('technology_movie', '=', ($type3D === '1')? '3D': '')->
                orWhere('technology_movie', '=', ($typeImax === '1')? 'IMAX': '');
            })->
            /*groupBy('offset_from_today')->*/
            orderBy('offset_from_today')->get();

        return $result;
    }

    public function _movie()
    {
        return optional(Movie::find($this->movie_id))->text;
    }

    public function _linkMovie()
    {
        return $this->hasOne(Movie::class, 'id',  'movie_id');
    }

    public function _hall()
    {
        return optional(CinemaHall::find($this->hall_id))->_name;
    }

    public function _schema()
    {
        return $this->hasOne(CinemaHall::class, 'id', 'hall_id');
    }

    public function beginBooking()
    {
        $bookings= Booking::where('schedule_id', $this->id)
            ->where('enable', 1)->get();

        foreach ($bookings as $booking){
            if($booking->enable)
                if(strtotime('now') - strtotime($booking->created_at) >= 43200){
                    $booking->update(
                        [
                            'enable' => 0
                        ]
                        );
                    $result = Booking::create(
                        [
                            'schedule_id' => $this->id,
                            'booking_schema' => $this->_schema->hall_schema,
                        ]
                            );
                    }
                    else
                        $result = $booking;
        }

        $sec = strtotime('now') - strtotime($booking->created_at);

        if(!isset($result)){
             $result = Booking::firstOrCreate(
                [
                    'schedule_id' => $this->id,
                ],
                [
                    'booking_schema' => $this->_schema->hall_schema,
                ]
            );
        }
       
        return $result;
    }
}
