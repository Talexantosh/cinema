<?php

namespace App\Models\Admin;

use App\Models\Customer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Mailing extends Model
{
    use HasFactory;

    protected $fillable = [
        'selectively',
        'template',
        'status',
        'progress',
        'qty'
    ];
    public function _template()
    {
        return $this->hasOne(MailingTemplate::class, 'id', 'template');
    }

    public static function setSelectively(bool $on)
    {
        self::firstOrCreate(
            [
                'id' => 1
            ],
            [
                'selectively' => (int) $on
            ]);        
    }

    public static function premailing(Request $request)
    {
        $keys = $request->except('_token');
        $mailing =  self::find(1);
        foreach($keys as $key => $value){
           $mailing[$key] = $value;
        }
        $mailing->progress = 0;
        $mailing->qty = 0;
        $mailing->save();

        return $mailing;
    }

    public static function getRecipientsMail()
    {
            $mailing = Mailing::find(1);
            if (!isset($mailing->selectively))
                return null;
            if ($mailing->selectively){
                $customers = MailingCustomer::all('customer')->toArray();
                $recipients = Customer::whereIn('id', $customers)->get();
            }
            else {
                $recipients = Customer::all();
            }
            return $recipients;
    }
}
