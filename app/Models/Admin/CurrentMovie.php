<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CurrentMovie extends Model
{
    use HasFactory;

    /**
     * 
     */
    protected $fillable = [
        'movie_id'
    ];
    
    /**
     * 
     */
    public function _movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id' );
    }
}
