<?php

namespace App\Listeners;

use App\Models\Admin\Mailing;
use App\Models\Admin\MailingCustomer;
use App\Models\Customer;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Events\MessageSent;
use Illuminate\Queue\InteractsWithQueue;

class SentCustomerEmail implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \Illuminate\Mail\Events\MessageSent  $event
     * @return void
     */
    public function handle(MessageSent $event)
    {
        //
        $mailing = Mailing::find(1);
        if ($mailing->selectively){
            $qty = MailingCustomer::all()->count();
        }
        else{
            $qty = Customer::all()->count();
        }
        $mailing->status = 1;
        $mailing->progress += 100 / $qty;
        $mailing->qty += 1;
        if ($mailing->qty >= $qty)
            $mailing->status = 0;
        $mailing->save();

    }
}
