<?php

namespace App\Jobs;

use App\Mail\MailingList;
use App\Models\Admin\Mailing;
use App\Models\Admin\MailingCustomer;
use App\Models\Customer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SendMailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 2;

    protected $recipient;

    protected $content;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($recipient, $content)
    {
        //
        $this->recipient = $recipient;
        $this->content = $content;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        Mail::to($this->recipient)->send(new MailingList($this->content)); 
    }
}
