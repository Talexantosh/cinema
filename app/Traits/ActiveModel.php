<?php
    namespace App\Traits;

    use App\Models\Admin\Text;
    use App\Models\Admin\WysiwygContent;
    use App\Models\Admin\ImageGallery;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Storage;

    trait ActiveModel
    {
        /**
         *  Delete a row from model with all relations
         * @param int $id
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception
         */
        static public function deleteRecord(int $id)
        {
            $record = static::find($id);
        
            try{
                if (isset($record->system) && $record->system)
                    throw new \Exception('The system page cannot be deleted!');
                empty($record->name)?: optional($record->_name)->delete();
                try{
                    empty($record->description)?: optional($record->_description)->delete();
                }
                catch(\Exception $e){
                    $record->_descriptionText->delete();
                }
                empty($record->address)?: optional($record->_address)->delete();

                if (!empty($record->gallery)){
                    $images = $record->_gallery;

                    try{
                        Storage::delete($images["item_location_1"]);
                        Storage::delete($images["item_location_2"]);
                    }
                    catch (\Exception $e){/** @todo */}
                }
                empty($record->gallery)?: optional($record->_gallery)->delete();
                empty($record->seo_block)?: optional($record->seoblock)->delete();
                $record->delete(); 
                return $record;
            }
            catch(\Exception $e){
                throw $e;
            }  
        }
        

        /**
         * Creste a row from model with all relations
         * @param void
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception 
         */
        static public function createRecord(){

            try{
                $record = static::create();

                $record->createText()
                ->createWysiwygContent()
                ->createImageGallery()
                ->createSeoBlock()
                ->save();
                
                return $record;
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        protected function createText(){
            $name = Text::create();
            $this->name = $name->id;
            return $this;
        }

        protected function createMultitext(){
            $description = Multitext::create();
            $this->description = $description->id;
            return $this;
        }

        protected function createAddressContent(){
            $address = Multitext::create();
            $this->address = $address->id;
            return $this;
        }

        protected function createWysiwygContent(){
            $description = WysiwygContent::create();
            $this->description = $description->id;
            return $this;
        }    

        protected function createImageGallery(){
            $gallery = ImageGallery::create();
            $this->gallery = $gallery->id;
            return $this;
        }
            
        protected function createSeoBlock(){
            $seoblock = SeoBlock::create();
            $this->seo_block = $seoblock->id;
            return $this;
        }

        /**
         * Save a row in a model with all relations
         * @param Illuminet\Http\Request $request
         * @param int $id
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception 
         */
        static public function saveRecord(Request $request, int $id){

            try{
                $record = static::find($id);
                    
                $record->enabled = ($request->input('isEnable') == 'on')? 1: 0;
                $record->_name->string_rus = $request->input('nameRus', '');
                $record->_name->string_ua = $request->input('nameUa', '');
                $record->_description->content_rus = $request->input('descriptionRus', '');//$descriptionRus;
                $record->_description->content_ua = $request->input('descriptionUa', '');//$descriptionUa;
                $record->seoblock->url = $request->input('seoUrl', '');
                $record->seoblock->title = $request->input('seoTitle', '');
                $record->seoblock->keywords = $request->input('seoKeywords', '');
                $record->seoblock->description = $request->input('seoDescription','');
        
                $record->_name->save();
                $record->_description->save();
                $record->seoblock->save();
                $record->save();

                return $record;
                
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        /**
         * Delete image element from a gallery model
         * @param int $id
         * @param int $item
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception  
         */
        public static function deleteGalleryItem(int $id, int $item){

            $images = static::find($id)->_gallery;

            try{
                Storage::delete($images["item_location_$item"]);

                $images["item_location_$item"] = '';
                $images["item_url_$item"] = '';
                $images->save();
                return $images;
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        /**
         * Save image element in a gallery model
         * @param Illuminate\Http\Request $request
         * @param int $id
         * @param int $item
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception   
         */
        public static function storeGalleryItem(Request $request, int $id, int $item){
        
            $validatedData = $request->validate([
                'file' => 'required|image',
               ]);

            try{
                $name = $request->file('file')->getClientOriginalName();
        
                $path = $request->file('file')->storeAs(
                    'public/files', $name);
        
                $stores = static::find($id)->_gallery;
                $stores["item_location_$item"] =  $path;
                $stores["item_url_$item"] = asset('storage/files/' . $name);
                $stores->save(); 
                return $stores;
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        /**
         * Delete main image element from a model
         * @param int $id
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception  
         */
        public static function deleteMainImage(int $id){

            try{
                $image = static::find($id);
                Storage::delete($image->main_image_location);

                $image->main_image_location = '';
                $image->main_image_url = '';
                $image->save();

                return $image;
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        /**
         * Save main image element in a model
         * @param Illuminate\Http\Request $request
         * @param int $id
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception   
         */
        public static function storeMainImage(Request $request, int $id){
        
            $validatedData = $request->validate([
                'file' => 'required|image',
               ]);

            try{
                $name = $request->file('file')->getClientOriginalName();
        
                $path = $request->file('file')->storeAs(
                    'public/files', $name);
        
                $store = static::find($id);
                $store->main_image_location = $path;
                $store->main_image_url = asset('storage/files/' . $name);
                $store->save(); 

                return $store;
            }
            catch(\Exception $e){
                throw $e;
            }
        }

        /**
         * Save images in a gallery throu ajax request
         * @param Illuminate\Http\Request $request
         * @param int $id
         * @return Illuminate\Http\Response
         * @throws \Exception    
         */
        public static function storeGalleryAjax(Request $request, $id){

            $param = $request->validate([
                'file' => 'required|image',
            ]);

            $name = $src = '';

            try{
                $save = static::find($id)->_gallery;
        
                $success = false;
                $item = 0;
                for ($i = 1;  $i <= 5; $i++, $item++){
                    if ($save["item_location_$i"] == ''){
                        $name = $request->file('file')->getClientOriginalName();
        
                        $path = $request->file('file')->storeAs(
                            'public/files', $name);

                        $save["item_location_$i"] = $path;
                        $src = asset('storage/files/' . $name);
                        $save["item_url_$i"] = $src;
                        $success = true;
                        break;
                    }
                }

                if (!$success)
                    throw new \Exception('Все ячейки заняты. Очистите и повторите снова.');

                $save->save();
    
            }
            catch (\Exception $e){
                return response()->json(['error' => $e->getMessage()]);
            }
            
            return response()->json([
                'success' => $name,
                'response' => json_encode([
                    'eq' => $item,
                    'src' => $src
            ])]);
        }

        /**
         * Universal method for save a row in a model 
         * with all relations. 
         * @todo needs improvement
         * @param Illuminet\Http\Request $request
         * @param int $id
         * @return Illuminate\Database\Eloquent\Model
         * @throws \Exception 
         */
        public static function saveFromRequest(Request $request, int $id)
        {
            try{
                $record = static::find($id);
                
                if (isset( $record->enabled))
                    $record->enabled = ($request->input('isEnable') == 'on')? 1: 0;
                if (!empty($record->_name)){
                    $record->_name->string_rus = $request->input('nameRus', '');
                    $record->_name->string_ua = $request->input('nameUa', '');
                }
                if (!empty($record->description)){
                    $record->_descriptionText->text_rus = $request->input('descriptionRus', '');//$descriptionRus;
                    $record->_descriptionText->text_ua = $request->input('descriptionUa', '');//$descriptionUa;
                }    
                if (!empty($record->seo_block)){
                    $record->seoblock->url = $request->input('seoUrl', '');
                    $record->seoblock->title = $request->input('seoTitle', '');
                    $record->seoblock->keywords = $request->input('seoKeywords', '');
                    $record->seoblock->description = $request->input('seoDescription','');
                }
                if (!empty($record->address))
                    $record->_address->text_rus = $request->input('address', '');
        
                empty($record->name)?:$record->_name->save();
                empty($record->description)?:$record->_descriptionText->save();
                empty($record->address)?:$record->_address->save();
                empty($record->seo_block)?:$record->seoblock->save();
                $record->save();

                return $record;
                
            }
            catch(\Exception $e){
                throw $e;
            }
        }
    }