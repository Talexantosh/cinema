<?php

namespace App\Http\Controllers;

use App\Models\Admin\AdPage;
use Illuminate\Http\Request;
use App\Models\Banner;
use App\Models\Admin\TopSladerGallery;
use App\Models\Admin\BottomSladerGallery;
use App\Models\Admin\CurrentMovie;
use App\Models\Admin\SoonMovie;
use App\Models\Admin\Cinema;
use App\Models\Admin\CinemaHall;
use App\Models\Admin\Movie;
use Illuminate\Support\Facades\Log;
use App\Models\Admin\MovieSchedule;
use App\Models\Admin\Promotion;
use App\Models\Admin\News;
use App\Models\AuthCustomer;
use App\Models\Booking;
use App\Models\Customer;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Support\MessageProvider;
use Illuminate\Log\Logger;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class FrontController extends Controller
{
    const COUNT_PER_PAGE_FOR_NEWS = 4;
    /**
     * 
     */
    public function index(Request $request)
    {
        if (!optional(AdPage::find($request->id))->enabled)
            return abort(503);
        $schedule = MovieSchedule::findMovieSchedule("all", 
            "today", 
            'today', 
            "all", 
            "all", 
            '1', 
            '1', 
            '1');

        $movieIds = array_unique(array_column($schedule->toArray(), 'movie_id'), SORT_NUMERIC);
        
        return view('pages.front-page-main',
            [
                'title' => 'Главная',
                'banner' => Banner::find(1),
                'topSladerGallery' => TopSladerGallery::all(),
                'currentMovie' => Movie::select('*')->whereIn('id', $movieIds)->get(), //CurrentMovie::all(),
                'soonMovie' => SoonMovie::all(),
                'bottomSladerGallery' => BottomSladerGallery::all(),
                'pageId' => $request->id
            ]);
    }

    /**
     * 
     */
    public function indexPosters()
    {
        return view('pages.poster',
            [
                'title' => __('Афиша'),
                'banner' => Banner::find(1),
                'movies' => CurrentMovie::all()
            ]);
    }

    /**
     * 
     */
    public function  indexSchedule()
    {
        return view('pages.front-movie-schedule',
            [
                'title' => __('Расписание'),
                'banner' => Banner::find(1),
                'modelCinema' => Cinema::all(),
                'modelMovie' => CurrentMovie::all(),
                'allSchedule' => MovieSchedule::findMovieSchedule(
                    'all', 'today', '+1 days', 'all', 'all', '1', '1', '1')
            ]);
    }

    /**
     * 
     */
    public function indexSoon()
    {
        return view('pages.soon',
        [
            'title' => __('Скоро'),
            'banner' => Banner::find(1),
            'movies' => SoonMovie::all()
        ]);
    }

    /**
     * 
     */
    public function indexCinemas()
    {
        return view('pages.front-cinemas',
        [
            'title' => __('Кинотеатры'),
            'banner' => Banner::find(1),
            'cinemas' => Cinema::all()
        ]);
    }

    /**
     * 
     */
    public function indexCinema(int $id)
    {
        return view('pages.front-cinema-page',
        [
            'title' => __('Страница кинотеатра'),
            'banner' => Banner::find(1),
            'cinema' => Cinema::find($id)
        ]);
    }

    /**
     * 
     */
    public function indexHall(int $id)
    {
        return view('pages.front-cinema-hall-page',
        [
            'title' => __('Страница зала кинотеатра'),
            'banner' => Banner::find(1),
            'hall' => CinemaHall::find($id)
        ]);
    }

    /**
     * 
     */
    public function ticketBooking(int $cinemaId, int $hallId, int $movieId)
    {

    }

    /**
     * 
     */
    public function indexPromotion(int $id)
    {
        $promotion = Promotion::find($id);

        if (!optional($promotion)->enabled)
            return abort(503);

        return view('pages.front-page-promotion',
        [
            'title' => __('Страница акции'),
            'banner' => Banner::find(1),
            'promotion' => $promotion
        ]);
    }

    /**
     * 
     */
    public function indexPromotions()
    {
        return view('pages.front-promotions',
        [
            'title' => __('Акции и скидки'),
            'banner' => Banner::find(1),
            'promotions' => Promotion::all()
        ]);
    }

    /**
     * 
     */
    public function indexNews()
    {
        $count = 0;
        $news = News::all();
        foreach ($news as $item)
            if($item->enabled)
                $count++;

        $return = view('pages.front-news-grid',
        [
            'title' => __('Новости'),
            'banner' => Banner::find(1),
            'countAll' => $count,
            'countPerPage' => self::COUNT_PER_PAGE_FOR_NEWS
        ]);

        return $return;
    }

    /**
     * 
     */
    public function indexAdvertising(Request $request, int $id)
    {
        $name = urldecode($request->name);
        switch ($name){
            case 'Главная Страница': {
                return redirect()->route('front-main-index', ['id' => $id]);
            }
            case 'Контакты':{
                return view('pages.front-ad-page-contacts',
                [
                    'title' => __('Контакты'),
                    'banner' => Banner::find(1),
                    'seoblock' => optional(AdPage::find($id))->seoblock,
                    'model' => AdPage::all()->where('cinema', '=', 1)
                ] );
            }
            default:{
                $model = AdPage::find($id);
                if (!optional($model)->enabled)
                    return abort(503);
                $title = optional($model->_name)->string_rus;
                return view('pages.front-advertising', 
                [
                    'title' => __("$title"),
                    'banner' => Banner::find(1),
                    'model' => $model
                ]);
            }
        }
    }

    /**
     * 
     */
    public function indexAbout()
    {

    }

    /**
     * 
     */
    public function pageMovie(int $id)
    {
        return view('pages.movie-card', [
            'title' => __("О фильме"),
            'banner' => Banner::find(1),
            'movie' => Movie::find($id),
            'modelCinema' => Cinema::all()
        ]);
    }

    /**
     * @param Request $request
     * movieId,
     * cinemaId,
     * hallId,
     * beginDate,
     * endDate,
     * type2D,
     * type3D,
     * typeImax
     * @return html
     */
    public function handlePostAjaxForMovieSchedule(Request $request)
    {

        $collection = MovieSchedule::findMovieSchedule(
            $request->cinemaId,
            $request->beginDate,
            $request->endDate,
            $request->movieId,
            $request->hallId,
            $request->type2D,
            $request->type3D,
            $request->typeImax
        );

       
        if (!count($collection)){
            return '<div class="alert alert-light text-center">' .
                    __("По Вашему запросу ничего не найдено") .
                    '</div>';
        }
        
        return view('layouts.schedule-table', ['collection' => $collection]);                 
    }

    /**
     * @param Request $request
     * page
     */
    public function handlePostAjaxForNewsPagination(Request $request)
    {   /** Трехкратный запас на случай если много новостей отключено.
        * Требует доработки. Например перебор здесь, с целью передать
        * уже контретное количество записей*/
        $tripleStock = 3;
        try{
            $collection = News::orderBy('id', 'desc')->
                offset(($request->page - 1)*self::COUNT_PER_PAGE_FOR_NEWS)->
                limit(self::COUNT_PER_PAGE_FOR_NEWS*$tripleStock)->get();
        } 
        catch(\Exception $e){
            Log::error($e->getMessage());
            return '<div class="alert alert-light text-center">' .
            'Something went wrong' .
            '</div>';
        }
        
        return view('layouts.news-grid', [
            'collection' => $collection,
            'countPerPage' => self::COUNT_PER_PAGE_FOR_NEWS
        ]);                 
    }

    public function indexLogin()
    {
        return view('auth.customer-login');
    }

    public function indexRegister()
    {
        return view('auth.customer-register');
    }

    public function handleLogin(Request $request)
    {
        $validate = $request->validate(
            [
                'email' => ['required', 'string', 'email', 'max:255'],
                'password' => ['required', 'string', 'min:8']
            ]
            );
        if(!Auth::guard('customer')->attempt($validate)){
            return redirect()->back()->withErrors(
                [
                    'email' => 'Проверьте email',
                    'password' => 'Проверьте пароль'
                ]);
        }

        return redirect(url(session('url', '/')));
    }

    public function handleRegister(Request $request)
    {
        $validate = $request->validate(
        [
            'login' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        try{
             $user = AuthCustomer::create([
            'firstname' => '',
            'lastname' => '',
            'email' => $validate['email'],
            'login' => $validate['login'],
            'password' => Hash::make($validate['password']),
        ]);

        $customer = Customer::create(array_merge($user->toArray(), 
                    [
                        'dob' => Carbon::now(),
                        'phone' => '',
                        'city' => '',
                        'user_id' => $user->id
                    ]));
        }
        catch(\Exception $e){
            Log::error($e->getMessage());
            return Redirect::to(route('customer-register-index'))->withInput()->withErrors(
                [
                    'email' => "Код ошибки: " . $e->getCode(),
                ]);
        }

        Auth::guard('customer')->login($user);
        //Auth::guard('customer')->setUser($user);

        $url = Session::get('url', '/index');
        return redirect($url);
    }

    public function indexCabinet()
    {
        $user = Auth::guard('customer')->user();
        $customer = Customer::where('email', '=', $user->email)->get();
        return view('pages.customer-cabinet', 
        [
            'title' => __('Личный кабинет'),
            'banner' => Banner::find(1),
            'model' => $customer['0']
        ]);
    }

    public function changeUser(Request $request, int $id)
    {
        if (Auth::guard('customer')->id() == Customer::find($id)->user_id)
            return (new AdminController())->saveCustomer($request, $id);
        else
            return  view('admin.layouts.customer-form', ['model' => Customer::find($id)])
            ->with('alert', 'User not authenticated');
    }

    public function userLogout()
    {
        if (Auth::guard('customer')->check())
            Auth::guard('customer')->logout();
        return Redirect::back();
    }

    public function indexBooking(Request $request)
    {
        return view('pages.booking', 
        [
            'title' => __('Бронирование билета'),
            'banner' => Banner::find(1),
            'model' => MovieSchedule::find($request->schedule)
        ]);

    }

    public function bookTicket(Request $request, int $schedule)
    {
        try{
             if (Booking::validateSchedule($request, $schedule)){
            Booking::bookTicket($request, $schedule);
            }
        }
        catch(\Exception $e){
            Log::error($e->getMessage());
            abort(503);
        }
        Session::put('status', 'Ваши места забронированы!');
        return Redirect::back();
    }
}
