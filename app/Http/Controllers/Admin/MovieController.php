<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Movie;
use App\Models\Admin\Text;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;
use App\Models\Admin\MovieImageGallery;
use App\Models\Admin\CurrentMovie;
use App\Models\Admin\SoonMovie;
use Illuminate\Support\Facades\Storage;

class MovieController extends Controller
{
    const REQURE_SECTIONS = "[main]\n;Основное описание фильма\ntitle = \ndescript = \n\n[additional]\n;Для таблицы с дополнительным описанием - массив s[]\n\n[release]\n;Дата выхода фильма\ndate=";
    /**
     * 
     */
    protected $movie;

    /**
     * 
     */
    public function index(?int $id = 0)
    {
        $model = Movie::find($id);

        return view('admin.pages.movie-page',
            [
                'adminName' => Auth::user()->login,
                'movie' => $model
            ]);
    }

    public function save(Request $request, int $id)
    {
       $validate = $this->validate( $request,
            [
                'nameRus' => 'required',
                'nameUa' => 'required',
                'descriptionRus' => 'required|regex:/\[main\]\s*.*\s*title\s*=.*\s*descript\s*=.*\s*\[additional\]/',
                'descriptionUa' => 'required|regex:/\[main\]\s*.*\s*title\s*=.*\s*descript\s*=.*\s*\[additional\]/',
                'trailer_url' => 'required',
                'seoDescription' => 'required',
                'seoUrl' => 'required',
                'seoTitle' => 'required',
                'seoKeywords' => 'required',
                'seoDescription' => 'required'
            ]);

        /**
         * @todo validate for description field
         * 
         */ 
         try{
             parse_ini_string($validate["descriptionRus"], true);
             parse_ini_string($validate["descriptionUa"], true);
         }
         catch(\Exception $e)
         {
            return redirect()->route('movie-page-index', $id)
            ->withErrors(['message' => 'description field: ' . $e->getMessage()])
            ->withInput();
         }

        $movie = Movie::find($id);
        $movie->disabled = ($request->input('isDisable', 1) == 'on')? 0: 1;
        $movie->text->string_rus = $request->input('nameRus');
        $movie->text->string_ua = $request->input('nameUa');
        $movie->multitext->text_rus = $request->input('descriptionRus');
        $movie->multitext->text_ua = $request->input('descriptionUa');
        $movie->trailer_url = $request->input('trailer_url');
        $movie->type_3d = (int) ($request->input('type_3d', 'off') == 'on');
        $movie->type_2d = (int) ($request->input('type_2d', 'off') == 'on');
        $movie->type_imax = (int) ($request->input('type_imax', 'off') == 'on');
        $movie->seoblock->url = $request->input('seoUrl');
        $movie->seoblock->title = $request->input('seoTitle');
        $movie->seoblock->keywords = $request->input('seoKeywords');
        $movie->seoblock->description = $request->input('seoDescription');

        $movie->text->save();
        $movie->multitext->save();
        $movie->seoblock->save();
        $movie->save();

        return redirect()->route('movie-page-index', $movie->id);
    }

    public function deleteMainImage(int $id)
    {
        $movie = Movie::find($id);

        Storage::delete($movie->main_image);

        $movie->main_image = '';
        $movie->main_image_url = '';
        $movie->save();

        return redirect()->back();
    }

    public function pageReset(int $id)
    {
        $model = Movie::find($id);

        if (is_null($model))
            return redirect()->back()->with('error','Page not find');

        $model->text->update([
            'string_rus' => '',
            'string_ua' => ''
        ]);

        $model->multitext->update([
            'text_rus' => self::REQURE_SECTIONS,
            'text_ua' => self::REQURE_SECTIONS
        ]);

        $model->seoblock->update([
            'url' => '',
            'title' => '',
            'keywords' => '',
            'description' => ''
        ]);

        $model->update([
                'disabled' => 1,
                'lang' => 0,
                'trailer_url' => '',
                'type_3d' => 0,
                'type_2d' => 0,
                'type_imax' => 0
        ]);
            
        return redirect()->route('movie-page-index', [$id]);
    }

    /**
     * 
     */
    public function addCurrentMovie()
    {
        $movie = $this->addMovie();
        CurrentMovie::create([
            'movie_id' => $movie->id
        ]);
        return redirect()->route('movie-page-index', [$movie->id]);
    }

    /**
     * 
     */
    public function addSoonMovie()
    {
        $movie = $this->addMovie;
        SoonMovie::create([
            'movie_id' => $movie->id
        ]);
        return redirect()->route('movie-page-index',[$movie->id]);
    }

    /**
     * 
     */
    public function pageDelete(int $id)
    {
        $model = Movie::find($id);

        if (is_null($model))
            return redirect()->back()->with('error','Page not find');

        $model->text->delete();

        $model->multitext->delete();

        $model->seoblock->delete();

        Storage::delete($model->main_image);

        foreach ($model->gallery as $item)
            Storage::delete($item->image_location);

        /** в цикле перебрать, для коллекции не существует метода удалить */
        foreach ($model->gallery as $item)
            $item->delete();
        
        $model->delete();

        $currentMovie = CurrentMovie::where('movie_id', $id)->first();

        if($currentMovie)
            $currentMovie->delete();
        
        $soonMovie = SoonMovie::where('movie_id', $id)->first();

        if ($soonMovie)
            $soonMovie->delete();
 
        return redirect()->route('movies-index');
    }

    /**
 * Prepare the data for validation.
 *
 * @return void
 */
protected function prepareForValidation()
{
    $this->merge([
        'slug' => Str::slug($this->slug),
    ]);
}

/**
 * 
 */
protected function addMovie(){

    $text = Text::create();

    $description = Multitext::create(
        [
            'text_rus' => self::REQURE_SECTIONS,
            'text_ua' => self::REQURE_SECTIONS
        ]);      
    
    $seoBlock = SeoBlock::create();

    $movie = Movie::create(
        [
            'name' =>  $text->id,
            'description' => $description->id,
            'seo_block' => $seoBlock->id,
            'main_image' => '',
            'main_image_url' => '',
            'trailer_url' => '',
            'type_3d' => 0,
            'type_2d' => 0,
            'type_imax' => 0
        ]
    );
    
    for ($i=1; $i <= 5; $i++)
    {
        $image = MovieImageGallery::create(
        [
            'item' => $i,
            'movie_id' => $movie->id,
        ]);
    }

    return $this->movie = $movie;
}

    /**
     * Send movie into release
     */
    public function inRelease(int $id){
        $soon = SoonMovie::where('movie_id', $id)->get();

        try{
            if ($soon[0]){
                SoonMovie::find($soon[0]->id)->delete();
                CurrentMovie::create(['movie_id' => $id]);
            }
        }
        catch(\Exception $e){
            return redirect()->route('movie-page-index', $id)->with('error', $e->getMessage());
        }
        
        return redirect()->route('movie-page-index', $id);
    }
}
