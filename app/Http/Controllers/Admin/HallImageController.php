<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\CinemaHall;
use App\Models\Admin\HallImageGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class HallImageController extends Controller
{
    /**
     * 
     */
    protected $name;
 
    /**
     * 
     */
    protected $path;

    /**
     * 
     */
    public function index(int $id, ?string $type = null, ?int $item = null)
    {
        switch ($type)
        {
            case 'schema':
                {
                    $action = route('load-hall-schema', $id);
                    break;
                }
            case 'top':
                {
                    $action = route('load-hall-top-banner', $id);
                    break;
                }
            case 'gallery':
                {
                    $action = route('load-hall-gallery-item', [$id, $item]);
                    break;
                }
            default:
                {
                    abort(404);
                }
        }

        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => $action
            ]);
    }

    /**
     * 
     */
    public function storeSchema(Request $request, int $id)
    {
        $this->getFileParameters($request) ;

        $hall = CinemaHall::find($id);
        $hall->schema_location =  $this->path;
        $hall->schema_url = asset('storage/files/' . $this->name);
        $hall->save(); 
 
        return redirect()->route('cinema-hall-index', $id);
 
    }

    public function deleteSchema(int $id)
    {
        $hall = CinemaHall::find($id);

        Storage::delete($hall->schema_location);

        $hall->schema_location = '';
        $hall->schema_url = '';
        $hall->save();

        return redirect()->back();
    }

    /**
     * 
     */
    public function storeTopBanner(Request $request, int $id)
    { 
        $this->getFileParameters($request);

        $hall = CinemaHall::find($id);
        $hall->img_location =  $this->path;
        $hall->img_url = asset('storage/files/' . $this->name);
        $hall->save(); 
 
        return redirect()->route('cinema-hall-index', $id);
 
    }

    public function deleteTopBanner(int $id)
    {
        $hall = CinemaHall::find($id);

        Storage::delete($hall->img_location);

        $hall->img_location = '';
        $hall->img_url = '';
        $hall->save();

        return redirect()->back();
    }

    /**
     * 
     */
    public function storeGalleryItem(Request $request, int $id, int $item)
    {
         
        $this->getFileParameters($request);

        $hall = CinemaHall::find($id);
        if (!$hall->gallery)
        {
            $galleries = HallImageGallery::createIfNotExist($id);
        }
        try{
            $hall->gallery[$item - 1]->hall_id = $id;
            $hall->gallery[$item - 1]->image_location = $this->path;
            $hall->gallery[$item - 1]->url = asset('storage/files/' . $this->name);
            $hall->gallery[$item - 1]->save(); 
        }
        catch(\Exception $e){
            return redirect()->route('cinema-hall-index', $id)->withErrors(['errors' => 'Проблемы с сохранением галлереи']);
        }
 
        return redirect()->route('cinema-hall-index', $id);
 
    }

    public function deleteGalleryItem(int $id, int $item)
    {
        $images = CinemaHall::find($id)->gallery;

        try{
            Storage::delete($images[$item - 1]->image_location);

            $images[$item - 1]->image_location = '';
            $images[$item - 1]->url = '';
            $images[$item - 1]->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(['errors' => 'Проблема с операцией очистки']);
        }

        return redirect()->back();
    }

    protected function getFileParameters(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|image',
    
           ]);
    
        $this->name = $request->file('file')->getClientOriginalName();

        $this->path = $request->file('file')->storeAs(
            'public/files', $this->name);
    }
}
