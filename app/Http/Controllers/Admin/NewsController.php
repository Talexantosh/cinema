<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\News;
use App\Models\Admin\SeoBlock;
use App\Models\Admin\Text;
use App\Models\Admin\Multitext;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\ImageGallery;
use Illuminate\Support\Facades\Storage;

class NewsController extends Controller
{
    /**
     * 
     */
    public function index()
    {
        $news = News::all()->sortDesc();

        return view('admin.pages.news-list',
        [
            'adminName' => Auth::user()->login,
            'model' => $news
        ]);
    }
    
    /**
     * 
     */
    public function indexNews(int $id)
    {
        $news = News::find($id);
    
        return view('admin.pages.news-page',
        [
            'adminName' => Auth::user()->login,
            'model' => $news
        ]);
    }

    public function createNews(){

        try{
            $news = News::create();

            $name = Text::create();
            $news->name = $name->id;

            $description = Multitext::create();
            $news->description = $description->id;

            $gallery = ImageGallery::create();
            $news->gallery = $gallery->id;

            $seoblock = SeoBlock::create();
            $news->seo_block = $seoblock->id;

            $news->save();

            $news->_description->text_rus = 
            $news->_description->text_ua =
            News::DESCRIPTION_TEMPLATE;
            $news->_description->save();
        }
        catch(\Exception $e){
            return redirect()->route('admin-news-index')->withErrors($e->getMessage());
        }

        return redirect()->route('admin-news-index');
    }

    /**
     * 
     */
    public function deleteNews(int $id){

        $news = News::find($id);

        try{
            $news->_name->delete();
            $news->_description->delete();
            $news->_gallery->delete();
            $news->seoblock->delete();
            $news->delete();
        }
        catch(\Exception $e){
            return redirect()->route('admin-news-index')->withErrors($e->getMessage());
        }

        return redirect()->route('admin-news-index');
    }

    /**
     * 
     */
    public function saveNews(Request $request, int $id){

        $validate = $this->validate( $request,
            [
                'nameRus' => 'required',
                'nameUa' => 'required',
                'descriptionRus' => 'required',
                'descriptionUa' => 'required',
                'seoUrl' => 'required',
                'seoTitle' => 'required',
                'seoKeywords' => 'required',
                'seoDescription' => 'required'
            ]);

        try{
            $news = News::find($id);

            try{
                $news->validateDescription($request->input('descriptionRus'));
                $news->validateDescription($request->input('descriptionUa'));
            }
            catch(\Exception $e){
                return redirect()->back()->withErrors('Error validation: ' . $e->getMessage());
            }

            $news->enabled = ($request->input('isEnable') == 'on')? 1: 0;
            $news->date_publish = $request->input('datePublish', date('Y-m-d'));
            $news->_name->string_rus = $request->input('nameRus', '');
            $news->_name->string_ua = $request->input('nameUa', '');
            $news->_description->text_rus = $request->input('descriptionRus', '');
            $news->_description->text_ua = $request->input('descriptionUa', '');
            $news->seoblock->url = $request->input('seoUrl', '');
            $news->seoblock->title = $request->input('seoTitle', '');
            $news->seoblock->keywords = $request->input('seoKeywords', '');
            $news->seoblock->description = $request->input('seoDescription','');
    
            $news->_name->save();
            $news->_description->save();
            $news->seoblock->save();
            $news->save();
            
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->route('admin-news-page-index', [$news->id]);
    }

    /**
     * 
     */
    public function newsGalleryItemDelete(int $id, int $item){

        $images = News::find($id)->_gallery;

        try{
            Storage::delete($images["item_location_$item"]);

            $images["item_location_$item"] = '';
            $images["item_url_$item"] = '';
            $images->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->back();
    }

    /**
     * 
     */
    public function newsGalleryItemLoad(int $id, int $item){
        
        return view('admin.pages.file-upload', 
        [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => route('news-item-gallery-store', [$id, $item])
        ]);
    }

    /**
     * 
     */
    public function newsGalleryItemStore(Request $request, int $id, int $item){
        
        $validatedData = $request->validate([
            'file' => 'required|image',
           ]);
    
           try{
                $name = $request->file('file')->getClientOriginalName();
        
                $path = $request->file('file')->storeAs(
                    'public/files', $name);
        
                $stores = News::find($id)->_gallery;
                $stores["item_location_$item"] =  $path;
                $stores["item_url_$item"] = asset('storage/files/' . $name);
                $stores->save(); 
           }
           catch(\Exception $e){
                return redirect()->route('admin-news-page-index', $id)->withErrors($e->getMessage());
           }
    
           return redirect()->route('admin-news-page-index', $id);
    }
}
