<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\BottomSladerGallery;
use Illuminate\Support\Facades\Auth;

class BottomSladerGalleryFilesController extends Controller
{
    /**
     * 
     */
    public function index(int $id)
    {
        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login, 
            'id' => $id,
            'action' => route('bottom-gallery-store', [$id])
            ]);
    }

    /**
     * 
     */
    public function store(Request $request)
    {
         
        $validatedData = $request->validate([
         'file' => 'required|image',
 
        ]);
 
        $name = $request->file('file')->getClientOriginalName();
 
        $path = $request->file('file')->storeAs(
            'public/files', $name);
 
 
        $save = BottomSladerGallery::find($request->input('id'));
 
        if (!$save)
        {
            $save = new BottomSladerGallery;
        }

        $save->id = $request->input('id');
        $save->image_location = $path;
        $save->url = asset('storage/files/' . $name);
        // Default value
        $save->text = "";
        $save->save();
 
        return redirect()->route('banners');
 
    }
}
