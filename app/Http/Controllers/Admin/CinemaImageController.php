<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Cinema;
use Illuminate\Support\Facades\Storage;
use App\Models\Admin\CinemaImageGallery;

class CinemaImageController extends Controller
{
    /**
     * 
     */
    protected $name;
 
    /**
     * 
     */
    protected $path;

    /**
     * 
     */
    public function index(Request $request, int $id, ?string $type = null, ?int $item = null)
    {
        switch ($type)
        {
            case 'logo':
                {
                    $action = route('load-cinema-logo', $id);
                    break;
                }
            case 'top':
                {
                    $action = route('load-cinema-top-banner', $id);
                    break;
                }
            case 'gallery':
                {
                    $action = route('load-cinema-gallery-item', [$id, $item]);
                    break;
                }
            default:
                {
                    abort(404);
                }
        }
    
        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => $action
            ]);
    }

    /**
     * 
     */
    public function storeLogo(Request $request, int $id)
    {
        $this->getFileParameters($request) ;

        $cinema = Cinema::find($id);
        $cinema->logo_location =  $this->path;
        $cinema->logo_url = asset('storage/files/' . $this->name);
        $cinema->save(); 
 
        return redirect()->route('cinema-page-index', $id);
 
    }

    public function deleteLogo(int $id)
    {
        $cinema = Cinema::find($id);

        Storage::delete($cinema->logo_location);

        $cinema->logo_location = '';
        $cinema->logo_url = '';
        $cinema->save();

        return redirect()->back();
    }

    /**
     * 
     */
    public function storeTopBanner(Request $request, int $id)
    { 
        $this->getFileParameters($request);

        $cinema = Cinema::find($id);
        $cinema->img_location =  $this->path;
        $cinema->img_url = asset('storage/files/' . $this->name);
        $cinema->save(); 
 
        return redirect()->route('cinema-page-index', $id);
 
    }

    public function deleteTopBanner(int $id)
    {
        $cinema = Cinema::find($id);

        Storage::delete($cinema->img_location);

        $cinema->img_location = '';
        $cinema->img_url = '';
        $cinema->save();

        return redirect()->back();
    }

    /**
     * 
     */
    public function storeGalleryItem(Request $request, int $id, int $item)
    {
         
        $this->getFileParameters($request);

        $cinema = Cinema::find($id);
        if (!$cinema->gallery)
        {
            $galleries = CinemaImageGallery::createIfNotExist($id);
        }
        
        try{
            $cinema->gallery[$item - 1]->image_location = $this->path;
            $cinema->gallery[$item - 1]->url = asset('storage/files/' . $this->name);
            $cinema->gallery[$item - 1]->save(); 
        }
        catch(\Exception $e){
            return redirect()->route('cinema-page-index', $id)->withErrors(['errors' => 'Exception for save item gallery']);
        }
 
        return redirect()->route('cinema-page-index', $id);
 
    }

    public function deleteGalleryItem(int $id, int $item)
    {
        $images = Cinema::find($id)->gallery;

        Storage::delete($images[$item]->image_location);

        try{
            $images[$item - 1]->image_location = '';
            $images[$item - 1]->url = '';
            $images[$item - 1]->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors(['errors' => 'Exception for save item gallery']);
        }

        return redirect()->back();
    }

    protected function getFileParameters(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|image',
    
           ]);
    
        $this->name = $request->file('file')->getClientOriginalName();

        $this->path = $request->file('file')->storeAs(
            'public/files', $this->name);
    }
}
