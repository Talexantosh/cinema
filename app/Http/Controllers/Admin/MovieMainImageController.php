<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\Movie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MovieMainImageController extends Controller
{
     /**
     * 
     */
    public function index(int $id)
    {
        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login, 
            'id' => $id,
            'action' => route('movie-main-image-store', ['id' => $id])
            ]);
    }

    /**
     * 
     */
    public function store(Request $request)
    {
         
        $validatedData = $request->validate([
         'file' => 'required|image',
 
        ]);
 
        $name = $request->file('file')->getClientOriginalName();
 
        $path = $request->file('file')->storeAs(
            'public/files', $name);
 
        $id = $request->id;
        $save = Movie::find($id);

        $save->main_image = $path;
        $save->main_image_url = 'storage/files/' . $name;
        $save->save();
 
        return redirect()->route('movie-page-index', ['id' => $id]);
    }
}
