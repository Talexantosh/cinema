<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;
use Illuminate\Support\Facades\Storage;

class BackgroundController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $banner = Banner::find(1);
        
        $banner->back_image_status = ($request->input('photo') == 'photo')? 1: 0;

        $banner->save();
        
        return redirect()->back();
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete()
    {
        $banner = Banner::find(1);

        Storage::delete($banner->back_image_location);

        $banner->back_image_location = '';
        $banner->back_image_url = '';
        $banner->save();

        return redirect()->back();
    }
}
