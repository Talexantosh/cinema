<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin\BottomSladerGallery;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Models\Banner;

class BottomSladerGalleryController extends Controller
{
     /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $banner = Banner::find(1);
        
        if ($request->input('isEnable') == 'on')
        {
            $banner->bottom_slader_status = 1;
        }
        else
        {
            $banner->bottom_slader_status = 0;
        }

        $banner->bottom_slader_speed = $request->input('speedSlader');

        

        for ($i = 1, $j = 0; $i <= $banner->top_slader_count; $i++, $j++)
        {
            $bottomSlader = BottomSladerGallery::find($i);
            if ($request->input("text$j"))
            {
                $bottomSlader->text = $request->input("text$j");
                $bottomSlader->save();
            }
        }

        $banner->save();
        
        return redirect()->back();
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, int $id)
    {
        //
        $bottomSlader = BottomSladerGallery::find($id);
        Storage::delete($bottomSlader->image_location);

        $banner = Banner::find(1);
        
        if ($banner->bottom_slader_count == $id)
            $banner->bottom_slader_count -= 1;
        $banner->save();
       

        $bottomSlader->image_location = '';
        $bottomSlader->url = 'dist/img/logo.jpg';
        $bottomSlader->text = '';
        $bottomSlader->save();

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function addItem()
    {
        $collection = Banner::find(1);
        $collection->bottom_slader_count += 1;
        $collection->save();

        $newImage = new BottomSladerGallery;
        $newImage->image_location = '';
        $newImage->url = 'dist/img/logo.jpg';
        $newImage->text = '';
        $newImage->save();
        return redirect()->back();
    }
}
