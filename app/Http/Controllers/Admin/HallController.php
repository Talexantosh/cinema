<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\CinemaHall;
use App\Models\Admin\HallImageGallery;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;
use App\Models\Admin\Text;
use Illuminate\Support\Facades\Auth;

class HallController extends Controller
{
    /**
     * 
     */
    public function indexHall(int $id)
    {
        $hall = CinemaHall::find($id);

        return view('admin.pages.hall-page',
        [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'model' => $hall
        ]);


    }

    /**
     * 
     */
    public function deleteHall(int $id)
    {
        $hall = CinemaHall::find($id);
        if ($hall)
            $hall->delete();
        else 
            abort(404);
        return redirect()->back();
    }

    /**
     * 
     */
    public function addHall(int $id)
    {
        $hall = CinemaHall::create([
            'name' => Text::create()->id,
            'description' => Multitext::create()->id,
            'seoblock_id' =>  SeoBlock::create()->id,
            'cinema_id' => $id,

        ]);
        
        HallImageGallery::createIfNotExist($hall->id);
        
        $hall->_description->text_rus = CinemaHall::HALL_DESCRIPTION_SAMPLE_RUS;
        $hall->_description->text_ua = CinemaHall::HALL_DESCRIPTION_SAMPLE_UA;
        $hall->_description->save();
        
        return redirect()->route('cinema-hall-index', $hall->id);
    }

    /**
     * 
     */
    public function saveHall(Request $request, int $id)
    {
        $validate = $this->validate( $request,
            [
                'nameRus' => 'required',
                'nameUa' => 'required',
                'descriptionRus' => 'required',
                'descriptionUa' => 'required',
                'seoUrl' => 'required',
                'seoTitle' => 'required',
                'seoKeywords' => 'required',
                'seoDescription' => 'required'
            ]);

            $hall = CinemaHall::find($id);

            try{  // тоже валидация
                parse_ini_string($request->input('descriptionRus'), true);
                parse_ini_string($request->input('descriptionUa'), true);
            }
            catch(\Exception $e){
                return redirect()->route('cinema-hall-index', $id)
                ->withErrors(['message' => 'Error parsing description: ' . $e->getMessage()])
                ->withInput([
                    'nameRus' => $request->old('nameRus'),
                    'nameUa' => $request->old('nameUa'),
                    'descriptionRus' => $request->old('descriptionRus'),
                    'descriptionUa' => $request->old('descriptionUa'),
                    'seoUrl' => $request->old('seoUrl'),
                    'seoTitle' => $request->old('seoTitle'),
                    'seoKeywords' => $request->old('seoKeywords'),
                    'seoDescription' => $request->old('seoDescription')
                ]);
            }

        try{
           $hall->saveHall($request);
        }
        catch(\Exception $e){
            return redirect()->route('cinema-hall-index', $id)
                ->withErrors(['message' => 'Error operating with bd: ' . $e->getMessage()])
                ->withInput([
                    'nameRus' => $request->old('nameRus'),
                    'nameUa' => $request->old('nameUa'),
                    'descriptionRus' => $request->old('descriptionRus'),
                    'descriptionUa' => $request->old('descriptionUa'),
                    'seoUrl' => $request->old('seoUrl'),
                    'seoTitle' => $request->old('seoTitle'),
                    'seoKeywords' => $request->old('seoKeywords'),
                    'seoDescription' => $request->old('seoDescription')
                ]);
        }
        
        return redirect()->route('cinema-hall-index', $id);
    }
}
