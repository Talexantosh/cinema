<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Movie;
use Illuminate\Support\Facades\Storage;

class MovieGalleryUploadController extends Controller
{
    /**
     * 
     */
    public function index(int $id, int $item)
    {
        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => route('movie-item-gallery-store', [$id, $item])
            ]);
    }

    /**
     * 
     */
    public function store(Request $request, int $id, int $item)
    {
         
        $validatedData = $request->validate([
         'file' => 'required|image',
 
        ]);
 
        $name = $request->file('file')->getClientOriginalName();
 
        $path = $request->file('file')->storeAs(
            'public/files', $name);
 
        $stores = Movie::find($id)->gallery;
        $stores[$item - 1]->image_location =  $path;
        $stores[$item - 1]->url = asset('storage/files/' . $name);
        $stores[$item - 1]->save(); 
 
        return redirect()->route('movie-page-index', $id);
 
    }

    public function delete(int $id, int $item)
    {
        $images = Movie::find($id)->gallery;

        Storage::delete($images[$item - 1]->image_location);

        $images[$item - 1]->image_location = '';
        $images[$item - 1]->url = '';
        $images[$item - 1]->save();

        return redirect()->back();
    }
}
