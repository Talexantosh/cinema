<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use  App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Banner;
use App\Models\Admin\TopSladerGallery;
use App\Models\Admin\BottomSladerGallery;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bannerConfig = Banner::find(1);
        $fileLocations = TopSladerGallery::all(['image_location']);
        $urlTopImages =  TopSladerGallery::all(['url']);
        $textTopValues = TopSladerGallery::all(['text']);

        $filesBottomGallery = BottomSladerGallery::all(['image_location']);
        $urlBottomImages = BottomSladerGallery::all(['url']);
        $textBottomValues = BottomSladerGallery::all(['text']);
         
        return view('admin.pages.banners', 
            [
                'adminName' => Auth::user()->login,
                'stateTop' => $bannerConfig->top_slader_status,
                'countTopImages' => $bannerConfig->top_slader_count, 
                'urlTopImages' => $urlTopImages,
                'textTopValues' => $textTopValues,
                'speedTopSlader' => $bannerConfig->top_slader_speed,
                'fileLocations' => $fileLocations,
                'stateBack' => $bannerConfig->back_image_status,
                'urlBackground' => $bannerConfig->back_image_url,
                'backgroundLocation' => $bannerConfig->back_image_location,
                'stateBottom' => $bannerConfig->bottom_slader_status,
                'countBottomImages' => $bannerConfig->bottom_slader_count,
                'urlBottomImages' => $urlBottomImages, 
                'textBottomValues' => $textBottomValues,
                'speedBottomSlader' => $bannerConfig->bottom_slader_speed,
                'filesBottomGallery' => $filesBottomGallery
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
