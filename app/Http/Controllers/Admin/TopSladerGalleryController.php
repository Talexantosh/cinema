<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Banner;
use App\Models\Admin\TopSladerGallery;

class TopSladerGalleryController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $banner = Banner::find(1);
        
        if ($request->input('isEnable') == 'on')
        {
            $banner->top_slader_status = 1;
        }
        else
        {
            $banner->top_slader_status = 0;
        }

        $banner->top_slader_speed = $request->input('speedSlader');

        

        for ($i = 1, $j = 0; $i <= $banner->top_slader_count; $i++, $j++)
        {
            $topSlader = TopSladerGallery::find($i);
            if ($request->input("text$j"))
            {
                $topSlader->text = $request->input("text$j");
                $topSlader->save();
            }
        }

        $banner->save();
        
        return redirect()->back();
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function delete(Request $request, int $id)
    {
        //
        $topSlader = TopSladerGallery::find($id);
        Storage::delete($topSlader->image_location);

        $banner = Banner::find(1);
        
        if ($banner->top_slader_count == $id)
            $banner->top_slader_count -= 1;
        $banner->save();
       

        $topSlader->image_location = '';
        $topSlader->url = 'dist/img/logo.jpg';
        $topSlader->text = '';
        $topSlader->save();

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Http\Response
     */
    public function addItem()
    {
        $collection = Banner::find(1);
        $collection->top_slader_count += 1;
        $collection->save();

        $newImage = new TopSladerGallery;
        $newImage->image_location = '';
        $newImage->url = 'dist/img/logo.jpg';
        $newImage->text = '';
        $newImage->save();
        return redirect()->back();
    }
}
