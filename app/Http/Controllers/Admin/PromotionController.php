<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Promotion;
use App\Models\Admin\Text;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;
use App\Models\Admin\ImageGallery;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PromotionController extends Controller
{
    /**
     * Admin panel page promotions
     */
    public function promotionsIndex(){
        $model = Promotion::all();

        return view('admin.pages.promotions',
        [
            'adminName' => Auth::user()->login,
            'model' => $model
        ]);
    }

    public function promotionCreate(){

        try{
            $promotion = Promotion::create();

            $name = Text::create();
            $promotion->name = $name->id;

            $description = Multitext::create();
            $promotion->description = $description->id;

            $gallery = ImageGallery::create();
            $promotion->gallery = $gallery->id;

            $seoblock = SeoBlock::create();
            $promotion->seo_block = $seoblock->id;

            $promotion->save();

            $promotion->_description->text_rus = 
            $promotion->_description->text_ua =
            Promotion::DESCRIPTION_TEMPLATE;
            $promotion->_description->save();
        }
        catch(\Exception $e){
            return redirect()->route('admin-promotions-index')->withErrors($e->getMessage());
        }

        return redirect()->route('admin-promotions-index');
    }

    /**
     * 
     */
    public function promotionDelete(int $id){

        $promotion = Promotion::find($id);

        try{
            $promotion->_name->delete();
            $promotion->_description->delete();
            $promotion->_gallery->delete();
            $promotion->seoblock->delete();
            $promotion->delete();
        }
        catch(\Exception $e){
            return redirect()->route('admin-promotions-index')->withErrors($e->getMessage());
        }

        return redirect()->route('admin-promotions-index');
    }

    /**
     * 
     */
    public function promotionIndex(int $id){

        $model = Promotion::find($id);
        
        return view('admin.pages.promotion-page', [
            'adminName' => Auth::user()->login,
            'model' => $model
        ]);
    }

    /**
     * 
     */
    public function promotionSave(Request $request, int $id){

        $validate = $this->validate( $request,
            [
                'nameRus' => 'required',
                'nameUa' => 'required',
                'descriptionRus' => 'required',
                'descriptionUa' => 'required',
                'seoUrl' => 'required',
                'seoTitle' => 'required',
                'seoKeywords' => 'required',
                'seoDescription' => 'required'
            ]);

        try{
            $promotion = Promotion::find($id);

            try{
                $promotion->validateDescription($request->input('descriptionRus'));
                $promotion->validateDescription($request->input('descriptionUa'));
            }
            catch(\Exception $e){
                return redirect()->back()->withErrors('Error validation: ' . $e->getMessage());
            }

            $promotion->enabled = ($request->input('isEnable') == 'on')? 1: 0;
            $promotion->date_publish = $request->input('datePublish', date('Y-m-d'));
            $promotion->_name->string_rus = $request->input('nameRus', '');
            $promotion->_name->string_ua = $request->input('nameUa', '');
            $promotion->_description->text_rus = $request->input('descriptionRus', '');
            $promotion->_description->text_ua = $request->input('descriptionUa', '');
            $promotion->seoblock->url = $request->input('seoUrl', '');
            $promotion->seoblock->title = $request->input('seoTitle', '');
            $promotion->seoblock->keywords = $request->input('seoKeywords', '');
            $promotion->seoblock->description = $request->input('seoDescription','');
    
            $promotion->_name->save();
            $promotion->_description->save();
            $promotion->seoblock->save();
            $promotion->save();
            
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->route('admin-promotion-index', [$promotion->id]);
    }

    /**
     * Не реализована дл исключения дублирования 
     * картинок в пользу использования картинок из галлереи
     * Первая картинка в галлерее будет главной картинкой
     * акции
     * @deprecated
     */
     public function promotionMainImageDelete(int $id){

     }

     /**
      * Не реализована дл исключения дублирования 
      * картинок в пользу использования картинок из галлереи
      * Первая картинка в галлерее будет главной картинкой
      * акции
      * @deprecated
      */
    public function promotionMainImageLoad(int $id){

    }

    /**
     * 
     */
    public function promotionGalleryItemDelete(int $id, int $item){

        $images = Promotion::find($id)->_gallery;

        try{
            Storage::delete($images["item_location_$item"]);

            $images["item_location_$item"] = '';
            $images["item_url_$item"] = '';
            $images->save();
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->back();
    }

    /**
     * 
     */
    public function promotionGalleryItemLoad(int $id, int $item){
        
        return view('admin.pages.file-upload', 
        [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => route('promotion-item-gallery-store', [$id, $item])
        ]);
    }

    /**
     * 
     */
    public function promotionGalleryItemStore(Request $request, int $id, int $item){
        
        $validatedData = $request->validate([
            'file' => 'required|image',
           ]);
    
           try{
                $name = $request->file('file')->getClientOriginalName();
        
                $path = $request->file('file')->storeAs(
                    'public/files', $name);
        
                $stores = Promotion::find($id)->_gallery;
                $stores["item_location_$item"] =  $path;
                $stores["item_url_$item"] = asset('storage/files/' . $name);
                $stores->save(); 
           }
           catch(\Exception $e){
                return redirect()->route('admin-promotion-index', $id)->withErrors($e->getMessage());
           }
    
           return redirect()->route('admin-promotion-index', $id);
    }
}
