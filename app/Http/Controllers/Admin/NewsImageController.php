<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Admin\News;
use App\Models\Admin\NewsImageGallery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class NewsImageController extends Controller
{
     /**
     * 
     */
    protected $name;
 
    /**
     * 
     */
    protected $path;

    /**
     * 
     */
    public function index(int $id, ?string $type = null, ?int $item = null)
    {
        switch ($type)
        {
            case 'main':
                {
                    $action = route('load-hall-top-banner', $id);
                    break;
                }
            case 'gallery':
                {
                    $action = route('load-hall-gallery-item', [$id, $item]);
                    break;
                }
            default:
                {
                    abort(404);
                }
        }

        return view('admin.pages.file-upload', 
            [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'action' => $action
            ]);
    }

    /**
     * 
     */
    public function storeMainImage(Request $request, int $id)
    { 
        $this->getFileParameters($request);

        $img = News::find($id);
        $img->img_location =  $this->path;
        $img->img_url = asset('storage/files/' . $this->name);
        $img->save(); 
 
        return redirect()->route('news-page-index', $id);
 
    }

    public function deleteMainImage(int $id)
    {
        $img = News::find($id);

        Storage::delete($img->img_location);

        $img->img_location = '';
        $img->img_url = '';
        $img->save();

        return redirect()->back();
    }

    /**
     * 
     */
    public function storeGalleryItem(Request $request, int $id, int $item)
    {
         
        $this->getFileParameters($request);

        $hall = News::find($id);
        if (!$hall->gallery)
        {
            $galleries = NewsImageGallery::createIfNotExist($id);
        }
        $galleries[$item]->news_id = $id;
        $galleries[$item]->image_location = $this->path;
        $galleries[$item]->url = asset('storage/files/' . $this->name);
        $galleries[$item]->save(); 
 
        return redirect()->route('news-page-index', $id);
 
    }

    public function deleteGalleryItem(int $id, int $item)
    {
        $images = News::find($id)->gallery;

        Storage::delete($images[$item]->image_location);

        $images[$item]->image_location = '';
        $images[$item]->url = '';
        $images[$item]->save();

        return redirect()->back();
    }

    /**
     * 
     */
    protected function getFileParameters(Request $request)
    {
        $validatedData = $request->validate([
            'file' => 'required|image',
    
           ]);
    
        $this->name = $request->file('file')->getClientOriginalName();

        $this->path = $request->file('file')->storeAs(
            'public/files', $this->name);
    }
}
