<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\AdPage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AdPageController extends Controller
{
     /**
     * 
     */
    public function indexAdPages()
    {
        $adPages = AdPage::all()->where('cinema', '!=', 1);

        return view('admin.pages.ad-page-list',
        [
            'adminName' => Auth::user()->login,
            'model' => $adPages
        ]);
    }
    
    /**
     * 
     */
    public function indexAdPage(int $id)
    {
        $adPage = AdPage::find($id);

        if (!$adPage)
            abort(404);
        
        switch ($adPage->_name->string_rus){
            case 'Главная Страница': {
                return view('admin.pages.ad-page-main',
                    [
                        'adminName' => Auth::user()->login,
                        'model' => $adPage
                    ]);
            }
            case 'Контакты': {
                return view('admin.pages.ad-page-contacts',
                    [
                        'adminName' => Auth::user()->login,
                        'model' => AdPage::getAllAddresses(),
                        'list' => AdPage::find($id)
                    ]);
            }
        }
    
        return view('admin.pages.ad-page',
        [
            'adminName' => Auth::user()->login,
            'model' => $adPage
        ]);
    }

     /**
     * 
     */
    public function handleAdPage(int $id, string $type, string $operation)
    {    
        try{
            switch ($type){
                case "normal":{
                    if ($operation == "create")
                        AdPage::createRecord();
                    if ($operation == "delete")
                        AdPage::deleteRecord($id);
                    break;
                }
                case "address":{
                    if ($operation == "create")
                        AdPage::createAddress();
                    if ($operation == "delete")
                        AdPage::deleteRecord($id);
                    break;
                }
            }
        }
        catch(\Exception $e){
            return redirect()->route('admin-ad-pages-index')->withErrors($e->getMessage());
        }

        return redirect()->back();
    }

    /**
     * 
     */
    public function saveAdPage(Request $request, int $id, string $type){

        try{
            switch ($type){
                case "main": {
                    $validate = $this->validate( $request,
                    [
                        'seoRus' => 'required',
                        'seoUa' => 'required',
                        'seoUrl' => 'required',
                        'seoTitle' => 'required',
                        'seoKeywords' => 'required',
                        'seoDescription' => 'required'
                    ]);

                    $adPage = AdPage::saveMainAdPage($request, $id);
                    break;
                }
                case "address": {
                    try{
                        $validate = $this->validate( $request,
                        [
                            'nameRus' => 'required',
                            'nameUa' => 'required',
                            'descriptionRus' => 'required',
                            'descriptionUa' => 'required',
                            'address' => 'required'
                        ]); 
                        AdPage::saveFromRequest($request, $id);
                        return view('admin.layouts.cinema-contact-response', 
                        ['model' => AdPage::find($id)]);
                    }
                    catch(\Exception  $e){
                        if ($e instanceof ValidationException){
                            $messages = $e->errors();
                            return view('admin.layouts.cinema-contact-response', 
                            ['model' => AdPage::find($id), 'messages' => $e->errors()]);
                        }
                        else {
                            return view('admin.layouts.cinema-contact-response', 
                            ['model' => AdPage::find($id), 'alert' => $e->getMessage()]);
                        }
                    }
                }
                case "contacts": {
                    $validate = $this->validate( $request,
                    [
                        'seoUrl' => 'required',
                        'seoTitle' => 'required',
                        'seoKeywords' => 'required',
                        'seoDescription' => 'required'
                    ]);
                    $adPage = AdPage::saveRecord($request, $id);
                }
                default: {
                    $validate = $this->validate( $request, 
                        [
                            'nameRus' => 'required',
                            'nameUa' => 'required',
                            'descriptionRus' => 'required',
                            'descriptionUa' => 'required',
                            'seoUrl' => 'required',
                            'seoTitle' => 'required',
                            'seoKeywords' => 'required',
                            'seoDescription' => 'required'
                        ]);                                     

                    $adPage = AdPage::saveRecord($request, $id);
                }
            }
        }
        catch(\Exception $e){
            return redirect()->back()->withErrors($e->getMessage());
        }

        return redirect()->route('admin-ad-page-index', [$adPage->id]);
    }

    /**
     * 
     */
    public function handleImage(int $id, string $type, int $item, string $operation)
    {
        if ($operation == "load"){
            return view('admin.pages.file-upload', 
                [
                    'adminName' => Auth::user()->login,
                    'id' => $id,
                    'action' => route('admin-ad-page-main-image-store', [$id, $item, $type])
                ]);
        }

        if ($operation == "delete"){
            try 
            {
                switch ($type){
                    case "main": {
                        AdPage::deleteMainImage($id);
                        break;
                    }
                    case "logo": {

                        break;
                    }
                    case "gallery":
                        AdPage::deleteGalleryItem($id, $item);
                        break;
                }
            }
            catch(\Exception $e){
                return redirect()->back()->withErrors($e->getMessage());
            }

            return redirect()->back();
        }
    }

    /**
     * 
     */
    public function storeImage(Request $request, int $id, string $type, int $item){
        
        $validatedData = $request->validate([
            'file' => 'required|image',
           ]);
    
           try{
               switch ($type){
                   case "main": {
                       AdPage::storeMainImage($request, $id);
                       break;
                   }
                   case "logo": {

                    break;
                   }
                   case "gallery": {
                    AdPage::storeGalleryItem($request, $id, $item);
                    break;
                   }
               }
           }
           catch(\Exception $e){
                return redirect()->back()->withErrors($e->getMessage());
           }
    
           return redirect()->back();
    }

    /**
     * 
     */
    public function storeGalleryAjax(Request $request, $id){

        $param = $request->validate([
            'file' => 'required|image',
        ]);

        return AdPage::storeGalleryAjax($request, $id);
    }
}
