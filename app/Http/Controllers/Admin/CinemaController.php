<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Cinema;
use App\Models\Admin\CinemaImageGallery;
use App\Models\Admin\Text;
use App\Models\Admin\Multitext;
use App\Models\Admin\SeoBlock;

class CinemaController extends Controller
{
    /**
     * 
     */
    public function indexCinemas()
    {
        return view('admin.pages.cinema-list',
        [
            'adminName' => Auth::user()->login,
            'model' => Cinema::all(),
        ]);
    }

    /**
     * 
     */
    public function indexCinema(int $id)
    {
        $cinema = Cinema::find($id);

        return view('admin.pages.cinema-page',
        [
            'adminName' => Auth::user()->login,
            'id' => $id,
            'model' => $cinema
        ]);

    }

    public function deleteCinema(int $id)
    {
        $cinema = Cinema::find($id);
        if ($cinema)
        {
            if ($id > 3){
                $cinema->deleteCinema();
            }
            else{
                $cinema->clearCinema();
            }
        }
        else{
            abort(404);
        }

        return redirect()->route('cinemas-index');
    }

    public function addCinema()
    {
        $text = Text::create();

        $description = Multitext::create();  
        
        $conditions = Multitext::create();      
    
        $seoBlock = SeoBlock::create();

        $cinema = Cinema::create([
            'name' =>  $text->id,
            'description' => $description->id,
            'conditions' => $conditions->id,
            'logo_location' => '',
            'logo_url' => '',
            'img_location' => '',
            'img_url' => '',
            'seoblock_id' => $seoBlock->id
        ]);

        $gallery = CinemaImageGallery::createIfNotExist($cinema->id);
        
        return redirect()->route('cinema-page-index', $cinema->id);
    }

    public function saveCinema(Request $request, int $id)
    {
        $validate = $this->validate( $request,
            [
                'nameRus' => 'required',
                'nameUa' => 'required',
                'descriptionRus' => 'required',
                'descriptionUa' => 'required',
                'conditionsRus' => 'required',
                'conditionsUa' => 'required',
                'seoUrl' => 'required',
                'seoTitle' => 'required',
                'seoKeywords' => 'required',
                'seoDescription' => 'required'
            ]);

        $cinema = Cinema::find($id);
        $cinema->_name->string_rus = $request->input('nameRus', '');
        $cinema->_name->string_ua = $request->input('nameUa', '');
        $cinema->_description->text_rus = $request->input('descriptionRus', '');
        $cinema->_description->text_ua = $request->input('descriptionUa', '');
        $cinema->_conditions->text_rus = $request->input('conditionsRus', '');
        $cinema->_conditions->text_ua = $request->input('conditionsUa', '');
        $cinema->seoblock->url = $request->input('seoUrl', '');
        $cinema->seoblock->title = $request->input('seoTitle', '');
        $cinema->seoblock->keywords = $request->input('seoKeywords', '');
        $cinema->seoblock->description = $request->input('seoDescription','');

        $cinema->_name->save();
        $cinema->_description->save();
        $cinema->_conditions->save();
        $cinema->seoblock->save();
        $cinema->save();
        
        return redirect()->route('cinema-page-index', [$cinema->id]);
    }
}
