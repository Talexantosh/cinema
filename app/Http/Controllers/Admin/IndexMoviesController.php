<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Admin\Movie;
use App\Models\Admin\CurrentMovie;
use App\Models\Admin\SoonMovie;

class IndexMoviesController extends Controller
{
    //
    public function index()
    {
        return view('admin.pages.movies',
        [
            'adminName' => Auth::user()->login,
            'currentMovies' => CurrentMovie::all(),
            'soonMovies' => SoonMovie::all()
        ]);
    }
}
