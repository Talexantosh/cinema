<?php

namespace App\Http\Controllers;

use App\Jobs\SendMailJob;
use App\Mail\MailingList;
use App\Models\Admin\Mailing;
use App\Models\Admin\MailingCustomer;
use App\Models\Admin\MailingTemplate;
use App\Models\AuthCustomer;
use App\Models\Customer;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;

class AdminController extends Controller
{
    public function index()
    {
       return view('admin.index', ['adminName' => Auth::user()->name]);
    }

    public function logout()
    {
        Auth::logout();
        return Redirect::back();
    }

    /**
     * 
     */
    public function customersIndex()
    {
        return view('admin.pages.customers-list', 
            [
                'adminName' => Auth::user()->login,
                'model' => Customer::paginate(10)
            ]);
    }

    /**
     * 
     */
    public function handleCustomer(Request $request, string $handle = 'view')
    {
        switch ($handle){
            case 'view':{
                return view('admin.pages.admin-customer-form', 
                    [
                        'adminName' => Auth::user()->login,
                        'model' => Customer::find($request->id)
                    ]);
            }
            case 'delete':{
                optional(Customer::find($request->id))
                ->delete();
                return redirect()->back();
            }
            case 'pagination':{
                return redirect()->back();
            }
            case 'mailing':{
                try{
                    $mailing = Mailing::find(1);
                    $template = MailingTemplate::all()->take(-5)->reverse();
                }
                catch (\Exception $e){
                    return view('admin.pages.mailing', 
                        [
                            'adminName' => Auth::user()->login,
                            'mailing' => optional($mailing),
                            'template' => optional($template)
                        ])->with('alert', $e->getMessage());
                }
                return view('admin.pages.mailing', 
                [
                    'adminName' => Auth::user()->login,
                    'mailing' => $mailing,
                    'template' => $template
                ]);
            }
            case "select":{
                return view('admin.pages.select-customer', 
                [
                    'adminName' => Auth::user()->login,
                    'model' => Customer::all(),
                    'check' => true
                ]);
            }
            case "delete-template":{
                optional(MailingTemplate::find($request->template_id))->delete();
                return redirect()->route('customers-list-handle', ['handle' => 'mailing']);
            }
        }
        
    }

    public function searchCustomer(Request $request)
    {
        return view('admin.layouts.customer-table', 
        [
            'model' => Customer::searchCustomers($request->search)
        ]);
    }

    /**
     * 
     */
    public function saveCustomer(Request $request, int $id)
    {
       /** валидации @todo */
       $validate = $request->validate(
        [
            'login' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);
        if($request->input('password') != ''){
            $validate = $request->validate(
                [
                    'password' => ['required', 'string', 'min:8', 'confirmed'],
                ]);
        }
        try{
            $model = Customer::saveCustomer($request, $id); 
            AuthCustomer::changeCustomer($model->id);
            return view('admin.layouts.customer-form', ['model' => $model]);
        }
        catch(\Exception $e){
            return view('admin.layouts.customer-form', ['model' => $model])
            ->with('alert', $e->getMessage());
        }
    }

    public function selectCustomer(Request $request)
    {
        try{
            Mailing::setSelectively(true);
            $result = $request->except(['_token']);
            MailingCustomer::deleteAll();
            MailingCustomer::createCustomers($result);
        }
        catch(\Exception $e){
            return redirect()->route('customers-list-handle', 'mailing')
            ->withErrors($e->getMessage());
        }
        
        return redirect()->route('customers-list-handle', 'mailing');
    }

    public function handleMailingForm(Request $request, string $handle)
    {
        switch ($handle){
            case 'template':{
                try{
                    MailingTemplate::addTemplateFile($request);
                }
                catch (\Exception $e){
                    Log::error($e->getMessage());
                }
                finally{
                    return view('admin.layouts.mailing-list-templates',
                    [
                        'template' => MailingTemplate::all()->take(-5)->reverse()
                    ]);
                }
            }
            case 'go':{
                $validate = $request->validate(
                    [
                        'template' => 'required'
                    ]
                );
 
                try{
                    $mailing = Mailing::premailing($request);

                    if(!($recipients = Mailing::getRecipientsMail()))
                        throw new \Exception('Отсутствует доступ к выбору пользователей');
                    
                    foreach ($recipients as $recipient){
                        dispatch(new SendMailJob($recipient->email,
                        addslashes(Mailing::find(1)->_template->content)));    
                    }

                    return response(optional($mailing->_template)->name);
                }
                catch(\Exception $e){
                    return response($e->getMessage(), 503);
                }
            }
            case 'select':{
                try{
                    Mailing::setSelectively(true);
                    $result = $request->except(['_token']);
                    MailingCustomer::deleteAll();
                    MailingCustomer::createCustomers($result);
                }
                catch(\Exception $e){
                    return redirect()->route('customers-list-handle', 'mailing')
                    ->withErrors($e->getMessage());
                }
                
                return redirect()->route('customers-list-handle', 'mailing');
            }
            case 'progress':{
                try{
                    $response = Mailing::all(['progress', 'qty'])->first()->toArray();
                    return response(json_encode($response));
                }
                catch (\Exception $e){
                    return response($e->getMessage(), 503);
                }
                
            }
        }
    }
}
