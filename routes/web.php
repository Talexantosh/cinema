<?php

use App\Http\Controllers\BannerController;
use App\Http\Middleware\CustomerMiddleware;
use Faker\Provider\bn_BD\Address;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['namespace' => 'App\Http\Controllers'], function(){

    Route::middleware(CustomerMiddleware::class)->group(function(){
        Route::get('/user/cabinet/index', 'FrontController@indexCabinet')->name('customer-cabinet-index');
        Route::get('/user/logout', 'FrontController@userLogout')->name('customer-logout');
        Route::post('/user/{id}/data/change', 'FrontController@changeUser')->name('front-customer-save-form');
        Route::post('/movie/{schedule}/ticket/booking', 'FrontController@bookTicket')->name('book-ticket-form');
        
    });
    Route::get('/user/booking/index', 'FrontController@indexBooking')->name('booking');
    Route::get('/user/login/index', 'FrontController@indexLogin')->name('customer-login-index');
    Route::get('/user/register/index', 'FrontController@indexRegister')->name('customer-register-index');
    Route::post('/user/login/handle', 'FrontController@handleLogin')->name('customer-login-handle');
    Route::post('/user/register/handle', 'FrontController@handleRegister')->name('customer-register-handle');
    Route::get('/index', 'FrontController@index')->name(('front-main-index'));
    Route::get('/posters/index', 'FrontController@indexPosters')->name('front-posters-index');
    Route::get('/schedule/index', 'FrontController@indexSchedule')->name('front-schedule-index');
    Route::get('/soon/index', 'FrontController@indexSoon')->name('front-soon-index');
    Route::get('/cinemas/index', 'FrontController@indexCinemas')->name('front-cinemas-index');
    Route::get('/cinema/{id}/index', 'FrontController@indexCinema')->name('front-cinema-index');
    Route::get('/cinema/hall/{id}/index', 'FrontController@indexHall')->name('front-cinema-hall-index');
    Route::get('/cinema/{cinemaId}/hall/{hallId}/movie/{movieId}/ticket/booking', 'FrontController@ticketBooking')->name('front-ticket-booking');
    Route::get('/promotion/{id}/index', 'FrontController@indexPromotion')->name('front-promotion-index');
    Route::get('/news/index', 'FrontController@indexNews')->name('front-page-news-index');
    Route::get('/ad-page/{id}/index', 'FrontController@indexAdvertising')->name('front-page-advertising-index');
    Route::get('/about/index', 'FrontController@indexAbout')->name('front-page-about-index');
    Route::get('/movie/{id}/page', 'FrontController@pageMovie')->name('front-page-movie');
    Route::post('/ajax/movie/schedules', 'FrontController@handlePostAjaxForMovieSchedule')->name('post-ajax-movie-schedules');
    Route::get('/promotions/index', 'FrontController@indexPromotions')->name('front-promotions-index');
    Route::post('/ajax/pagination/page/content/news/index', 'FrontController@handlePostAjaxForNewsPagination')->name('post-ajax-news-pagination');
    
});

Route::group(['middleware' => 'auth','namespace' => 'App\Http\Controllers'], function(){
    Route::get('/admin', 'AdminController@index')->name('admin-statistic');
    Route::match(['get', 'post'], '/logout', 'AdminController@logout')->name('admin-logout');
    Route::resource('/admin/banners', Admin\BannerController::class)->name('index', 'banners');
    Route::post('admin/banners/top/gallery/delete/{id}', 'Admin\TopSladerGalleryController@delete')->name('top-gallery-delete');
    Route::get('admin/banners/top/gallery/upload/{id}', 'Admin\FileUploadController@index')->name('top-gallery-upload');
    Route::post('admin/banners/top/gallery/update/{id}', 'Admin\FileUploadController@store')->name('top-gallery-store');
    Route::get('admin/banners/top/gallery/add/item', 'Admin\TopSladerGalleryController@addItem')->name('top-gallery-add');
    Route::post('admin/banners/top/gallery/save', 'Admin\TopSladerGalleryController@save');
    Route::post('admin/banners/background/save', 'Admin\BackgroundController@save')->name('banner-back-save');
    Route::get('admin/banners/background/delete', 'Admin\BackgroundController@delete')->name('banner-back-delete');
    Route::get('admin/banners/background/upload/index', 'Admin\BackgroundFileUploadController@index')->name('banner-back-upload-index');
    Route::post('admin/banners/background/upload/store', 'Admin\BackgroundFileUploadController@store')->name('banner-back-upload-store');
    Route::post('admin/banners/bottom/gallery/delete/{id}', 'Admin\BottomSladerGalleryController@delete')->name('bottom-gallery-delete');
    Route::get('admin/banners/bottom/gallery/upload/{id}', 'Admin\BottomSladerGalleryFilesController@index')->name('bottom-gallery-upload');
    Route::post('admin/banners/bottom/gallery/update/{id}', 'Admin\BottomSladerGalleryFilesController@store')->name('bottom-gallery-store');
    Route::get('admin/banners/bottom/gallery/add/item', 'Admin\BottomSladerGalleryController@addItem')->name('bottom-gallery-add');
    Route::post('admin/banners/bottom/gallery/save', 'Admin\BottomSladerGalleryController@save');
    Route::get('admin/movie/page/{id}/index', 'Admin\MovieController@index')->name('movie-page-index');
    Route::get('admin/movie/page/{id}/in/release', 'Admin\MovieController@inRelease')->name('soon-movie-in-release');
    Route::post('admin/movie/page{id}/save', 'Admin\MovieController@save')->name('movie-page-save');
    Route::get('admin/movie/page/{id}/image/main/index', 'Admin\MovieMainImageController@index')->name('movie-main-image-index');
    Route::post('admin/movie/page/{id}/image/main/store', 'Admin\MovieMainImageController@store')->name('movie-main-image-store');
    Route::get('admin/movie/page/{id}/image/main/delete', 'Admin\MovieController@deleteMainImage')->name('movie-main-image-delete');
    Route::get('admin/movie/page/{id}/reset', 'Admin\MovieController@pageReset')->name('movie-page-reset');
    Route::get('admin/movie/page/{id}/delete', 'Admin\MovieController@pageDelete')->name('movie-page-delete');
    Route::get('admin/movie/page/{id}/image/gallery/{item}/index', 'Admin\MovieGalleryUploadController@index')->name('movie-item-gallery-index');
    Route::get('admin/movie/page/{id}/image/gallery/{item}/delete', 'Admin\MovieGalleryUploadController@delete')->name('movie-item-gallery-delete');
    Route::post('admin/movie/page/{id}/image/gallery/{item}/store', 'Admin\MovieGalleryUploadController@store')->name('movie-item-gallery-store');
    Route::get('admin/movies/index', 'Admin\IndexMoviesController@index')->name('movies-index');
    Route::get('admin/movie/current/add', 'Admin\MovieController@addCurrentMovie')->name('add-current-movie');
    Route::get('admin/movie/soon/add', 'Admin\MovieController@addSoonMovie')->name('add-soon-movie');

    Route::get('admin/cinemas/index', 'Admin\CinemaController@indexCinemas')->name('cinemas-index');
    Route::get('admin/cinema/page/{id}/index', 'Admin\CinemaController@indexCinema')->name('cinema-page-index');
    Route::get('admin/cinema/page/{id}/delete', 'Admin\CinemaController@deleteCinema')->name('cinema-page-delete');
    Route::get('admin/cinema/page/add', 'Admin\CinemaController@addCinema')->name('cinema-page-add');
    Route::post('admin/cinema/page/{id}/save', 'Admin\CinemaController@saveCinema')->name('cinema-page-save');
    Route::get('admin/cinema/page/{id}/image/load/{type}/index', 'Admin\CinemaImageController@index')->name('cinema-image-index');
    Route::get('admin/cinema/page/{id}/image/load/{type}/item/{item}/index', 'Admin\CinemaImageController@index')->name('cinema-image-index-patch');
    Route::post('admin/cinema/page/{id}/logo/store', 'Admin\CinemaImageController@storeLogo')->name('load-cinema-logo');
    Route::get('admin/cinema/page/{id}/logo/delete', 'Admin\CinemaImageController@deleteLogo')->name('delete-cinema-logo');
    Route::post('admin/cinema/page/{id}/top/banner/store', 'Admin\CinemaImageController@storeTopBanner')->name('load-cinema-top-banner');
    Route::get('admin/cinema/page/{id}/top/banner/delete', 'Admin\CinemaImageController@deleteTopBanner')->name('delete-cinema-top-banner');
    Route::post('admin/cinema/page/{id}/image/gallery/{item}/store', 'Admin\CinemaImageController@storeGalleryItem')->name('load-cinema-gallery-item');
    Route::get('admin/cinema/page/{id}/image/gallery/{item}/delete', 'Admin\CinemaImageController@deleteGalleryItem')->name('delete-cinema-gallery-item');
    Route::get('admin/cinema/hall/page/{id}/index', 'Admin\HallController@indexHall')->name('cinema-hall-index');
    Route::get('admin/cinema/hall/page/{id}/delete', 'Admin\HallController@deleteHall')->name('cinema-hall-delete');
    Route::get('admin/cinema/{id}/hall/add', 'Admin\HallController@addHall')->name('cinema-hall-add');
    Route::post('admin/cinema/hall/page/{id}/save', 'Admin\HallController@saveHall')->name('cinema-hall-save');
    Route::get('admin/cinema/hall/page/{id}/image/load/{type}/index', 'Admin\HallImageController@index')->name('hall-image-index');
    Route::get('admin/cinema/hall/page/{id}/image/load/{type}/item/{item}/index', 'Admin\HallImageController@index')->name('hall-image-index-gallery');
    Route::post('admin/cinema/hall/page/{id}/schema/store', 'Admin\HallImageController@storeSchema')->name('load-hall-schema');
    Route::get('admin/cinema/hall/page/{id}/schema/delete', 'Admin\HallImageController@deleteSchema')->name('delete-hall-schema');
    Route::post('admin/cinema/hall/page/{id}/top/banner/store', 'Admin\HallImageController@storeTopBanner')->name('load-hall-top-banner');
    Route::get('admin/cinema/hall/page/{id}/top/banner/delete', 'Admin\HallImageController@deleteTopBanner')->name('delete-hall-top-banner');
    Route::post('admin/cinema/hall/page/{id}/image/gallery/{item}/store', 'Admin\HallImageController@storeGalleryItem')->name('load-hall-gallery-item');
    Route::get('admin/cinema/hall/page/{id}/image/gallery/{item}/delete', 'Admin\HallImageController@deleteGalleryItem')->name('delete-hall-gallery-item');
    
    Route::get('admin/news/index', 'Admin\NewsController@index')->name('admin-news-index');
    Route::get('admin/news/page/{id}/index', 'Admin\NewsController@indexNews')->name('admin-news-page-index');
    Route::get('admin/news/page/{id}/delete', 'Admin\NewsController@deleteNews')->name('admin-news-page-delete');
    Route::get('admin/news/create', 'Admin\NewsController@createNews')->name('admin-news-page-create');
    Route::post('admin/news/page/{id}/save', 'Admin\NewsController@saveNews')->name('admin-news-page-save');
    Route::get('admin/news/{id}/gallery/item/{item}/delete', 'Admin\NewsController@newsGalleryItemDelete')->name('news-item-gallery-delete');
    Route::get('admin/news/{id}/gallery/item/{item}/load', 'Admin\NewsController@newsGalleryItemLoad')->name('news-item-gallery-index');
    Route::post('admin/news/{id}/gallery/item/{item}/store', 'Admin\NewsController@newsGalleryItemStore')->name('news-item-gallery-store');

    Route::get('admin/promotions/index', 'Admin\PromotionController@promotionsIndex')->name('admin-promotions-index');
    Route::get('admin/promotion/create', 'Admin\PromotionController@promotionCreate')->name('admin-promotion-create');
    Route::get('admin/promotion/page/{id}/index', 'Admin\PromotionController@promotionIndex')->name('admin-promotion-index');
    Route::get('admin/promotion/page/{id}/delete', 'Admin\PromotionController@promotionDelete')->name('admin-promotion-delete');
    Route::post('admin/promotion/page/{id}/save', 'Admin\PromotionController@promotionSave')->name('admin-promotion-save');
    Route::get('admin/promotion/{id}/main/image/delete', 'Admin\PromotionController@promotionMainImageDelete')->name('promotion-main-image-delete');
    Route::get('admin/promotion/{id}/main/image/load', 'Admin\PromotionController@promotionMainImageLoad')->name('promotion-main-image-index');
    Route::get('admin/promotion/{id}/gallery/item/{item}/delete', 'Admin\PromotionController@promotionGalleryItemDelete')->name('promotion-item-gallery-delete');
    Route::get('admin/promotion/{id}/gallery/item/{item}/load', 'Admin\PromotionController@promotionGalleryItemLoad')->name('promotion-item-gallery-index');
    Route::post('admin/promotion/{id}/gallery/item/{item}/store', 'Admin\PromotionController@promotionGalleryItemStore')->name('promotion-item-gallery-store');

    Route::get('admin/ad-pages/index', 'Admin\AdPageController@indexAdPages')->name('admin-ad-pages-index');
    Route::get('admin/ad-pages/{id}/page/index', 'Admin\AdPageController@indexAdPage')->name('admin-ad-page-index');
    Route::get('admin/ad-pages/{id}/page/{type}/{operation}', 'Admin\AdPageController@handleAdPage')->name('admin-ad-page-handle');
    Route::post('admin/ad-pages/{id}/page/{type}/save', 'Admin\AdPageController@saveAdPage')->name('admin-ad-page-save');
    Route::get('admin/ad-pages/{id}/page/{type}/image/{item}/{operation}', 'Admin\AdPageController@handleImage')->name('admin-ad-page-image-handle');
    Route::post('admin/ad-pages/{id}/page/{type}/image/{item}store', 'Admin\AdPageController@storeImage')->name('admin-ad-page-main-image-store');
    Route::post('admin/ad-pages/page/{id}/gallery/ajax/store', 'Admin\AdPageController@storeGalleryAjax')->name('admin-store-image-via-dropzone');

    Route::get('admin/customers/listing', 'AdminController@customersIndex')->name('customers-list-index');
    Route::get('admin/customer/{handle}', 'AdminController@handleCustomer')->name('customers-list-handle');
    Route::post('admin/customer/search', 'AdminController@searchCustomer')->name('customer-search');
    Route::post('admin/customer/{id}/save', 'AdminController@saveCustomer')->name('admin-customer-save-form');
    Route::post('admin/customers/mailing/{handle}', 'AdminController@handleMailingForm')->name('admin-customers-mailing-handle');
   
});




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
