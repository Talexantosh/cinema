@extends('admin.layouts.app')

@section('Scripts')
@endsection

@section('Fonts')
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
@endsection

@section('Styles')
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
     <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('dist/css/bootstrap.min.css') }}" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <!-- Custom styles for this template -->
    @yield('custom_styles')
    <!--<link href="{{ asset('admin/css/pricing.css') }}" rel="stylesheet">-->
@endsection

@section('content')
<body class="hold-transition sidebar-mini layout-fixed">
<header>
    <div class="d-flex flex-column flex-md-row align-items-center pb-3 mb-4 border-bottom" style="height: 106px;">
      <nav class="d-inline-flex mt-2 mt-md-0 ms-md-auto">
        <a class="me-3 py-2 text-dark text-decoration-none" href="#">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" class="px-3 svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="24"><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z"></path></svg>
            {{ $adminName }}</a>
        <a class="me-3 py-2 text-dark text-decoration-none" href="{{route('admin-logout')}}">
            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="power-off" class="px-4 svg-inline--fa fa-power-off fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="24"><path fill="currentColor" d="M400 54.1c63 45 104 118.6 104 201.9 0 136.8-110.8 247.7-247.5 248C120 504.3 8.2 393 8 256.4 7.9 173.1 48.9 99.3 111.8 54.2c11.7-8.3 28-4.8 35 7.7L162.6 90c5.9 10.5 3.1 23.8-6.6 31-41.5 30.8-68 79.6-68 134.9-.1 92.3 74.5 168.1 168 168.1 91.6 0 168.6-74.2 168-169.1-.3-51.8-24.7-101.8-68.1-134-9.7-7.2-12.4-20.5-6.5-30.9l15.8-28.1c7-12.4 23.2-16.1 34.8-7.8zM296 264V24c0-13.3-10.7-24-24-24h-32c-13.3 0-24 10.7-24 24v240c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24z"></path></svg>
        </a>
      </nav>
    </div>
</header>
    <!-- Main Sidebar Container -->
<div class="container-fluid">
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="d-flex justify-content-center">
      <img src="{{ asset('storage/files/images/logo.svg') }}" class="brand-image elevation-3 img-thumbnail align-items-center" width="100px" alt="Logo"  style="opacity: .8" />
    </div>
    
    <!-- Sidebar -->
    <div class="sidebar">      
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item menu-open">
            <a href="{{route('admin-statistic')}}" class="nav-link" ><!--active">-->
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Статистика
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('banners') }}" class="nav-link">
              <i class="nav-icon far fa-image"></i>
              <p>
                Баннеры/Слайдеры
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('movies-index') }}" class="nav-link">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Фильмы
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('cinemas-index') }}" class="nav-link">
              <i class="nav-icon fas fa-columns"></i>
              <p>
                Кинотеатры
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin-news-index') }}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Новости
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin-promotions-index') }}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Акции
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{ route('admin-ad-pages-index') }}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Страницы
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('customers-list-index')}}" class="nav-link">
              <i class="nav-icon fas fa-table"></i>
              <p>
                Пользователи
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('customers-list-handle', ['handle' => 'mailing'])}}" class="nav-link">
              <i class="nav-icon far fa-envelope"></i>
              <p>
                Рассылка
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
  </div>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    
    <!-- /.content-header -->
    <div class="wrapper">
  <!-- Top navbar -->
<!--</div>-->
    @yield('Container')
  <!-- Navbar -->
  <!--<div class="container py-3">-->
    </div>
<!-- ./wrapper -->
  </div>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<!--<script src="dist/js/adminlte.js"></script>-->
<!-- AdminLTE for demo purposes -->
<!-- <script src="dist/js/demo.js"></script>-->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<!--<script type="text/javascript" src="/admin/dist/js/jquery.colorbox-min.js"></script>-->
<!--<script src="https://cdn.tiny.cloud/1/jxsqeq85qzdwuqqqruya91jqsrhqtxykhxtks6sn0t1kn69g/tinymce/5/tinymce.min.js"
        referrerpolicy="origin"></script>
<script type="text/javascript" src="/packages/barryvdh/elfinder/js/standalonepopup.js"></script>-->
    <script src="{{ asset('dist/js/admin.js') }}"></script>
</body>
@yield('custom_scripts')
@endsection
