@foreach ($template as $index => $item)
<div class="row">
    <div class="col" style="max-width: fit-content;">
        <div class="pl-5" >
            <input class="form-check-input" name="template" type="checkbox" value="{{$item->id}}" id="check{{$item->id}}" data-checkbox>
        </div>
    </div>
    <div class="col-6" id="name{{$item->id}}">
        {{$item->name}}
    </div>
    <div class="col-4" style="max-width: fit-content;">
        <a class="link-danger" href="{{route('customers-list-handle', ['handle' => 'delete-template', 'template_id' => $item->id])}}">Удалить</a>
    </div>
</div>
@endforeach