<!-- [ 
       'movie' => Model::find(id),
      ] -->
            <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                <span class="fs-6 mr-5">Название фильма:</span>
                <input type="text" id="nameRus" name="nameRus" value="{{ $movie->text->string_rus  }}" class="form-control" />
                    @if($errors->has('nameRus'))
                        <div class="error text-danger small">{{ $errors->first('nameRus') }}</div>
                    @endif
            </div>
            <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                <span class="fs-6 mr-5" >Описание:</span>
                <textarea class="form-control" rows="5" cols="90" id="descriptionRus" name="descriptionRus">{{ $movie->multitext->text_rus }}</textarea>
                    @if($errors->has('descriptionRus'))
                        <div class="error text-danger small">{{ $errors->first('descriptionRus') }}</div>
                    @endif
                <br>
            </div>