<!-- ['title' => 'На главной верх',
       'idSwitch' => 'switch-top-slader',
       'switchState' => 1,
      'actionForm' => action('App\Http\Controllers\Admin\TopSladerGalleryController@save'),
       'sizeImage' => 'Размер: 1000х190',
        'countImages' => 5,
        'actionDelete' => route('top-gallery-delete', $i),
        'actionUpdate' => route('top-gallery-upload', $i),
        'fileLocation' => [],
         'urlValue' => [],
         'textValue' => [],
         'speedSlader' => 0,
         'actionAdd' => top-gallery-add
         ] -->
         <h4 class="text-center">{{ $title  }}</h4>
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        <form action="{{ $actionForm }}" method="post" id="bottom-slader-gallery">
        @csrf
            <div class="d-flex justify-content-end">
                <label class="switch">
                    <input type="checkbox"  id="{{ $idSwitch }}" name="isEnable" >
                    <span class="slider round"></span>
                </label>
            </div>
            <div>
                <p>{{ $sizeImage }}</p>    
            </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
              @for ($i = 0; $i < $countImages; $i++)
              <div class="col">
                <div class="card shadow-sm">
                      <div class="d-flex justify-content-end">
                        <form></form>
                          <form id="deleteBottomItem{{$i}}" action="{{ route($actionDelete, $i + 1) }}" method="post">
                              @csrf   
                              <button type="submit" form="deleteBottomItem{{$i}}" value="{{$i}}">
                                      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                              </button>
                          </form>
                      </div>
                      <img src="{{ asset($urlValue[$i]->url) }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                  <div class="card-body">
                 <!-- ADD TOP SLADER -->   
                    <div class="d-flex justify-content-center align-items-center">
                        <form id="updateBottomItem{{$i}}" action="{{ route($actionUpdate, $i + 1) }}" method="get">
                          @csrf
                      <!--<a href="#" class="btn btn-sm btn-outline-secondary">Добавить</a>-->
                      @if($fileLocations[$i]->image_location == "") 
                        <button type="submit" form="updateBottomItem{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" > Добавить </button>
                      @else
                        <button type="submit" form="updateBottomItem{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" disabled > Добавить </button>
                      @endif
                           
                        </form>
                  </div>
                  
                  <!-- END ADD TOP SLADER -->
                    <div>
                        <br/>
                      <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                          <span class="fs-6">URL:</span>
                          <input type="url" id="url{{$i}}" name="url{{$i}}" value="{{ asset($urlValue[$i]->url) }}" class="form-control" />
                      </div>
                      <div class="d-none justify-content-center align-items-center input-group input-group-sm mb-3">
                          <span class="fs-8">Text:</span>
                          <input type="text" id="text{{$i}}" name="text{{$i}}" value="{{ $textValue[$i]->text }}" class="form-control" /><br>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endfor    
              <!-- -->
              <div class="col">
                <div class="card shadow-sm">
                  <div class="card-body">  
                    <div class="d-flex justify-content-center align-items-center">
                        <form id="addBottomItem" action="{{ route($actionAdd) }}" method="get">
                          @csrf
                      <!--<a href="#" class="btn btn-sm btn-outline-secondary">Добавить</a>-->
                          <button type="submit" form="addBottomItem" class="btn btn-sm btn-outline-secondary">
                            <div><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="57"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path></svg></div>
                            <div>Добавить<br/>фото</div>
                          </button> 
                        </form>
                    </div>
                  </div>
                </div>
              </div>
              <!-- -->    
            </div>
            <div class="d-flex justify-content-center align-items-center">
                <span style="width:200px;">
                    <select id="{{ $idSpeedSlader }}" name="speedSlader" form="bottom-slader-gallery">
                        <option value="0">5 c</option>
                        <option value="1">10 c</option>
                        <option value="2">20 c</option>
                        <option value="3">30 c</option>
                    </select>
                </span>
                <span>
                    <button class="btn  btn-outline-secondary" type="submit" form="bottom-slader-gallery">Сохранить
                    </button>
                </span>
            </div>       
      </form>
    </div>
</div>
<script type="text/javascript">
        document.getElementById("{{ $idSpeedSlader }}").value = "{{ $speedBottomSlader }}";
        if (<?php echo $switchBottomState; ?> == 1)
            document.getElementById("{{ $idSwitch }}").checked = true;
        else 
            document.getElementById("{{ $idSwitch }}").checked = false;
</script>
