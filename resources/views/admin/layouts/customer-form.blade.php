<div class="container">
    <form  id="customer-form">
        @csrf
    <div class="row">
        <div class="col-2">
            <label for="firstname" class="form-control">Имя</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="firstname" value="{{isset($model)? $model->firstname: ''}}" id="firstname" placeholder="Имя">
            @if($errors->has('firstname'))
                <div class="error text-danger small">{{ $errors->first('firstname') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="ru" class="form-control">Язык</label>
        </div>
        <div class="col-4">
            <div class="form-check form-check-inline">
                @if (optional($model)->language == 'ru')
                    <input class="form-check-input" type="radio" name="language" id="ru" value="ru" checked>
                @else
                    <input class="form-check-input" type="radio" name="language" id="ru" value="ru">
                @endif
                <label class="form-check-label" for="ru">Русский</label>
            </div>
                <div class="form-check form-check-inline">
                @if (optional($model)->language == 'ua')
                <input class="form-check-input" type="radio" name="language" id="ua" value="ua" checked>
                @else 
                <input class="form-check-input" type="radio" name="language" id="ua" value="ua">
                @endif
                <label class="form-check-label" for="ua">Украинский</label>
                </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="lastname" class="form-control">Фамилия</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="lastname" value="{{isset($model)? $model->lastname: ''}}" id="lastname" placeholder="Фамилия">
            @if($errors->has('lastname'))
                <div class="error text-danger small">{{ $errors->first('lastname') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="male" class="form-control">Пол</label>
        </div>
        <div class="col-4">
            <div class="form-check form-check-inline">
                @if (optional($model)->gender == 'male')
                <input class="form-check-input" type="radio" name="gender" id="male" value="male" checked>
                @else
                <input class="form-check-input" type="radio" name="gender" id="male" value="male">
                @endif
                <label class="form-check-label" for="male">Мужской</label>
            </div>
            <div class="form-check form-check-inline">
                @if (optional($model)->gender == 'female')
                <input class="form-check-input" type="radio" name="gender" id="female" value="female" checked>
                @else
                <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                @endif
                <label class="form-check-label" for="female">Женский</label>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="login" class="form-control">Псевдоним</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="login" value="{{optional($model)->login}}" id="login" placeholder="Псевдоним">
            @if($errors->has('login'))
                <div class="error text-danger small">{{ $errors->first('login') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="phone" class="form-control">Телефон</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="phone" value="{{optional($model)->phone}}" id="phone" placeholder="Телефон">
            @if($errors->has('phone'))
                <div class="error text-danger small">{{ $errors->first('phone') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="email" class="form-control">E-mail</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="email" value="{{optional($model)->email}}" id="email" placeholder="E-mail">
            @if($errors->has('email'))
                <div class="error text-danger small">{{ $errors->first('email') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="dob" class="form-control">Дата рождения</label>
        </div>
        <div class="col-4">
            <input type="date" class="form-control" name="dob" value="{{optional($model)->dob}}" id="dob">
            @if($errors->has('dob'))
                <div class="error text-danger small">{{ $errors->first('dob') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="address" class="form-control">Адрес</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="address" value="{{optional($model)->address}}" id="address" placeholder="Адрес">
            @if($errors->has('address'))
                <div class="error text-danger small">{{ $errors->first('address') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="city" class="form-control">Город</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="city" value="{{optional($model)->city}}" id="city" placeholder="Город">
            @if($errors->has('city'))
                <div class="error text-danger small">{{ $errors->first('city') }}</div>
            @endif
        </div>
    </div>
    <div class="row">
        <div class="col-2">
            <label for="password" class="form-control">Пароль</label>
        </div>
        <div class="col-4">
            <input type="password" class="form-control" name="password" value="" id="password" placeholder="Пароль">
            @if($errors->has('password'))
                <div class="error text-danger small">{{ $errors->first('password') }}</div>
            @endif
        </div>
        <div class="col-2">
            <label for="confirmpw" class="form-control">Подтвердить пароль</label>
        </div>
        <div class="col-4">
            <input type="password" class="form-control" name="confirmpw" id="confirmpw" placeholder="Подтвердить пароль">
            @if($errors->has('confirmpw'))
                <div class="error text-danger small">{{ $errors->first('confirmpw') }}</div>
            @endif
        </div>
    </div>    
    <div class="row">
        <div class="col-2">
            <label for="card" class="form-control">Номер карты</label>
        </div>
        <div class="col-4">
            <input type="text" class="form-control" name="card" value="{{optional($model)->card_number}}" id="card" placeholder="Номер карты">
            @if($errors->has('card'))
                <div class="error text-danger small">{{ $errors->first('card') }}</div>
            @endif
        </div>
        <br/><br/><br/><br/>
    </div>     
    <div class="row">
        <div class="col-10 text-center">
            <button id="submit-button" type="submit" class="btn btn-secondary">
            <span class="submit-spinner submit-spinner_hide"></span>Сохранить</button>
        </div>   
    </div>
    </form>
</div>  