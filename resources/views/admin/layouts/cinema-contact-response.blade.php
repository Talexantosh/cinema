<!-- todo address card -->
<div class="d-flex justify-content-end">
    @if(isset($alert))
        <div class="alert alert-danger alert-dismissible" style="font-size: small; width:fit-content">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Ошибка!</strong> {{$alert}}
        </div>
    @endif
    <span>ВКЛ: </span>
    <label class="switch">
        @if($model->enabled)
            <input type="checkbox"  id="slader-status" name="isEnable" checked>
        @else
            <input type="checkbox"  id="slader-status" name="isEnable">
        @endif
        <span class="slider round"></span>
    </label>
</div>
<div class="content-rus-1 rus">
    <div class="d-flex justify-content-start align-items-center input-group input-group-sm mb-3">
        <span class="fs-6 mr-5">Название Кинотеатра:</span>
            <input type="text" name="nameRus" value="{{ optional($model->_name)->string_rus  }}" class="form-control" />
        @if(isset($messages['nameRus']))
            <div class="error text-danger small" style="font-size: small;">{{ $messages['nameRus'][0] }}</div>
        @endif
    </div>
    <br/>
    <div class="d-flex justify-content-start align-items-center input-group input-group-sm mb-3">
        <span class="fs-6 mr-5" >Адрес:</span>
        <textarea class="form-control" rows="0" cols="0" name="descriptionRus">{{ optional($model->_descriptionText)->text_rus }}</textarea>
        @if(isset($messages['descriptionRus']))
            <div class="error text-danger small" style="font-size: small;">{{ $messages['descriptionRus'][0] }}</div>
        @endif
    </div>
</div>  
<div class="content-ua-1 ua">
    <div class="d-flex justify-content-start align-items-center input-group input-group-sm mb-3">
        <span class="fs-6 mr-5">Название Кинотеатра:</span>
        <input type="text" name="nameUa" value="{{ optional($model->_name)->string_ua  }}" class="form-control" />
        @if(isset($messages['nameUa']))
            <div class="error text-danger small">{{ $messages['nameUa'][0] }}</div>
        @endif
    </div>
    <div class="d-flex justify-content-start align-items-center input-group input-group-sm mb-3">
        <span class="fs-6 mr-5" >Адрес:</span>
        <textarea class="form-control" rows="0" cols="0" name="descriptionUa">{{ optional($model->_descriptionText)->text_ua }}</textarea>
        @if(isset($messages['descriptionUa']))
            <div class="error text-danger small">{{ $messages['descriptionUa'][0] }}</div>
        @endif
    </div>
</div>
<div class="d-flex justify-content-start align-items-center input-group input-group-sm mb-3">
    <span class="fs-6 mr-5" >Координаты на Карте:</span>
    <textarea class="form-control" rows="0" cols="0" name="address">{{ optional($model->_address)->text_rus }}</textarea>
    @if(isset($messages['address']))
        <div class="error text-danger small" style="font-size: small;">{{ $messages['address'][0] }}</div>
    @endif
</div>
