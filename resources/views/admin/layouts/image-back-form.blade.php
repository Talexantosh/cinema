<!-- ['title' => 'На главной верх',
      'actionForm' => action('App\Http\Controllers\Admin\TopSladerGalleryController@save'),
       'sizeImage' => 'Размер: 2000х3000',
        'actionDelete' => route('top-gallery-delete'),
        'actionUpdate' => route('top-gallery-upload'),
        'urlValue' => '',
        'fileLocations' => '',
        'state' => 1
         ] -->
         <h4 class="text-center">{{ $title  }}</h4>
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        <form action="{{ $actionForm }}" method="post" id="background-image">
        @csrf
            <div>
                <p>{{ $sizeImage }}</p>    
            </div>
            
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                <!-- -->
              <div class="col-4">
                <div class="card shadow-sm">
                  <div class="card-body">  
                    <div class="d-flex justify-content-start align-items-center fst-normal">
                        <div>
                            <div  class="fst-normal">
                              <input type="radio" id="photo"
                              name="photo" value="photo" onclick="this.form.submit()">
                              <label for="photo" class="fw-light fs-6" >Фото на фоне</label>
                            </div>
                            <div  class="fst-normal">
                              <input type="radio" id="color"
                              name="photo" value="color" onclick="this.form.submit()" checked>
                              <label for="color" class="fw-light fs-6">Просто фон</label>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- -->   
              <div class="col-4">
                <div class="card shadow-sm">
                      <div class="d-flex justify-content-end">
                          <!--<form id="item-delete" action="{{ $actionDelete }}" method="get">-->
                              <button type="submit" form="item-delete" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                              </button>
                          <!--</form>-->
                      </div>
                      <img src="{{ asset($urlValue) }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                  <div class="card-body">   
                    <div class="d-flex justify-content-center align-items-center">
                        <!--<form id="updateItem" action="{{ $actionUpdate }}" method="get">-->
                            <button type="submit" form="updateItem" value="" class="btn btn-sm btn-outline-secondary" @if($fileLocations != "") disabled @endif > Добавить </button>
                        <!--</form>-->
                    </div>
                  </div>
                </div>
              </div>
            </div> 
         </div>       
      </form>
    </div>
</div>
 <form id="item-delete" action="{{ $actionDelete }}" method="get">
   @csrf
 </form>
 <form id="updateItem" action="{{ $actionUpdate }}" method="get">
    @csrf
</form>
<script type="text/javascript">
        if (<?php echo $state; ?> == 1)
            document.getElementById("photo").checked = true;
        else 
            document.getElementById("color").checked = true;
</script>