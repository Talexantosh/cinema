<table class="display table table-striped table-bordered table-sm " id="customer-table" cellspacing="0" width="100%">
                    <thead>
                        <tr class="table-secondary">
                        @if (isset($check))
                        <th class="th-sm" scope="col">&nbsp;&nbsp;&nbsp;</th>
                        @endif
                        <th class="th-sm" scope="col">Дата регистрации</th>
                        <th class="th-sm" scope="col">Дата рождения</th>
                        <th class="th-sm" scope="col">Email</th>
                        <th class="th-sm" scope="col">Телефон</th>
                        <th class="th-sm" scope="col">ФИО</th>
                        <th class="th-sm" scope="col">Псевдоним</th>
                        <th class="th-sm" scope="col">Город</th>
                        @if (!isset($check))
                        <th class="th-sm" scope="col">Действия</th>
                        @endif
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($model as $index => $customer)
                        <tr>
                        @if (isset($check))
                        <td><input type="checkbox" name="check{{optional($customer)->id}}" value="{{optional($customer)->id}}" id="check{{optional($customer)->id}}"></td>
                        @endif    
                        <td>{{ optional($customer)->created_at }}</td>
                        <td>{{ optional($customer)->dob}}</td>
                        <td>{{ optional($customer)->email}}</td>
                        <td>{{ optional($customer)->phone}}</td>
                        <td>{{ optional($customer)->firstname . '  ' . optional($customer)->lastname}}</td>
                        <td>{{ optional($customer)->login}}</td>
                        <td>{{ optional($customer)->city}}</td>
                        @if (!isset($check))
                        <td>
                            <a href="{{ route('customers-list-handle', ['handle' => 'view', 'id' =>optional($customer)->id]) }}" style="margin: 0px 10px;" >
                                <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" class="svg-inline--fa fa-pencil-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z">
                                </path></svg></a>   
                            <a href="{{ route('customers-list-handle', ['handle' => 'delete', 'id' =>optional($customer)->id]) }}" style="margin: 0px 10px;">
                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="16"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>
                            </a>
                        </td>  
                        @endif
                        </tr>
                        @endforeach          
                    </tbody>
                    </table>
                    <br/><br/>
                    @if(!isset($check))
                    <div class="text-center">
                        {{$model->render()}}
                    </div>
                    @endif