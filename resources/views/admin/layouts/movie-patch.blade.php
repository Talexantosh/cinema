@php
  use \App\Models\Admin\SoonMovie;

  $enable = SoonMovie::where('movie_id', $id)->get();
@endphp

@if(count($enable))
<br/>
<div class="d-flex justify-content-center align-items-center">

    <button class="btn  btn-outline-secondary" type="submit" form="movie-in-release-form" style="width: 40%;">В прокат
    </button>

    <form id="movie-in-release-form" action="{{ route('soon-movie-in-release', ['id' => $id]) }}" method="get">
          @csrf
    </form>
</div>
@endif