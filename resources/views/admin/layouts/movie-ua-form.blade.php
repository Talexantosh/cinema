<!-- [ 
       'movie' => Model::find(id),
      ] -->
      <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                <span class="fs-6 mr-5">Назва фільму:</span>
                <input type="text" id="nameUa" name="nameUa" value="{{ $movie->text->string_ua  }}" class="form-control" />
                @if($errors->has('nameUa'))
                    <div class="error text-danger small">{{ $errors->first('nameUa') }}</div>
                @endif
            </div>
            <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                <span class="fs-6 mr-5" >Опис:</span>
                <textarea class="form-control" rows="5" cols="90" id="descriptionUa" name="descriptionUa">{{ $movie->multitext->text_ua }}</textarea>
                @if($errors->has('descriptionUa'))
                    <div class="error text-danger small">{{ $errors->first('descriptionUa') }}</div>
                @endif
                <br>
            </div>