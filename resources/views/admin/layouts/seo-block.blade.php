<div class="row mb-3">
        <div class="col-sm-2">
            <div class="col-form-label">SEO блок</div>
        </div>
        <div class="col-sm-10">
                <div class="row mb-3">
                    <label for="inputSeoUrl" class="col-sm-2 form-check-label">URL</label>
                     <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputSeoUrl" name="seoUrl"
                        value="{{ $seoBlock->url}}">
                        @if($errors->has('seoUrl'))
                            <div class="error text-danger small">{{ $errors->first('seoUrl') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputTitle" class="col-sm-2 form-check-label">Title</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputTitle"  name="seoTitle"
                        value="{{ $seoBlock->title}}">
                        @if($errors->has('seoTitle'))
                            <div class="error text-danger small">{{ $errors->first('seoTitle') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputKeywords" class="col-sm-2 form-check-label">Keywords</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="inputKeywords"  name="seoKeywords"
                        value="{{ $seoBlock->keywords }}">
                        @if($errors->has('seoKeywords'))
                            <div class="error text-danger small">{{ $errors->first('seoKeywords') }}</div>
                        @endif
                    </div>
                </div>
                <div class="row mb-3">
                    <label for="inputDescription" class="col-sm-2 form-check-label">Description</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" aria-label="inputDescription"  name="seoDescription">{{ $seoBlock->description}}</textarea>
                        @if($errors->has('seoDescription'))
                            <div class="error text-danger small">{{ $errors->first('seoDescription') }}</div>
                        @endif
                    </div>
                </div>
        </div>
    </div>