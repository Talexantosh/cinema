
@extends('admin.layouts.main-layout')

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<style>
  /* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection

@section('Container')
@php $movie->parseDescriptionRus(); @endphp
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        <form action="{{ route('movie-page-save', ['id' => $movie->id]) }}" method="post" id="movie-page-form">
        @csrf
          <input name="id" id="id" value="{{$movie->id}}" type="hidden" />

          <div class="tabs">
            @include('admin.layouts.lang-tabs')

            <div class="d-flex justify-content-end">
                <span>ВКЛ: </span>
                <label class="switch">
                    <input type="checkbox"  id="slader-status" name="isDisable" >
                    <span class="slider round"></span>
                </label>
            </div>

            <div id="content-1">
              <!-- Русский вариант -->
              @include('admin.layouts.movie-rus-form', 
              [ 
              'movie' => $movie,
              ])
            </div>
            <div id="content-2">
              <!-- Украинский вариант -->
              @include('admin.layouts.movie-ua-form', 
              [ 
              'movie' => $movie,
              ])
            </div>
          </div>
          <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                      <!-- Main image -->
                      <div class="col-2">
                        <p>Главная картинка</p>
                        <p>128X190</p>
                      </div>
                    <div class="col-3">
                      <div class="card shadow-sm">
                            <div class="d-flex justify-content-end">
                              <!-- form --> 
                              <button class="border-0" type="submit" form="main-image-delete" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                              </button>
                              <!--</form>-->
                            </div>
                            <img src="{{ asset(($movie->main_image_url != '')? $movie->main_image_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                        <div class="card-body">   
                          <div class="d-flex justify-content-center align-items-center">
                              <!-- form -->
                            <button type="submit" form="load-main-image" value="" class="btn btn-sm btn-outline-secondary" @if($movie->main_image != "") disabled @endif > Загрузить </button>
                              <!--</form>-->
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end main image -->
                  <!-- image gallery -->
                  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                    <div class="col">
                        <p>Галерея картинок</p>
                        <p>Размер: 840х560</p>    
                    </div>
                    @for ($i = 1, $j = 0; $i < 6; $i++, $j++)
                    <div class="col">
                      <div class="card shadow-sm">
                            <div class="d-flex justify-content-end">  
                                    <button class="border-0" type="submit" form="deleteGalleryItem_{{$i}}" value="{{$i}}">
                                            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                                    </button>
                            </div>
                            <img src="{{ asset(($movie->gallery[$j]->url != '')? $movie->gallery[$j]->url: 'dist/img/logo.jpg')  }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" /> 
                        <div class="card-body">  
                          <div class="d-flex justify-content-center align-items-center">                            
                                  @if($movie->gallery[$j]->image_location == "") 
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" > Добавить </button>
                                  @else
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" disabled > Добавить </button>
                                  @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    @endfor
                  </div>
                  <!-- end image gallery -->
                  <!-- Url трейлер -->
                  <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                      <span class="fs-6 mr-5">Ссылка на трейлер:</span>
                      <input type="text" id="trailer_url" name="trailer_url" value="{{ $movie->trailer_url }}" class="form-control" />
                      @if($errors->has('trailer_url'))
                        <div class="error text-danger small">{{ $errors->first('trailer_url') }}</div>
                      @endif
                  </div>
                  <!-- end Url трейлер -->
                  <!-- checkboxes -->
                  <br/><br/>
                    <div class="form-check form-check-inline">
                      <label class="form-check-label mr-3">Тип кино </label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" id="type_3d" name="type_3d"
                      @if($movie->type_3d) checked @endif >
                      <label class="form-check-label mr-3" for="type_3d">3D</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" id="type_2d" name="type_2d"
                        @if($movie->type_2d) checked @endif >
                      <label class="form-check-label mr-3" for="type_2d">2D</label>
                    </div>
                    <div class="form-check form-check-inline">
                      <input class="form-check-input" type="checkbox" id="type_imax" name="type_imax" 
                      @if($movie->type_imax) checked @endif >
                      <label class="form-check-label mr-3" for="type_imax">IMAX</label>
                    </div>
                  <!-- end checkboxes -->
                  <!-- seo block -->
                  <br/><br/>
                    @include('admin.layouts.seo-block', ['seoBlock' => $movie->seoblock])
                  <br/><br/>
                  <!-- end seo block -->
                  <div class="d-flex justify-content-center align-items-center">     
                      <span>
                          <button class="btn  btn-outline-secondary" type="submit" form="movie-page-form">Сохранить
                          </button>
                      </span>
                      <span class="pl-5">
                          <button class="btn  btn-outline-secondary" type="submit" form="movie-page-reset-form">Вернуть базовую версию
                          </button>
                      </span>
                      <span class="pl-5">
                          <button class="btn  btn-outline-secondary" type="submit" form="movie-page-delete">Удалить страницу
                          </button>
                      </span>
                  </div>       
            </form>
            <!-- Patch for run into release -->
            @include('admin.layouts.movie-patch', ['id' => $movie->id])
          </div>
  </div>
        <form id="main-image-delete" action="{{ route('movie-main-image-delete', ['id' => $movie->id]) }}" method="get">
          @csrf
        </form>
        <form id="load-main-image" action="{{ route('movie-main-image-index', ['id' => $movie->id]) }}" method="get">
            @csrf
        </form>
        <form id="movie-page-reset-form" action="{{ route('movie-page-reset', ['id' => $movie->id]) }}" method="get">
          @csrf
        </form>
        <form id="movie-page-delete" action="{{ route('movie-page-delete', ['id' => $movie->id]) }}" method="get">
          @csrf
        </form>
        @for ($i = 1, $j = 0; $i < 6; $i++, $j++)
          <form id="deleteGalleryItem_{{$i}}" action="{{ route('movie-item-gallery-delete', ['id' => $movie->id, 'item' => $i]) }}" method="get">
            @csrf   
          </form>
          <form id="loadGalleryItem_{{$i}}" action="{{ route('movie-item-gallery-index',  ['id' => $movie->id, 'item' => $i]) }}" method="get">
            @csrf
          </form>
        @endfor

<script type="text/javascript">       
    if (<?php echo $movie->disabled; ?> == 1)
        document.getElementById("slader-status").checked = false;
    else 
        document.getElementById("slader-status").checked = true;
</script>
@endsection