
@extends('admin.layouts.main-layout')

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/slider-switch.css') }}" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('Container')
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
    @include('admin.layouts.error-message')
        <form action="{{ route('admin-ad-page-save', ['id' => $model->id, 'type' => 'main']) }}" method="post" id="admin-ad-page-form">
        @csrf
          <div class="tabs">
            @include('admin.layouts.lang-tabs')

            <div class="d-flex justify-content-end">
                <span>ВКЛ: </span>
                <label class="switch">
                    <input type="checkbox"  id="slader-status" name="isEnable" >
                    <span class="slider round"></span>
                </label>
            </div>
            <div class="d-flex justify-content-start align-items-start input-group mb-3">
                <span class="fs-6 mr-5">Телефон:</span>
                <div>
                  <div>
                    <input type="text" id="phoneOne" name="phoneOne" value="{{ $model->main_image_location  }}" class="form-control" />
                  </div>
                  @if($errors->has('phoneOne'))
                      <div class="error text-danger small">{{ $errors->first('phoneOne') }}</div>
                  @endif
                  <br/>
                  <div>
                    <input type="text" id="phoneTwo" name="phoneTwo" value="{{ $model->main_image_url  }}" class="form-control" />
                  </div>
                  @if($errors->has('phoneTwo'))
                      <div class="error text-danger small">{{ $errors->first('phoneTwo') }}</div>
                  @endif
                </div>
            </div> <br/>
            <div class="content-rus-1">
              <!-- Русский вариант -->
                    <span class="fs-6 mr-5" >SEO Текст:</span>
                <div class="d-flex justify-content-end align-items-center input-group mb-3">
                    <textarea class="form-control" rows="5" id="seoRus" name="seoRus">{{ $model->_description->content_rus }}</textarea>
                    @if($errors->has('seoRus'))
                        <div class="error text-danger small">{{ $errors->first('seoRus') }}</div>
                    @endif
                    <br>
                </div>
                <br/>
            </div>
            <div class="content-ua-1">
              <!-- Украинский вариант -->
                    <span class="fs-6 mr-5" >SEO Текст:</span>
                  <div class="d-flex justify-content-end align-items-center input-group input-group-sm mb-3">
                    <textarea class="form-control" rows="5" id="seoUa" name="seoUa">{{ $model->_description->content_ua }}</textarea>
                    @if($errors->has('seoUa'))
                        <div class="error text-danger small">{{ $errors->first('seoUa') }}</div>
                    @endif
                    <br>
                </div>
                <br/>
            </div>
          </div>
            
                  <!-- seo block -->
                  <br/><br/>
                    @include('admin.layouts.seo-block', ['seoBlock' => $model->seoblock])
                  <br/><br/>
                  <!-- end seo block -->
                  <div class="d-flex justify-content-center align-items-center">     
                      <span>
                          <button class="btn  btn-outline-secondary" type="submit" form="admin-ad-page-form">Сохранить
                          </button>
                      </span>
                  </div>       
            </form>
          </div>
  </div>

<script type="text/javascript"> 

    if (<?php echo $model->enabled; ?> == 0)
        document.getElementById("slader-status").checked = false;
    else 
        document.getElementById("slader-status").checked = true;

</script>
@endsection