@extends('admin.layouts.main-layout')

@section('custom_styles')
@endsection

@section('Container')
<h4 class="text-center">Список фильмов текущих</h4>
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
            @foreach($currentMovies as $j => $movie)
            @php 
              $imgCurrentMovies = ($movie->_movie && $movie->_movie->main_image_url != '')? $movie->_movie->main_image_url: 'dist/img/logo.jpg';
              $nameCurrentMovies = ($movie->_movie)? $movie->_movie->text->string_rus: 'Фильм не определен';
            @endphp
            
            <div class="col-3">
                <a href="{{ route('movie-page-index', ['id' => $movie->movie_id]) }}">
                <div class="card shadow-sm">
                    <img src="{{ asset($imgCurrentMovies)  }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" /> 
                    <div class="card-body">  
                        <div class="d-flex justify-content-center align-items-center">                            
                            <p class="text-secondary">{{ $nameCurrentMovies }}</p>    
                        </div>
                    </div>
                </div>
                </a>
            </div>
            @endforeach
            <div class="col">
                <div class="card shadow-sm">
                  <div class="card-body">  
                    <div class="d-flex justify-content-center align-items-center">
                        <form id="add-current-movie" action="{{ route('add-current-movie') }}" method="get">
                          @csrf
                          <button type="submit" form="add-current-movie" class="btn btn-sm btn-outline-secondary">
                            <div><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="57"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path></svg></div>
                            <div>Добавить<br/>фильм</div>
                          </button> 
                        </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
  </div>
  <br/><br/>
  <h4 class="text-center">Список фильмов которые покажут скоро</h4>
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
            @foreach($soonMovies as $j => $movie)
            @php
                $imgSoonMovies = ($movie->_movie && $movie->_movie->main_image_url != '')? $movie->_movie->main_image_url: 'dist/img/logo.jpg';
                $nameSoonMovies = ($movie->_movie)? $movie->_movie->text->string_rus: 'Фильм не определен';
            @endphp
            
            <div class="col-3">
                <a href=" {{ route('movie-page-index', ['id' => $movie->movie_id]) }}">
                <div class="card shadow-sm">
                    <img src="{{ asset($imgSoonMovies)  }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" /> 
                    <div class="card-body">  
                        <div class="d-flex justify-content-center align-items-center">                            
                            <p class="text-secondary">{{ $nameSoonMovies }}</p>    
                        </div>
                    </div>
                </div>
                </a>
            </div>
            @endforeach
            <div class="col">
                <div class="card shadow-sm">
                  <div class="card-body">  
                    <div class="d-flex justify-content-center align-items-center">
                        <form id="add-soon-movie" action="{{ route('add-soon-movie') }}" method="get">
                          @csrf
                          <button type="submit" form="add-soon-movie" class="btn btn-sm btn-outline-secondary">
                            <div><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="57"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path></svg></div>
                            <div>Добавить<br/>фильм</div>
                          </button> 
                        </form>
                    </div>
                  </div>
                </div>
            </div>
        </div>
    </div>
  </div> 
  <br/><br/>           
@endsection