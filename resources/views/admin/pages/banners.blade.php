@extends('admin.layouts.main-layout')

@section('custom_styles')
<style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }

      button {
          border: 0px;
      }

      a.btn {
          width: 100%;
      }

      .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
    </style>

    
@endsection

@section('Container')
  <!-- Top Slader asset('dist/img/logo.jpg')-->
  @include('admin.layouts.image-gallery-form', 
      ['title' => 'На главной верх',
      'idSwitch' => 'switch-top-slader',
       'switchState' => $stateTop,
       'actionForm' => action('App\Http\Controllers\Admin\TopSladerGalleryController@save'),
       'sizeImage' => 'Размер: 1000х190',
       'countImages' => $countTopImages,
       'actionDelete' => 'top-gallery-delete',
       'urlImage' => $urlTopImages, 
       'actionUpdate' => 'top-gallery-upload',
       'urlValue' => $urlTopImages,
       'textValue' => $textTopValues,
       'speedSlader' => $speedTopSlader,
       'idSpeedSlader' => 'top-speed-slader',
       'actionAdd' => 'top-gallery-add', 
       'fileLocations' => $fileLocations
      ])
      <br/>
      @include('admin.layouts.image-back-form', 
        [
          'title' => 'Сквозной банер на заднем фоне',
          'actionForm' => route('banner-back-save'),
          'sizeImage' => 'Размер: 2000х3000',
          'actionDelete' => route('banner-back-delete'),
          'actionUpdate' => route('banner-back-upload-index'),
          'urlValue' => ($urlBackground != '')? $urlBackground: 'dist/img/logo.jpg', 
          'fileLocations' => $backgroundLocation,
          'state' => $stateBack
          ])

      <br/>
      @include('admin.layouts.image-gallery-bottom-form', 
      ['title' => 'На главной Новости Акции внизу',
      'idSwitch' => 'switch-bottom-slader',
       'switchBottomState' => $stateBottom,
       'actionForm' => action('App\Http\Controllers\Admin\BottomSladerGalleryController@save'),
       'sizeImage' => 'Размер: 1000х190',
       'countImages' => $countBottomImages,
       'actionDelete' => 'bottom-gallery-delete',
       'urlImage' => $urlBottomImages, 
       'actionUpdate' => 'bottom-gallery-upload',
       'urlValue' => $urlBottomImages,
       'textValue' => $textBottomValues,
       'speedBottomSlader' => $speedBottomSlader,
       'actionAdd' => 'bottom-gallery-add', 
       'idSpeedSlader' => 'bottom-speed-slader',
       'fileLocations' => $filesBottomGallery
      ])
@endsection