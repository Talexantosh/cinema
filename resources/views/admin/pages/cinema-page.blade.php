@extends('admin.layouts.main-layout')

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<!-- jQuery table plugin --> 
<link rel="stylesheet" type="text/css" href="{{ asset('admin/css/datatables.min.css') }}'"/>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css"/>
    <script type="text/javascript" src="{{ asset('admin/js/datatables.min.js') }}"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>
@endsection

@section('Container')
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        <form action="{{ route('cinema-page-save', $id) }}" method="post" id="cinema-page-form">
        @csrf
            <input name="id" id="id" value="{{$id}}" type="hidden" />

            <div class="tabs">
                @include('admin.layouts.lang-tabs')

                <div id="content-1">
                <!-- Русский вариант -->
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5">Название кинотеатра:</span>
                        <input type="text" id="nameRus" name="nameRus" value="{{ $model->_name->string_rus  }}" class="form-control" />
                        @if($errors->has('nameRus'))
                        <div class="error text-danger small">{{ $errors->first('nameRus') }}</div>
                        @endif
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Описание:</span>
                        <textarea class="form-control" rows="5" cols="90" id="descriptionRus" name="descriptionRus">{{ $model->_description->text_rus }}</textarea>
                        @if($errors->has('descriptionRus'))
                        <div class="error text-danger small">{{ $errors->first('descriptionRus') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Условия:</span>
                        <textarea class="form-control" rows="5" cols="90" id="conditionsRus" name="conditionsRus">{{ ($model->_conditions->text_rus != '')? $model->_conditions->text_rus: $model::CONDITION_SAMPLE }}</textarea>
                        @if($errors->has('conditionsRus'))
                        <div class="error text-danger small">{{ $errors->first('conditionsRus') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                </div>
                <div id="content-2">
                <!-- Украинский вариант -->
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5">Название кинотеатра:</span>
                        <input type="text" id="nameUa" name="nameUa" value="{{ $model->_name->string_ua  }}" class="form-control" />
                        @if($errors->has('nameUa'))
                        <div class="error text-danger small">{{ $errors->first('nameUa') }}</div>
                        @endif
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Описание:</span>
                        <textarea class="form-control" rows="5" cols="90" id="descriptionUa" name="descriptionUa">{{ $model->_description->text_ua }}</textarea>
                        @if($errors->has('descriptionUa'))
                        <div class="error text-danger small">{{ $errors->first('descriptionUa') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Условия:</span>
                        <textarea class="form-control" rows="5" cols="90" id="conditionsUa" name="conditionsUa">{{ ($model->_conditions->text_ua != '')? $model->_conditions->text_ua: $model::CONDITION_SAMPLE }}</textarea>
                        @if($errors->has('conditionsUa'))
                        <div class="error text-danger small">{{ $errors->first('conditionsUa') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                </div>
            </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
            <!-- Logo -->
            <div class="col-2">
                <p>Логотип:</p>
            </div>
            <div class="col-3" id="card-cinema-logo">
                <div class="card shadow-sm">
                    <div class="d-flex justify-content-end">
                        <!-- form --> 
                        <button class="border-0" type="submit" form="delete-cinema-logo" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </button>
                        <!--</form>-->
                    </div>
                    <img src="{{ asset(($model->logo_url != '')?$model->logo_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                    <div class="card-body">   
                        <div class="d-flex justify-content-center align-items-center">
                            <button type="submit" form="load-cinema-logo" value="" class="btn btn-sm btn-outline-secondary" @if($model->logo_location != "") disabled @endif > Загрузить </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br/>
            <!-- Top banner -->
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                <div class="col-2">
                    <p>Фото верхнего</p>
                    <p>банера:</p>
                    <p>1000x380</p>
                </div>
                <div class="col-3" id="card-cinema-banner">
                    <div class="card shadow-sm">
                        <div class="d-flex justify-content-end">
                            <button class="border-0" type="submit" form="delete-cinema-top-banner" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                            </button>
                        </div>
                        <img src="{{ asset(($model->img_url != '')?$model->img_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                        <div class="card-body">   
                            <div class="d-flex justify-content-center align-items-center">
                            <!-- form -->
                                <button type="submit" form="load-cinema-top-banner" value="" class="btn btn-sm btn-outline-secondary" @if($model->img_location != "") disabled @endif > Загрузить </button>
                            <!--</form>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <!-- image gallery -->
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                    <div class="col">
                        <p>Галерея картинок</p>
                        <p>Размер: 840х560</p>    
                    </div>
                    @for ($i = 1, $j = 0; $i < 6; $i++, $j++)
                    <div class="col-2" style="display: block;">
                        <div class="card shadow-sm">
                            <div class="d-flex justify-content-end">  
                                <button class="border-0" type="submit" form="deleteGalleryItem_{{$i}}" value="{{$i}}">
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                                </button>
                            </div>
                            <img src="{{ asset(($model->gallery[$j]->url != '')? $model->gallery[$j]->url: 'dist/img/logo.jpg')  }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" /> 
                            <div class="card-body">  
                                <div class="d-flex justify-content-center align-items-center">                            
                                        @if($model->gallery[$j]->image_location == "") 
                                        <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" > Добавить </button>
                                        @else
                                        <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" disabled > Добавить </button>
                                        @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endfor
                </div>
                <br/>
                <!-- table halls -->
                <h4 class="d-flex justify-content-center align-items-center">Список залов</h4>
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                    <div class="col-12">
                        <table id="dataTableRus" class="table table-striped table-bordered table-sm display" cellspacing="0" width="100%">
                        <thead>
                            <tr class="table-secondary">
                            <th class="th-sm" scope="col">Название</th>
                            <th class="th-sm" scope="col">Дата создания</th>
                            <th class="th-sm" scope="col">Действия</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($model->halls as $index => $hall)
                            <tr>
                            <td>{{ $hall->_name->string_rus }}</td>
                            <td>{{ $hall->created_at}}</td>
                            <td>
                                <a href="{{ route('cinema-hall-index', $hall->id) }}" style="margin: 0px 10px;" >
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="pencil-alt" class="svg-inline--fa fa-pencil-alt fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M497.9 142.1l-46.1 46.1c-4.7 4.7-12.3 4.7-17 0l-111-111c-4.7-4.7-4.7-12.3 0-17l46.1-46.1c18.7-18.7 49.1-18.7 67.9 0l60.1 60.1c18.8 18.7 18.8 49.1 0 67.9zM284.2 99.8L21.6 362.4.4 483.9c-2.9 16.4 11.4 30.6 27.8 27.8l121.5-21.3 262.6-262.6c4.7-4.7 4.7-12.3 0-17l-111-111c-4.8-4.7-12.4-4.7-17.1 0zM124.1 339.9c-5.5-5.5-5.5-14.3 0-19.8l154-154c5.5-5.5 14.3-5.5 19.8 0s5.5 14.3 0 19.8l-154 154c-5.5 5.5-14.3 5.5-19.8 0zM88 424h48v36.3l-64.5 11.3-31.1-31.1L51.7 376H88v48z">
                                    </path></svg></a>
                                <a href="{{ route('cinema-hall-delete', $hall->id) }}" style="margin: 0px 10px;">
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="trash-alt" class="svg-inline--fa fa-trash-alt fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="16"><path fill="currentColor" d="M268 416h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12zM432 80h-82.41l-34-56.7A48 48 0 0 0 274.41 0H173.59a48 48 0 0 0-41.16 23.3L98.41 80H16A16 16 0 0 0 0 96v16a16 16 0 0 0 16 16h16v336a48 48 0 0 0 48 48h288a48 48 0 0 0 48-48V128h16a16 16 0 0 0 16-16V96a16 16 0 0 0-16-16zM171.84 50.91A6 6 0 0 1 177 48h94a6 6 0 0 1 5.15 2.91L293.61 80H154.39zM368 464H80V128h288zm-212-48h24a12 12 0 0 0 12-12V188a12 12 0 0 0-12-12h-24a12 12 0 0 0-12 12v216a12 12 0 0 0 12 12z"></path></svg>
                                </a>
                            </td>  
                            </tr>
                            @endforeach          
                        </tbody>
                        </table>
                    </div>
                </div>
                <!-- button create -->
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6 ">
                    <div class="col-12 d-flex justify-content-center align-items-center">
                        <a href="{{ route('cinema-hall-add', ['id' => $model->id]) }}" class="btn btn-sm btn-outline-secondary">Создать зал</a>
                    </div>
                </div>
                <br/><br/>
                <!-- seo block -->
                <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6" id="seo-block-share">
                    <div class="col-12">
                        @include('admin.layouts.seo-block', ['seoBlock' => $model->seoblock])
                        <br/><br/>
                    </div>
                </div>
                <!-- button save -->         
                <div class="d-flex justify-content-center align-items-center">     
                    <span class="pr-5">
                        <button class="btn  btn-outline-secondary" type="submit" form="cinema-page-form">Сохранить
                        </button>
                    </span>
                </div>    
        </form>
    </div>
</div>
<form id="delete-cinema-logo" action="{{ route('delete-cinema-logo', $id) }}" method="get">
    @csrf
</form>
<form id="load-cinema-logo" action="{{ route('cinema-image-index', ['id' => $id, 'type' => 'logo']) }}" method="get">
    @csrf
</form>
<form id="delete-cinema-top-banner" action="{{ route('delete-cinema-top-banner', $id) }}" method="get">
    @csrf
</form>
<form id="load-cinema-top-banner" action="{{ route('cinema-image-index', ['id' => $id, 'type' => 'top']) }}" method="get">
    @csrf
</form>
@for ($i = 1; $i <= 5; $i++)
    <form id="deleteGalleryItem_{{$i}}" action="{{ route('delete-cinema-gallery-item', ['id' => $id, 'item' => $i]) }}" method="get"> 
    @csrf
    </form>
    <form id="loadGalleryItem_{{$i}}" action="{{ route('cinema-image-index-patch', ['id' => $id, 'type' => 'gallery', 'item' => $i]) }}" method="get">
    @csrf
    </form>
@endfor

<script type="text/javascript">
    $(document).ready(function() {
        $('#dataTableRus').DataTable();
        $('#dataTableUa').DataTable();
    } );
</script>
@endsection