
@extends('admin.layouts.main-layout')

@section('custom_styles')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/slider-switch.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/spinner-submit.css') }}" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('plugins/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script src="{{ asset('dist/js/lang.select.tab.js') }}"></script>

<link rel="stylesheet" href="{{ asset('plugins/dropzone/dropzone.css') }}">
<script src="{{ asset('plugins/dropzone/dropzone.js') }}"></script>
@endsection

@section('Container')
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
      @include('admin.layouts.error-message')
        <div class="tabs">@include('admin.layouts.lang-tabs')<br/>
        @foreach($model as $item)
          <form action="{{ route('admin-ad-page-save', ['id' => $item->id, 'type' => 'address']) }}" method="post" id="admin-ad-page-address-form-{{$item->id}}">
            @csrf
          <div class="card shadow-sm">
            <div class="card-body">   
              <div class="justify-content-center align-items-center">
                <!-- todo address card -->
                <div id="address-{{$item->id}}">
                  @include('admin.layouts.cinema-contact-response', ['model' => $item])
                </div>
                <div class="row">
                  <div class="col">
                  <span class="fs-6 mr-5" >
                      <p>Размер: 840х560</p>    
                  </span>
                  </div>
                  <div class="col">
                  <div class="fs-6 mr-5" >
                    <div class="card shadow-sm">
                      <div class="d-flex justify-content-between">  
                      <span>Лого</span>
                        <button class="border-0" type="submit" form="deleteGalleryItem_{{$item->id}}_1" value="1">
                          <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </button>
                      </div>
                      <img src="<?php echo asset(($item->_gallery["item_url_1"] != '')? $item->_gallery["item_url_1"]: 'dist/img/logo.jpg') ?>" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" data-gallery-image-{{$item->id}}/> 
                      <div class="card-body">  
                        <div class="d-flex justify-content-center align-items-center">                            
                          @if($item->_gallery["item_location_1"] == "") 
                            <button type="submit" form="loadGalleryItem_{{$item->id}}_1" value="1" class="btn btn-sm btn-outline-secondary" data-button-add > Загрузить </button>
                          @else
                            <button type="submit" form="loadGalleryItem_{{$item->id}}_1" value="1" class="btn btn-sm btn-outline-secondary" data-button-add disabled > Загрузить </button>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  <div class="col">
                  <div class="fs-6 mr-5" >
                    <div class="card shadow-sm">
                      <div class="d-flex justify-content-between"> 
                      <span class="text-center">  
                        Главная картинка
                      </span> 
                        <button class="border-0" type="submit" form="deleteGalleryItem_{{$item->id}}_2" value="2">
                          <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                        </button>
                      </div>
                      <img src="<?php echo asset(($item->_gallery["item_url_2"] != '')? $item->_gallery["item_url_2"]: 'dist/img/logo.jpg') ?>" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" data-gallery-image-{{$item->id}}/> 
                      <div class="card-body">  
                        <div class="d-flex justify-content-center align-items-center">                            
                          @if($item->_gallery["item_location_2"] == "") 
                            <button type="submit" form="loadGalleryItem_{{$item->id}}_2" value="2" class="btn btn-sm btn-outline-secondary" data-button-add > Загрузить </button>
                          @else
                            <button type="submit" form="loadGalleryItem_{{$item->id}}_2" value="2" class="btn btn-sm btn-outline-secondary" data-button-add disabled > Загрузить </button>
                          @endif
                        </div>
                      </div>
                    </div>
                  </div>
                  </div>
                  </div>
                  <div class="d-flex justify-content-between align-items-center">
                        <div class="fs-6 mr-5" ><p>Drop зона:</p></div>
                        <div class="fs-6 mr-5" >
                          <div class="card shadow-sm" style="min-width: 480px;"> 
                            <div class="card-body">  
                              <div id="dropzone-{{$item->id}}" class="dropzone"></div>
                            </div>
                          </div>
                        </div>
                  </div>
              </div>
              <div class="d-flex justify-content-center align-items-center">     
                <span class="fs-6 mr-5" >
                    <button class="btn  btn-outline-secondary" type="submit">
                      <span class="submit-spinner submit-spinner_hide"></span>Сохранить</button>
                </span>
                <span class="fs-6 mr-5" >
                    <a class="btn  btn-outline-secondary" href="{{ route('admin-ad-page-handle', ['id' => $item->id, 'type' => 'address', 'operation' => 'delete']) }}">Удалить</a>
                </span>
            </div> 
            </div>
          </div>
          <br/>
          </form>
          <br/>
        @endforeach
        </div>
          <!-- create new card with address the cinema -->
          <div class="d-flex justify-content-center align-items-center">
            <a href="{{ route('admin-ad-page-handle', ['id' => 0, 'type' => 'address', 'operation' => 'create']) }}" class="btn btn-sm btn-outline-secondary">
              <span>
                  <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="16"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path>
                  </svg>
              </span>
              <span>Добавить еще кинотеатр
              </span>
            </a>
          </div>
          <!-- seo block -->
          <br/><br/>
          <form action="{{ route('admin-ad-page-save', ['id' => $list->id, 'type' => 'normal']) }}" method="post" id="admin-ad-page-form">
            @include('admin.layouts.seo-block', ['seoBlock' => $list->seoblock])
          <br/><br/>
          <!-- end seo block -->
          <div class="d-flex justify-content-center align-items-center">     
              <span>
                  <button class="btn  btn-outline-secondary" type="submit" form="admin-ad-page-form">Сохранить</button>
              </span>
          </div>       
        </form>
    </div>
  </div>
  @foreach($model as $item)
    @for ($i = 1, $j = 0; $i <= 2; $i++, $j++)
      <form id="deleteGalleryItem_{{$item->id}}_{{$i}}" action="{{ route('admin-ad-page-image-handle', ['id' => $item->id, 'type' => 'gallery', 'item' => $i, 'operation' => 'delete']) }}" method="get">
        @csrf   
      </form>
      <form id="loadGalleryItem_{{$item->id}}_{{$i}}" action="{{ route('admin-ad-page-image-handle',  ['id' => $item->id, 'type' => 'gallery', 'item' => $i, 'operation' => 'load']) }}" method="get">
        @csrf
      </form>
    @endfor
  @endforeach

<script type="text/javascript"> 

// The dropzone method is added to jQuery elements and can
// be invoked with an (optional) configuration object.
<?php foreach ($model as $item):?>
var myDropzone = new Dropzone("div#dropzone-{{$item->id}}", {
  url: "{{ route('admin-store-image-via-dropzone', [$item->id]) }}",
  maxFilesize: 10,
  paramName: "file",
  autoProcessQueue: true,
  renameFile: function (file) {
    var dt = new Date();
    var time = dt.getTime();
    return time + file.name;
  },
  resizeWidth: 840,
  resizeHeight: 560,
  maxFiles: 5,
  acceptedFiles: ".jpeg,.jpg,.png,.gif",
  addRemoveLinks: true,
  timeout: 60000,
  headers: {
            'x-csrf-token': document.head.querySelector('meta[name="csrf-token"]').content,
          },
  dictDefaultMessage : "Перетащите файлы сюда для загрузки галереи" ,
  dictCancelUpload: "Отмена",
  success: function (file, response) {
    
    if ("error" in response){
      $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response.error);
      return;
    }
   
    myDropzone.removeFile(file);
   
    responseObject = JSON.parse(response.response);
    $("img[data-gallery-image-{{$item->id}}]").eq(responseObject.eq).attr("src",
      responseObject.src);
    $("button[data-button-add]").eq(responseObject.eq).attr("disabled",
      true);
      
  },
  error: function (file, response) {
    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
      return false;
  } 
  });

  $("#admin-ad-page-address-form-{{$item->id}}").on("submit", function(){
    event.preventDefault();
    $("#admin-ad-page-address-form-{{$item->id}} [type='submit']").attr('disabled', true);
    $("#admin-ad-page-address-form-{{$item->id}} [type='submit'] .submit-spinner").removeClass('submit-spinner_hide');
    $.ajax({
      url: "{{ route('admin-ad-page-save', ['id' => $item->id, 'type' => 'address']) }}",
      method: 'post',
     /*  headers: {
        'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
      }, */
      data: $(this).serialize(),
      success: function(data){
        $("#admin-ad-page-address-form-{{$item->id}} [type='submit']").attr('disabled', false);
        $("#admin-ad-page-address-form-{{$item->id}} [type='submit'] .submit-spinner").addClass('submit-spinner_hide');
        $("#address-{{$item->id}}").html(data);
      },
      error: function (data){
        $("#admin-ad-page-address-form-{{$item->id}} [type='submit']").attr('disabled', false);
        $("#admin-ad-page-address-form-{{$item->id}} [type='submit'] .submit-spinner").addClass('submit-spinner_hide');
        $("#address-{{$item->id}}").html(data);
      }
    }).done();
  });
<?php endforeach ?>

</script>
@endsection