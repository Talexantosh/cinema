@extends('admin.layouts.main-layout')

@section('custom_styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/spinner-submit.css') }}" >  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.5/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.5/js/jquery.dataTables.js"></script>



@endsection

@section('Container')

    <div class="container">
        <div class="row">
            <div class="col-3">
                <button type="submit" form="customers-select" class="btn btn-outline-secondary">Отправить выбранные</button>
            </div>
            <div class="col-5">
                <h4 class="text-center">Пользователи</h4>
            </div>
            <div class="col-4 text-right">
                <!-- todo search -->
                <form class="row g-3" id="search-form">
                @csrf
                <div class="col-auto">
                    <label for="search" class="visually-hidden">Поиск</label>
                    <input type="text" class="form-control" name="search" id="search" placeholder="Поиск">
                </div>
                <div class="col-auto">
                    <button id="search-button" type="submit" class="btn btn-second mb-3">
                        <span class="submit-spinner submit-spinner_hide"></span>Поиск</button>
                </div>
                </form>
            </div>
        </div>
            <div class="row">
                <div class="col-12" id="customer-table">
                    <form method="post" action="{{route('admin-customers-mailing-handle', ['handle' => 'select'])}}" id="customers-select">
                        @csrf
                    @include('admin.layouts.customer-table', ['model' => $model])
                    </form>
                </div>
            </div>
            <br/><br/>
        </div>
    </div>  
<script type="text/javascript">
$(function(){
    $('#customer-table').DataTable( {
        stateSave: true,
        pagingType: "numbers",
        searching: false,
        ordering:  true,
        pageLength: 10,
        paging: true
    } );

    $("#search-form").on("submit", function(){
        event.preventDefault();
        function buttonHandle(disabled, removeClass){
            $("#search-button").attr('disabled', disabled);
            if (removeClass)
                $(".submit-spinner").removeClass('submit-spinner_hide');
            else 
                $(".submit-spinner").addClass('submit-spinner_hide');
        };
        buttonHandle(true, true);
        $.ajax({
        url: "{{ route('customer-search') }}",
        method: 'post',
        /*  headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        }, */
        data: $(this).serialize(),
        responseHandle: function(data){
            buttonHandle(false, false);
            $("#customer-table").html(data);
        },        
        success: function(data){
            this.responseHandle(data);
        },
        error: function (data){
           this.responseHandle(data);
        }
        }).done();
    });
});
</script>
@endsection