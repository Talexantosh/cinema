
@extends('admin.layouts.main-layout')

@section('custom_styles')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/spinner-submit.css') }}" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{asset('plugins/jqueryform/jquery.form.js')}}"></script>
@endsection

@section('Container')
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        @include('admin.layouts.error-message')
        
        @if (isset($alert))
        <div class="alert alert-danger"> 
        {{$alert}}
        </div>
        @endif
        
        <div class="d-none alert alert-danger alert-dismissible fade show" role="alert">
            <span></span> 
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        <div class="d-none alert alert-success alert-dismissible fade show" role="alert">
            <span></span> 
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
        
        <div class="card shadow-sm">
            <div class="card-body"> 
        <form id="form-mailing">
            @csrf
                <div class="row">
                    <div class="col-12">
                        <h1 class="text-center">E-mail</h1>
                    </div>
                </div>
                <br/><br/>
                <div class="row">
                    <div class="col-3">
                        <label class="fw-normal"  for="selectively">Выбрать кому слать e-mail</label>
                    </div>
                    <div class="col-6">
                        <div class="form-check form-check-inline">
                            @if (!isset($mailing->selectively) )
                            <input class="form-check-input" type="radio" name="selectively" id="all" value="0" checked>
                            @elseif (!$mailing->selectively)
                            <input class="form-check-input" type="radio" name="selectively" id="all" value="0" checked>
                            @else
                            <input class="form-check-input" type="radio" name="selectively" id="all" value="0">
                            @endif
                            <label class="form-check-label" for="all">Всем пользователям</label>
                        </div>
                        <div class="form-check form-check-inline">
                            @if (!isset($mailing->selectively))
                            <input class="form-check-input" type="radio" name="selectively" id="selectively" value="1">
                            @elseif ($mailing->selectively)
                            <input class="form-check-input" type="radio" name="selectively" id="selectively" value="1" checked>
                            @else
                            <input class="form-check-input" type="radio" name="selectively" id="selectively" value="1">
                            @endif
                            <label class="form-check-label" for="selectively">Выборочно</label>
                        </div>
                    </div>
                    <div class="col-3">
                        @if (isset($mailing->selectively) && $mailing->selectively )
                        <button type="button" id="select-customer" class="btn btn-outline-secondary">Выбрать пользователей</button>
                        @else
                        <button type="button" id="select-customer" class="btn btn-outline-secondary" disabled>Выбрать пользователей</button>
                        @endif
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-7">
                        <div class="row">
                            <div class="col-5">
                                <label class="fw-normal"  for="file">Загрузить HTML-письмо</label>
                            </div>
                            <div class="col-6">
                                <input form="template-select" type="file"  id="file" name="file" accept=".html, .htm">
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-6">
                                <label class="fw-normal"  for="name-template">Шаблон текущей рассылки </label>
                            </div>
                            <div class="col-6">
                                <span class="text-primary" id="name-template">
                                    @if (isset($mailing->_template))
                                    {{optional($mailing->_template)->name}}
                                    @endif
                                </span>
                            </div>
                        </div>
                        <br/>
                        <div class="row">
                            <div class="col-4">
                                <label class="fw-normal"  for="qty">Количество писем </label>
                            </div>
                            <div class="col-2 p-0">
                                <span class="text-primary" id="qty"></span>
                            </div>
                            <div class="col-4 p-0">
                                <label  class="fw-normal"  for="progress">Рассылка выполнена на </label>
                            </div>
                            <div class="col-2 p-0">
                                <span class="text-primary" id="progress"></span>
                                <span class="text-primary"> %</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-5">
                        <div class="border border-dark border-4 rounded" style="height: 12rem;">
                            <div class="row">
                                <div class="col-12 text-center">
                                    Список последних загруженных шаблонов
                                </div>
                            </div>
                            <br/>
                            <!-- todo -->
                            <div id="list-templates">
                            @include('admin.layouts.mailing-list-templates')
                            </div>
                            <!-- -->
                        </div>
                    </div>
                </div>
                <br/><br/><br/>
                <div class="row">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-outline-secondary">
                        <span class="submit-spinner submit-spinner_hide"></span>Начать рассылку</button>
                    </div>
                </div>
        </form>
            </div>
        </div>
    </div>
</div>
<form id="template-select" action="{{route('admin-customers-mailing-handle', ['handle' => 'template'])}}" method="POST">
    @csrf
</form>
        
 

<script type="text/javascript"> 
function setBtnSelectCustomerDisabled(e, val){
    if (e.target.checked)
        $("#select-customer").attr('disabled', val);
    else
        $("#select-customer").attr('disabled', !val);
}

function setBtnSubmitDisable(val){
    $("[type=submit]").attr('disabled', val);
    (val)? $(".submit-spinner").removeClass("submit-spinner_hide")
         : $(".submit-spinner").addClass("submit-spinner_hide");
}

function getMailingProgress(){
    $.ajax({
            url: "{{route('admin-customers-mailing-handle', 'progress')}}",
            type: "post",
            dataType: "json",
            data: null,
            processData: false,
            contentType: false,
            headers: {
                'x-csrf-token': $('meta[name="csrf-token"]').attr('content'),
            },
            success: function (data, status)
            {
                $('#progress').text(data.progress);
                if (data.progress == "100")
                    clearInterval(window.interv);
                $('#qty').text(data.qty);
            },
            error: function (xhr, desc, err)
            {
                $("div.alert-danger.alert-dismissible").removeClass("d-none");
                $("div.alert-danger.alert-dismissible span").text(err);
            }
    });
}

$(function(){
    /** @todo setInterval() */
    window.interv = setInterval(getMailingProgress, 5000);
    $("#selectively").on('input', function(e){
        setBtnSelectCustomerDisabled(e, false);
    });
    $("#all").on('input', function(e){
        setBtnSelectCustomerDisabled(e, true);
    });
    $("#select-customer").on('click', function(e){
        window.location = "<?php echo route('customers-list-handle', 'select')?>"
    });
    $("#file").on('change', function(e){
        $('#template-select').trigger('submit');
    });
    /* $('#template-select').ajaxForm(function(response){
        alert(response); 
    }); */
    $('#template-select').on("submit", function(event){
        event.preventDefault();
        $.ajax({
            url: $(this).attr("action"),
            type: $(this).attr("method"),
            dataType: "html",
            data: new FormData(this),
            processData: false,
            contentType: false,
            success: function (data, status)
            {
                $('#list-templates').html(data);
            },
            error: function (xhr, desc, err)
            {
                $("div.alert-danger.alert-dismissible").removeClass("d-none");
                $("div.alert-danger.alert-dismissible span").text(err);
            }
    })
    });    
    /* $('[data-checkbox]').each(function(index, elem){
        $(elem).on('input', function(e){
            $('#name-template').text($("#name" + e.target.value).text());
        });
    });    */ 
    $("#form-mailing").on("submit", function(){
    event.preventDefault();
    setBtnSubmitDisable(true);
    window.interv = setInterval(getMailingProgress, 5000);
    $.ajax({
      url: "{{ route('admin-customers-mailing-handle', ['handle' => 'go']) }}",
      method: 'post',
     /*  headers: {
        'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
      }, */
      data: $(this).serialize(),
      success: function(data){
        setBtnSubmitDisable(false);
        $('#name-template').text(data)
      },
      error: function (data){
        setBtnSubmitDisable(false);
        $("div.alert-danger.alert-dismissible").removeClass("d-none");
        $("div.alert-danger.alert-dismissible span").text(data);
      }
    }).done();
  });
});
</script>
@endsection