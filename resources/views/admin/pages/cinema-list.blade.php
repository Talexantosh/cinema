@extends('admin.layouts.main-layout')

@section('custom_styles')
   
@endsection

@section('Container')
<!-- [
        $model,
        $actionDeleteItem,
        $refCinemaForm,
        $actionAdd
    ] -->
<h4 class="text-center">Список кинотеатров</h4>
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
            @for ($i = 0; $i < count($model); $i++)
                <div class="col-3">
                    <div class="card shadow-sm">
                        <div class="d-flex justify-content-end">
                            <form id="deleteItem_{{$i}}" action="{{ route('cinema-page-delete', $i + 1) }}" method="get">
                                @csrf   
                                <button type="submit" form="deleteItem_{{$i}}" value="" class="border-0">
                                        <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                                </button>
                            </form>
                        </div>
                        <a href="{{  route('cinema-page-index', $i + 1) }}">
                            <img src="{{ asset(($model[$i]->logo_url == '')? 'dist\img\logo.jpg': $model[$i]->logo_url) }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Изображение не доступно" height="57" width="100%" /> 
                            <div class="card-body">   
                                <div class="d-flex justify-content-center align-items-center">
                                    <p class="text-secondary">{{ ($model[$i]->_name->string_rus == '')? 'Кинотеатр не установлен': $model[$i]->_name->string_rus }}</p>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>       
            @endfor 
            <div class="col-3">
                <div class="card shadow-sm">
                  <div class="card-body">  
                    <div class="d-flex justify-content-center align-items-center">
                        <a href="{{ route('cinema-page-add') }}" class="btn btn-sm btn-outline-secondary">
                            <div><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="plus" class="svg-inline--fa fa-plus" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" height="57"><path fill="currentColor" d="M432 256c0 17.69-14.33 32.01-32 32.01H256v144c0 17.69-14.33 31.99-32 31.99s-32-14.3-32-31.99v-144H48c-17.67 0-32-14.32-32-32.01s14.33-31.99 32-31.99H192v-144c0-17.69 14.33-32.01 32-32.01s32 14.32 32 32.01v144h144C417.7 224 432 238.3 432 256z"></path></svg></div>
                            <div>Добавить</div>
                        </a>
                    </div>
                  </div>
                </div>
            </div>   
        </div>
    </div>    
</div>           
@endsection