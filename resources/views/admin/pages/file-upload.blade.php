@extends('admin.layouts.main-layout')

@section('custom_styles')
@endsection

<!--  [
      'adminName' => Auth::user()->login, 
      'id' => $id,
      'action' => url('top-gallery-store')
      ] -->
@section('Container')
<h4 class="text-center">Загрузить ресурс</h4>
<div class="container mt-4">
    <form method="POST" enctype="multipart/form-data" id="upload-file" action="{{ $action }}" >    
        @csrf
        <input name="id" id="id" value="{{$id}}" type="hidden" />
        <div class="row">
            <div class="d-flex justify-content-center align-items-center ">
                    <div class="card shadow-sm">
                        <div class="card-body">
                            <div class="col">
                                <div class="form-group">
                                    <input class="form-control" type="file" name="file" placeholder="Выберите Файл" id="file" accept=".jpg,.jpeg,.bmp,.png,.gif">
                                        @error('file')
                                        <div class="alert alert-danger mt-1 mb-1">{{ $message }}</div>
                                        @enderror
                                </div>
                                <div  class="col d-flex justify-content-evenly align-items-center">
                                    <button type="submit" class="btn  btn-primary pr-10" id="submit">Сохранить</button>
                                    <a href="{{ $_SERVER['HTTP_REFERER'] }}" class=" btn btn-secondary pl-10">Отменить</a>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>     
    </form>
</div>
@endsection