
@extends('admin.layouts.main-layout')

@section('custom_styles')
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/slider-switch.css') }}" >
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ asset('plugins/tinymce/tinymce.min.js') }}" referrerpolicy="origin"></script>

<link rel="stylesheet" href="{{ asset('plugins/dropzone/dropzone.css') }}">
<script src="{{ asset('plugins/dropzone/dropzone.js') }}"></script>

@endsection

@section('Container')
  <div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
    @include('admin.layouts.error-message')
        <form action="{{ route('admin-ad-page-save', ['id' => $model->id, 'type' => 'normal']) }}" method="post" id="admin-ad-page-form">
        @csrf
          <div class="tabs">
            @include('admin.layouts.lang-tabs')

            <div class="d-flex justify-content-end">
                <span>ВКЛ: </span>
                <label class="switch">
                    <input type="checkbox"  id="slader-status" name="isEnable" >
                    <span class="slider round"></span>
                </label>
            </div>
            <div id="content-1">
              <!-- Русский вариант -->
                <div class="justify-content-center align-items-center input-group mb-3">
                    <span class="fs-6 mr-5">Название:</span>
                    <input type="text" id="nameRus" name="nameRus" value="{{ $model->_name->string_rus  }}" class="form-control" />
                    @if($errors->has('nameRus'))
                        <div class="error text-danger small">{{ $errors->first('nameRus') }}</div>
                    @endif
                </div>
                <br/>
                    <span class="fs-6 mr-5" >Описание:</span>
                <div class="justify-content-end align-items-center input-group mb-3">
                    <textarea class="form-control" rows="0" cols="0" id="descriptionRus" name="descriptionRus">{{ $model->_description->content_rus }}</textarea>
                    @if($errors->has('descriptionRus'))
                        <div class="error text-danger small">{{ $errors->first('descriptionRus') }}</div>
                    @endif
                    <br>
                </div>
                <br/>
            </div>
            <div id="content-2">
              <!-- Украинский вариант -->
                <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                    <span class="fs-6 mr-5">Название:</span>
                    <input type="text" id="nameUa" name="nameUa" value="{{ $model->_name->string_ua  }}" class="form-control"/>
                    @if($errors->has('nameUa'))
                        <div class="error text-danger small">{{ $errors->first('nameUa') }}</div>
                    @endif
                </div>
                <br/>
                    <span class="fs-6 mr-5" >Описание:</span>
                  <div class="justify-content-end align-items-center input-group input-group-sm mb-3">
                    <textarea class="form-control" rows="0" cols="0" id="descriptionUa" name="descriptionUa">{{ $model->_description->content_ua }}</textarea>
                    @if($errors->has('descriptionUa'))
                        <div class="error text-danger small">{{ $errors->first('descriptionUa') }}</div>
                    @endif
                    <br>
                </div>
                <br/>
            </div>
          </div>
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                      <!-- Main image -->
                      <div class="col-2">
                        <p>Главная картинка</p>
                        <p>1000X250</p>
                      </div>
                    <div class="col-3">
                      <div class="card shadow-sm">
                            <div class="d-flex justify-content-end">
                              <!-- form --> 
                              <button class="border-0" type="submit" form="main-image-delete" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                      <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                              </button>
                              <!--</form>-->
                            </div>
                            <img src="{{ asset(($model->main_image_url != '')? $model->main_image_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                        <div class="card-body">   
                          <div class="d-flex justify-content-center align-items-center">
                              <!-- form -->
                            <button type="submit" form="load-main-image" value="" class="btn btn-sm btn-outline-secondary" @if($model->main_image_location != "") disabled @endif > Загрузить </button>
                              <!--</form>-->
                          </div>
                        </div>
                      </div>
                    </div>
            </div>
                  <!-- end main image -->
                  <!-- image gallery -->
                  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                    <div class="col">
                        <p>Галерея картинок</p>
                        <p>Размер: 840х560</p>    
                    </div>
                    @for ($i = 1; $i < 6; $i++)
                    <div class="col">
                      <div class="card shadow-sm">
                            <div class="d-flex justify-content-end">  
                                    <button class="border-0" type="submit" form="deleteGalleryItem_{{$i}}" value="{{$i}}">
                                            <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                                    </button>
                            </div>
                            <img src="<?php echo asset(($model->_gallery["item_url_$i"] != '')? $model->_gallery["item_url_$i"]: 'dist/img/logo.jpg') ?>" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" data-gallery-image/> 
                        <div class="card-body">  
                          <div class="d-flex justify-content-center align-items-center">                            
                                  @if($model->_gallery["item_location_$i"] == "") 
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" data-button-add > Добавить </button>
                                  @else
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" data-button-add disabled > Добавить </button>
                                  @endif
                          </div>
                        </div>
                      </div>
                    </div>
                    @endfor
                  </div>
                  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                    <div class="col-2"><p>Drop зона:</p></div>
                    <div class="col-10">
                      <div class="card shadow-sm"> 
                        <div class="card-body">  
                          <div id="dropzone" class="dropzone"></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end image gallery -->
                  <!-- Url трейлер -->
                  <div class="d-none justify-content-center align-items-center input-group input-group-sm mb-3">
                      <span class="fs-6 mr-5">Видео:</span>
                      <input type="text" id="video_url" name="video_url" value="{{ $model->video }}" class="form-control" />
                      @if($errors->has('video'))
                        <div class="error text-danger small">{{ $errors->first('video') }}</div>
                      @endif
                  </div>
                  <!-- end Url трейлер -->
                  <!-- seo block -->
                  <br/><br/>
                    @include('admin.layouts.seo-block', ['seoBlock' => $model->seoblock])
                  <br/><br/>
                  <!-- end seo block -->
                  <div class="d-flex justify-content-center align-items-center">     
                      <span>
                          <button class="btn  btn-outline-secondary" type="submit" form="admin-ad-page-form">Сохранить
                          </button>
                      </span>
                  </div>       
            </form>
          </div>
  </div>
        <form id="main-image-delete" action="{{ route('admin-ad-page-image-handle', ['id' => $model->id, 'type' => 'main', 'item' => 1, 'operation' => 'delete']) }}" method="get">
          @csrf
        </form>
        <form id="load-main-image" action="{{ route('admin-ad-page-image-handle', ['id' => $model->id, 'type' => 'main', 'item' => 1, 'operation' => 'load']) }}" method="get">
            @csrf
        </form>
        @for ($i = 1, $j = 0; $i < 6; $i++, $j++)
          <form id="deleteGalleryItem_{{$i}}" action="{{ route('admin-ad-page-image-handle', ['id' => $model->id, 'type' => 'gallery', 'item' => $i, 'operation' => 'delete']) }}" method="get">
            @csrf   
          </form>
          <form id="loadGalleryItem_{{$i}}" action="{{ route('admin-ad-page-image-handle',  ['id' => $model->id, 'type' => 'gallery', 'item' => $i, 'operation' => 'load']) }}" method="get">
            @csrf
          </form>
        @endfor

<script type="text/javascript"> 

    if (<?php echo $model->enabled; ?> == 0)
        document.getElementById("slader-status").checked = false;
    else 
        document.getElementById("slader-status").checked = true;

  var useDarkMode = window.matchMedia('(prefers-color-scheme: dark)').matches;
  tinymce.init({
  selector: 'textarea#descriptionRus',
  plugins: 'preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
  editimage_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: false,
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  automatic_uploads: true,
  file_picker_callback: function (callback, value, meta) {
    /* Provide file and text for the link dialog */
    if (meta.filetype === 'file') {
      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    }

    /* Provide image and alt text for the image dialog */
    if (meta.filetype === 'image') {
      //callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
      
      const input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');

      input.addEventListener('change', (e) => {
        const file = e.target.files[0];

        const reader = new FileReader();
        reader.addEventListener('load', () => {
        /*
          Note: Now we need to register the blob in TinyMCEs image blob
          registry. In the next release this part hopefully won't be
          necessary, as we are looking to handle it internally.
        */
        const id = 'blobid' + (new Date()).getTime();
        const blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        const base64 = reader.result.split(',')[1];
        const blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        /* call the callback and populate the Title field with the file name */
        callback(blobInfo.blobUri(), { title: file.name, 
          /*alt: reader.result*/ });
      });
      reader.readAsDataURL(file);
    });

      input.click();
    }

    /* Provide alternative source and posted for the media dialog */
    if (meta.filetype === 'media') {
      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    }
  },
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  contextmenu: 'link image table',
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: useDarkMode ? 'dark' : 'default',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
});

tinymce.init({
  selector: 'textarea#descriptionUa',
  plugins: 'preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap pagebreak nonbreaking anchor insertdatetime advlist lists wordcount help charmap quickbars emoticons',
  editimage_cors_hosts: ['picsum.photos'],
  menubar: 'file edit view insert format tools table help',
  toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
  toolbar_sticky: false,
  autosave_ask_before_unload: true,
  autosave_interval: '30s',
  autosave_prefix: '{path}{query}-{id}-',
  autosave_restore_when_empty: false,
  autosave_retention: '2m',
  image_advtab: true,
  link_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_list: [
    { title: 'My page 1', value: 'https://www.tiny.cloud' },
    { title: 'My page 2', value: 'http://www.moxiecode.com' }
  ],
  image_class_list: [
    { title: 'None', value: '' },
    { title: 'Some class', value: 'class-name' }
  ],
  importcss_append: true,
  automatic_uploads: true,
  file_picker_callback: function (callback, value, meta) {
    /* Provide file and text for the link dialog */
    if (meta.filetype === 'file') {
      callback('https://www.google.com/logos/google.jpg', { text: 'My text' });
    }

    /* Provide image and alt text for the image dialog */
    if (meta.filetype === 'image') {
      //callback('https://www.google.com/logos/google.jpg', { alt: 'My alt text' });
 
      const input = document.createElement('input');
      input.setAttribute('type', 'file');
      input.setAttribute('accept', 'image/*');

      input.addEventListener('change', (e) => {
        const file = e.target.files[0];

        const reader = new FileReader();
        reader.addEventListener('load', () => {
        /*
          Note: Now we need to register the blob in TinyMCEs image blob
          registry. In the next release this part hopefully won't be
          necessary, as we are looking to handle it internally.
        */
        const id = 'blobid' + (new Date()).getTime();
        const blobCache =  tinymce.activeEditor.editorUpload.blobCache;
        const base64 = reader.result.split(',')[1];
        const blobInfo = blobCache.create(id, file, base64);
        blobCache.add(blobInfo);

        /* call the callback and populate the Title field with the file name */
        // callback(blobInfo.blobUri(), { title: file.name });
        callback(blobInfo.blobUri(), { title: file.name,
          /*alt: reader.result */});
      });
      reader.readAsDataURL(file);
    });

      input.click();
    }

    /* Provide alternative source and posted for the media dialog */
    if (meta.filetype === 'media') {
      callback('movie.mp4', { source2: 'alt.ogg', poster: 'https://www.google.com/logos/google.jpg' });
    }
  },
  templates: [
        { title: 'New Table', description: 'creates a new table', content: '<div class="mceTmpl"><table width="98%%"  border="0" cellspacing="0" cellpadding="0"><tr><th scope="col"> </th><th scope="col"> </th></tr><tr><td> </td><td> </td></tr></table></div>' },
    { title: 'Starting my story', description: 'A cure for writers block', content: 'Once upon a time...' },
    { title: 'New list with dates', description: 'New List with dates', content: '<div class="mceTmpl"><span class="cdate">cdate</span><br /><span class="mdate">mdate</span><h2>My List</h2><ul><li></li><li></li></ul></div>' }
  ],
  template_cdate_format: '[Date Created (CDATE): %m/%d/%Y : %H:%M:%S]',
  template_mdate_format: '[Date Modified (MDATE): %m/%d/%Y : %H:%M:%S]',
  height: 600,
  image_caption: true,
  quickbars_selection_toolbar: 'bold italic | quicklink h2 h3 blockquote quickimage quicktable',
  noneditable_class: 'mceNonEditable',
  toolbar_mode: 'sliding',
  contextmenu: 'link image table',
  skin: useDarkMode ? 'oxide-dark' : 'oxide',
  content_css: useDarkMode ? 'dark' : 'default',
  content_style: 'body { font-family:Helvetica,Arial,sans-serif; font-size:16px }'
});

// The dropzone method is added to jQuery elements and can
// be invoked with an (optional) configuration object.
var myDropzone = new Dropzone("div#dropzone", {
  url: "{{ route('admin-store-image-via-dropzone', [$model->id]) }}",
  maxFilesize: 10,
  paramName: "file",
  autoProcessQueue: true,
  renameFile: function (file) {
    var dt = new Date();
    var time = dt.getTime();
    return time + file.name;
  },
  resizeWidth: 840,
  resizeHeight: 560,
  maxFiles: 5,
  acceptedFiles: ".jpeg,.jpg,.png,.gif",
  addRemoveLinks: true,
  timeout: 60000,
  headers: {
            'x-csrf-token': document.head.querySelector('meta[name="csrf-token"]').content,
          },
  dictDefaultMessage : "Перетащите файлы сюда для загрузки галереи" ,
  dictCancelUpload: "Отмена",
  success: function (file, response) {
    
    if ("error" in response){
      $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response.error);
      return;
    }
   
    myDropzone.removeFile(file);
   
    responseObject = JSON.parse(response.response);
    $("img[data-gallery-image]").eq(responseObject.eq).attr("src",
      responseObject.src);
    $("button[data-button-add]").eq(responseObject.eq).attr("disabled",
      true);
      
  },
  error: function (file, response) {
    $(file.previewElement).addClass("dz-error").find('.dz-error-message').text(response);
      return false;
  } 
  });
</script>
@endsection