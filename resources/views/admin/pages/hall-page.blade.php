@extends('admin.layouts.main-layout')

@section('custom_styles')
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/modal-window.css') }}" >
@endsection

@section('Container')
<div class="album py-1 bg-light border border-1 rounded">
    <div class="container">
            @if ($errors->any())
              <div class="alert alert-danger">
                  <ul>
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              </div>
            @endif
        <form action="{{ route('cinema-hall-save', $id) }}" method="post" id="hall-page-form">
        @csrf
            <input name="id" id="id" value="{{$id}}" type="hidden" />

            <div class="tabs">
                @include('admin.layouts.lang-tabs')
                <div id="content-1">
                <!-- Русский вариант -->
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5">Номер зала:</span>
                        <input type="text" id="nameRus" name="nameRus" value="{{ $model->_name->string_rus  }}" class="form-control" />
                        @if($errors->has('nameRus'))
                            <div class="error text-danger small">{{ $errors->first('nameRus') }}</div>
                        @endif
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Описание зала:</span>
                        <textarea class="form-control" rows="5" cols="90" id="descriptionRus" name="descriptionRus">{{ $model->_description->text_rus }}</textarea>
                        @if($errors->has('descriptionRus'))
                            <div class="error text-danger small">{{ $errors->first('descriptionRus') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                </div>
                <div id="content-2">
                <!-- Украинский вариант -->
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5">Номер зала:</span>
                        <input type="text" id="nameUa" name="nameUa" value="{{ $model->_name->string_ua  }}" class="form-control" />
                        @if($errors->has('nameUa'))
                        <div class="error text-danger small">{{ $errors->first('nameUa') }}</div>
                        @endif
                    </div>
                    <br/>
                    <div class="justify-content-center align-items-center input-group input-group-sm mb-3">
                        <span class="fs-6 mr-5" >Описание зала:</span>
                        <textarea class="form-control" rows="5" cols="90" id="descriptionUa" name="descriptionUa">{{ $model->_description->text_ua }}</textarea>
                        @if($errors->has('descriptionUa'))
                        <div class="error text-danger small">{{ $errors->first('descriptionUa') }}</div>
                        @endif
                        <br>
                    </div>
                    <br/>
                </div>
            </div>

            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                <!-- schema -->
                <div class="col-2">
                    <p>Схема зала:</p>
                    <p>840x560</p>
                </div>
                <div class="col-3" id="card-hall-schema">
                    <div class="card shadow-sm">
                        <div class="d-flex justify-content-end">
                            <!-- form --> 
                            <button class="border-0" type="submit" form="delete-hall-schema" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                            </button>
                            <!--</form>-->
                        </div>
                        <img src="{{ asset(($model->schema_url != '')?$model->schema_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                        <div class="card-body">   
                            <div class="d-flex justify-content-center align-items-center">
                                <!-- form -->
                                <button type="submit" form="load-hall-schema" value="" class="btn btn-sm btn-outline-secondary" @if($model->schema_location != "") disabled @endif > Загрузить </button>
                                <!--</form>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <!-- Top banner -->
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                <!-- Logo -->
                <div class="col-2">
                    <p>Верхний</p>
                    <p>банер:</p>
                    <p>1000x380</p>
                </div>
                <div class="col-3" id="card-hall-banner">
                    <div class="card shadow-sm">
                        <div class="d-flex justify-content-end">
                            <!-- form --> 
                            <button class="border-0" type="submit" form="delete-hall-top-banner" value="" > <!--onclick="document.getElementById('item-delete').submit()"-->
                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                            </button>
                            <!--</form>-->
                        </div>
                        <img src="{{ asset(($model->img_url != '')? $model->img_url: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" /> 
                        <div class="card-body">   
                            <div class="d-flex justify-content-center align-items-center">
                                <!-- form -->
                            <button type="submit" form="load-hall-top-banner" value="" class="btn btn-sm btn-outline-secondary" @if($model->img_location != "") disabled @endif > Загрузить </button>
                                <!--</form>-->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <br/>
            <!-- image gallery -->
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6">
                <div class="col">
                    <p>Галерея картинок</p>
                    <p>Размер: 840х560</p>    
                </div>
                @for ($i = 1, $j = 0; $i < 6; $i++, $j++)
                <div class="col-2" style="display: block;">
                    <div class="card shadow-sm">
                        <div class="d-flex justify-content-end">  
                            <button class="border-0" type="submit" form="deleteGalleryItem_{{$i}}" value="{{$i}}">
                                    <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="times-circle" class="svg-inline--fa fa-times-circle fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z"></path></svg>
                            </button>
                        </div>
                        <img src="{{ asset(($model->gallery[$j]->url != '')? $model->gallery[$j]->url: 'dist/img/logo.jpg')  }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="Картинка не доступна" height="57" width="100%" /> 
                        <div class="card-body">  
                            <div class="d-flex justify-content-center align-items-center">                            
                                    @if($model->gallery[$j]->image_location == "") 
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" > Добавить </button>
                                    @else
                                    <button type="submit" form="loadGalleryItem_{{$i}}" value="{{$i}}" class="btn btn-sm btn-outline-secondary" disabled > Добавить </button>
                                    @endif
                            </div>
                        </div>
                    </div>
                </div>
                @endfor
            </div>
            <br/>
            <!-- seo block -->
            <div class="row row-cols-1 row-cols-sm-2 row-cols-md-6 g-6" id="seo-block-share">
                <div class="col-12">
                    @include('admin.layouts.seo-block', ['seoBlock' => $model->seoblock])
                    <br/><br/>
                </div>
            </div>
            <!-- button save -->         
            <div class="d-flex justify-content-center align-items-center">     
                <span class="pr-5">
                    <button class="btn  btn-outline-secondary" type="submit" form="hall-page-form">Сохранить
                    </button>
                </span>
            </div>   
            <!-- seo block -->
     
        </form>
    </div>
</div>
<form id="delete-hall-schema" action="{{ route('delete-hall-schema', $id) }}" method="get">
    @csrf
</form>
<form id="load-hall-schema" action="{{ route('hall-image-index', ['id' => $id, 'type' => 'schema']) }}" method="get">
    @csrf
</form>
<form id="delete-hall-top-banner" action="{{ route('delete-hall-top-banner', $id) }}" method="get">
    @csrf
</form>
<form id="load-hall-top-banner" action="{{ route('hall-image-index', ['id' => $id, 'type' => 'top']) }}" method="get">
    @csrf
</form>
@for ($i = 1; $i <= 5; $i++)
    <form id="deleteGalleryItem_{{$i}}" action="{{ route('delete-hall-gallery-item', ['id' => $id, 'item' => $i]) }}" method="get">
    @csrf   
    </form>
    <form id="loadGalleryItem_{{$i}}" action="{{ route('hall-image-index-gallery', ['id' => $id, 'type' => 'gallery', 'item' => $i]) }}" method="get">
    @csrf
    </form>
@endfor
@endsection