<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @yield('meta')
    
    <title>{{$title}}</title>
    
    <link href="{{ asset('dist/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ asset('dist/css/app.css')}}" rel="stylesheet">

    <style>
    .ua
    {
        display: none;
    }
    
    option.op-rus:checked p.rus
    {
        display: block;
    }

    option.op-ua:checked p.ua
    {
      display: block;
    }
    </style>
    
     @yield('style')

</head>
@if ($banner->back_image_status)
<body style="background: url({{ asset($banner->back_image_url) }});">
@else 
<body style="background-color:  rgb(10, 10, 255);">
@endif
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
@include('layouts.header')

@yield('content')

@include('layouts.footer')
<script src="{{ asset('plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
</body>
</html>

