<!-- slider -->
<div id="myCarousel" class="carousel slide d-flex justify-content-end" data-ride="carousel"  data-interval="false">
    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <?php for($i = 1, $j = 2; $i < 5; $i++, $j++): ?>
            <?php if($model->_gallery["item_url_$j"] == ""):
                continue; ?>
            <?php endif; ?>
            <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
        <?php endfor; ?>
    </ul>
    <!-- The slideshow -->
    <div class="carousel-inner">
        <?php for($i = 0, $j = 1; $i < 5; $i++, $j++): ?>
            <?php if($model->_gallery["item_url_$j"] == ""):
                continue; ?>
            <?php endif; ?>
            <?php if($i == 0): ?>
            <div class="carousel-item active">
                <img src="{{ asset($model->_gallery->item_url_1) }}" alt="" width="442" height="280">
            </div>
            <?php  continue; ?>
            <?php endif; ?>
            <div class="carousel-item">
                <img src="<?php echo asset($model->_gallery["item_url_$j"]) ?>" alt="" width="442" height="280">
            </div>
        <?php endfor; ?>
    </div>
    
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#myCarousel" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>