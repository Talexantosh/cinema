
@php $count = 0; @endphp
@foreach($collection as $index => $news)
    @if ($news->enabled)
    @php $count++; @endphp
    <div class="col-6">
        <div class="card">
            <div class="card-body" style="background-color: #161f27;">
                <img src="{{ asset($news->_gallery->item_url_1) }}"  class="img-thumbnail" alt="{{ $news->_name->string_rus }}" height="250" />
                <div>
                    <span class="rus">{{ $news->_name->string_rus }}</span>
                    <span class="ua">{{ $news->_name->string_ua }}</span>
                </div>
                <div>
                    @php $orgs = $news->getOrgs(); @endphp
                    <span class="rounded text-white" style="background-color: rgb(5,5,5);">{{ date("d.m.Y", strtotime($news->date_publish)) }}</span>
                    @foreach($orgs['rus'] as $org)
                    <span class="rus rounded text-white ml-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                    @endforeach
                    @foreach($orgs['ua'] as $org)
                    <span class="ua rounded text-white ml-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                    @endforeach
                </div>
                <div>
                    @php $content = $news->getDescriptionContent(); @endphp
                    @foreach($content['rus'] as $par)
                    <p class="rus text-muted">{!! $par !!}</p>
                    @endforeach
                    @foreach($content['ua'] as $par)
                    <p class="ua text-muted">{!! $par !!}</p>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    @endif
    @if($count == $countPerPage)
        @break
    @endif
    @endforeach