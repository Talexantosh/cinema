<div>
    <div class="container mt-5" style="text-align: center;">
        <div class="row">
            <div class="col-4 d-flex justify-content-center ">
                <div class="rounded-circle" style="width: 100px; background-color: #eee;">
                    <img class="logo" src="{{ asset('storage/files/images/logo.svg')}}" alt="logo">
                </div>
            </div>
            <div class="col-8" style="overflow: hidden;">
                <div class="header-column" style="width: 200px">
                    <input name="stringSearch" type="text" id="search-on-site" placeholder="Поиск" style="border: 2px solid black" />
                    <script>
                        $("body").on("change", "#search-on-site", function () {
                            /** @todo */
                        });
                    </script>
                </div>
                <div class="header-column" style="width: 250px">
                    <div class="social facebook">
                        <a href="#"><i class="fa fa-facebook fa-1x"></i></a>
                    </div>
                    <div class="social twitter">
                        <a href="#"><i class="fa fa-twitter fa-1x"></i></a>
                    </div>
                    <div class="social github">
                        <a href="#"><i class="fa fa-github fa-1x"></i></a>
                    </div>
                    <div class="social google-pluse">
                        <a href="#"><i class="fa fa-google-plus fa-1x"></i></a>
                    </div>
                </div>
                @php 
                use  App\Models\Admin\AdPage;
                $pages = AdPage::all()->where('cinema', '!=', 1);
                $phones = AdPage::find(13);
                @endphp
                <div class="header-column text-white" style="width: 200px">
                    <h5>{{optional($phones)->main_image_location}}</h5>
                    <h5>{{optional($phones)->main_image_url}}</h5>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-2" style="height: 30px"></div>
            <div class="col-9" style="border: 1px solid black; background-color: white; height: 30px">
                <nav>
                    <ul>
                        <li class="menu"><a href="{{route('front-posters-index')}}">{{__('Афиша')}}</a></li>
                        <li class="menu"><a href="{{route('front-schedule-index')}}">{{__('Расписание')}}</a></li>
                        <li class="menu"><a href="{{route('front-soon-index')}}">{{__('Скоро')}}</a></li>
                        <li class="menu"><a href="{{route('front-cinemas-index')}}">{{__('Кинотеатры')}}</a></li>
                        <li class="menu"><a href="{{route('front-promotions-index')}}">{{__('Акции')}}</a></li>
                        <li class="menu">
                            <div class="dropdown show">
                                <a class="dropdown-toggle" href="{{route('front-page-about-index')}}" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                {{__('О кинотеатре')}}</a>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item" href="{{route('front-page-news-index')}}">{{__('Новости')}}</a>
                                    @foreach($pages as $page)
                                    <a class="dropdown-item rus" href="{{route('front-page-advertising-index', ['id' => $page->id, 'name' => optional($page->_name)->string_rus])}}">
                                        <span class="rus">{{optional($page->_name)->string_rus}}</span>
                                        <span class="ua">{{optional($page->_name)->string_ua}}</span>
                                    </a>
                                    @endforeach
                                    <a class="dropdown-item" href="{{route('customer-cabinet-index')}}">{{__('Мой кабинет')}}</a>
                                </div>
                            </div>
                        </li>
                        <li class="menu" style="width: 50px;">
                            <a href="{{route('customer-cabinet-index')}}">
                                <svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="user" class="svg-inline--fa fa-user fa-w-14" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" width="12"><path fill="currentColor" d="M313.6 304c-28.7 0-42.5 16-89.6 16-47.1 0-60.8-16-89.6-16C60.2 304 0 364.2 0 438.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-25.6c0-74.2-60.2-134.4-134.4-134.4zM400 464H48v-25.6c0-47.6 38.8-86.4 86.4-86.4 14.6 0 38.3 16 89.6 16 51.7 0 74.9-16 89.6-16 47.6 0 86.4 38.8 86.4 86.4V464zM224 288c79.5 0 144-64.5 144-144S303.5 0 224 0 80 64.5 80 144s64.5 144 144 144zm0-240c52.9 0 96 43.1 96 96s-43.1 96-96 96-96-43.1-96-96 43.1-96 96-96z"></path></svg>
                            </a>
                        </li>
                        <li class="menu" style="width: 50px;">
                            <a href="{{route('customer-logout')}}">
                            <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="power-off" class="svg-inline--fa fa-power-off fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"  width="12"><path fill="currentColor" d="M400 54.1c63 45 104 118.6 104 201.9 0 136.8-110.8 247.7-247.5 248C120 504.3 8.2 393 8 256.4 7.9 173.1 48.9 99.3 111.8 54.2c11.7-8.3 28-4.8 35 7.7L162.6 90c5.9 10.5 3.1 23.8-6.6 31-41.5 30.8-68 79.6-68 134.9-.1 92.3 74.5 168.1 168 168.1 91.6 0 168.6-74.2 168-169.1-.3-51.8-24.7-101.8-68.1-134-9.7-7.2-12.4-20.5-6.5-30.9l15.8-28.1c7-12.4 23.2-16.1 34.8-7.8zM296 264V24c0-13.3-10.7-24-24-24h-32c-13.3 0-24 10.7-24 24v240c0 13.3 10.7 24 24 24h32c13.3 0 24-10.7 24-24z"></path></svg>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>

            <div class="col-1" style="height: 30px">
                <select id="select-lang" style="height: 30px; width: 60px">
                    <option class="op-rus">Рус</option>
                    <option class="op-ua">Укр</option>
                </select>
                <script>
                    var option = document.querySelector('#select-lang');
                    option.onchange=function(){
                        switch (this.value)
                        {
                            case "Рус":
                                {
                                    $(".ua").hide();
                                    $(".rus").show();
                                    break;
                                }
                            case "Укр":
                                {
                                    $(".rus").hide();
                                    $(".ua").show();
                                    break;
                                }
                        }
                    };
                </script>
            </div>
        </div>
    </div>
</div>
</div>
