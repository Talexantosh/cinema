<hr style="color: #8edfce; margin-top: 50px; margin-bottom: 0px; width: 70%">
<div class="footer-clean">
    <footer>
        <div class="container">

            <div class="row justify-content-center">
                <div class="col-sm-4 col-md-3 item text-secondary">
                    <h3>{{ __('Мобильные приложения')}}</h3>
                    <img src="{{ asset('storage/files/images/appstore.png') }}" style="width: 200px"/>
                    <p class="copyright">{{ __('Разработка сайтов')}}: For Avada-Media</p>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <a href="{{route('front-posters-index')}}" class="text-secondary"><h3>{{ __('Афиша')}}</h3></a>
                    <ul>
                        <li><a href="{{route('front-schedule-index')}}">{{ __('Расписание')}}</a></li>
                        <li><a href="{{route('front-soon-index')}}">{{ __('Скоро в прокате')}}</a></li>
                        <li><a href="{{route('front-cinemas-index')}}">{{ __('Кинотеатры')}}</a></li>
                        <li><a href="{{route('front-promotions-index')}}">{{__('Акции')}}</a></li>
                    </ul>
                </div>
                <div class="col-sm-4 col-md-3 item">
                    <a class="text-secondary" href="{{route('front-page-about-index')}}"><h3>О кинотеатре</h3></a>
                    <ul>     
                        <li><a href="{{route('front-page-news-index')}}"><span>{{__('Новости')}}</span></a></li>
                        @php 
                        use  App\Models\Admin\AdPage;
                        $pages = AdPage::all()->where('cinema', '!=', 1);
                        @endphp 
                        @foreach($pages as $page)
                        <li><a href="{{route('front-page-advertising-index', ['id' => $page->id, 'name' => optional($page->_name)->string_rus])}}">
                            <span class="rus">{{optional($page->_name)->string_rus}}</span>
                            <span class="ua">{{optional($page->_name)->string_ua}}</span>
                            </a>
                        </li>
                        @endforeach
                        <li><a href="{{route('customer-cabinet-index')}}"><span>{{__('Мой кабинет')}}</span></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 item social">
                    <div class="social facebook">
                        <a href="#" ><i class="fa fa-facebook fa-1x"></i></a>
                    </div>
                    <div class="social twitter">
                        <a href="#" ><i class="fa fa-twitter fa-1x"></i></a>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                </div>
                <p class="copyright d-flex justify-content-center">© 2016, All rights reserved</p>
            </div>
        </div>
    </footer>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
