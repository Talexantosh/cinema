    @php
    use App\Models\Admin\Movie;
    use App\Models\Admin\Cinema;
    use App\Models\Admin\CinemaHall;
    use Illuminate\Support\Facades\Log;

    const SCHEDULE_HEADER_TABLE = "<table class=\"table border border-warning border-3 table-bordered table-sm \">" .
    "<thead><tr class=\"rus\"><th>" . "Кинотеатр" . "</th><th>" . "Зал" . "</th><th>" . "Фильм" . 
    "</th><th>" . "Начало" . "</th><th>" . "Цена" .
    "</th><th>Технология</th><th>Бронировать</th></tr>" . 
    "<tr class=\"ua\"><th>" . "Кинотеатр" . "</th><th>" . "Зал" . "</th><th>" . "Фільм" . 
    "</th><th>" . "Початок" . "</th><th>" . "Ціна" .
    "</th><th>Технологія</th><th>Бронювати</th></tr></thead><tbody>";

    const SCHEDULE_CLOSE_TABLE = "</tbody></table>";

    $offset_from_today = "-1";
    $skip = true;
    $rus = $ua = '';
    
    foreach($collection as $item){
        if ($item->offset_from_today != $offset_from_today)
        {
            if (!$skip)
            {
                echo $rus;
                echo $ua;
                echo SCHEDULE_CLOSE_TABLE;
            }
            else
                $skip = false;

            $d1 = strtotime('+' . $item->offset_from_today . ' days'); // переводит из строки в дату
            setlocale(LC_TIME,"ru_RUS.utf8");
            // strftime("%A, %B %d", time());
            $date2 = date("d-m-Y, l", $d1); // переводит в новый формат
            echo "<br/>";
            echo "<h5 class=\"text-center\">$date2</h5>";
            echo SCHEDULE_HEADER_TABLE;

            $rus = $ua = "";
            $offset_from_today = $item->offset_from_today;
        }
        
        try{
            $cinema = Cinema::find($item->cinema_id);
            $hall = CinemaHall::find($item->hall_id);
            $movie = Movie::find($item->movie_id);
            $cinemaRus = $cinema->_name->string_rus;
            $hallRus = $hall->_name->string_rus;
            $movieRus = $movie->text->string_rus;
            $cinemaUa = $cinema->_name->string_ua;
            $hallUa = $hall->_name->string_ua;
            $movieUa = $movie->text->string_ua;

            $rus .= "<tr class=\"rus\"><td>" . $cinemaRus . "</td><td>" .
            $hallRus . "</td><td>" . $movieRus . "</td><td>" .
            $item->begin_time . "</td><td>"  . $item->price_ticket . "</td>";
            
            $ua .= "<tr class=\"ua\"><td>" . $cinemaUa . "</td><td>" .
            $hallUa . "</td><td>" . $movieUa . "</td><td>" .
            $item->begin_time . "</td><td>"  . $item->price_ticket . "</td>";

            $rus .= "<td>$item->technology_movie</td>";
            $ua .= "<td>$item->technology_movie</td>";
            
            if(!$item->offset_from_today && strtotime($item->begin_time) >= time()+3600):
            $rus .= '<td><a href=' . route('booking', ['schedule' => $item->id]) . ' class="btn btn-outline-primary btn-sm">Забронировать</td></tr>';
            $ua .= '<td><a href=' . route('booking', ['schedule' => $item->id]) . ' class="btn btn-outline-primary btn-sm">Забронювати</td></tr>';
            else:
            $rus .= "<td><a href=\"#\" class=\"btn btn-outline-primary btn-sm disabled\">Забронировать</td></tr>";
            $ua .= "<td><a href=\"#\" class=\"btn btn-outline-primary btn-sm disabled\">Забронювати</td></tr>";
            endif;
        }
        catch(\Exception $e)
        {
            Log::error("Error for schedule: " . $e->getMessage());
        }
}

    echo $rus;
    echo $ua;
    echo SCHEDULE_CLOSE_TABLE;
@endphp