@extends('layouts.front-main-layout')

@section('meta')
    <meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-9">
    <div style="text-align: center; margin-bottom: -100px;">
        <div class="main-div" style="background-color: #f7f6f6;">
            <!-- main -->
            <h2 style="margin-top: 50px;">{{ __('Новости')}}</h2>
            <div style="margin-bottom: 50px; width: 90%; margin: 0 auto; text-align: left; background-color: #161f27;">
                <div class="row" id="news-grid">
                    <!-- ajax content -->
                </div>
                <div class="row">
                    <div class="col">
                        @php $countPages = ceil($countAll/$countPerPage); @endphp
                    @if($countAll)
                    <ul class="pagination text-white" style="margin:20px 0;">
                        <li class="page-item" ><a class="page-link" href="#" style="background-color: #161f27;">Previous</a></li>
                        <li class="page-item" id="page-1" ><a class="page-link" href="#" style="background-color: #161f27;">1</a></li>
                        @for($i = 2; $i <= $countPages; $i++)
                        <li class="page-item" id="page-{{$i}}"><a class="page-link" href="#" style="background-color: #161f27;">{{$i}}</a></li>
                        @endfor
                        <li class="page-item"><a class="page-link" href="#" style="background-color: #161f27;">Next</a></li>
                    </ul>
                    @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-3">
    <div style="margin-top: 50px;">
        <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
            <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
        </div>
    </div>
    </div>
</div>
<script>
    var count = <?php echo ceil($countAll/$countPerPage) ?>;
    var currentPage = 0;
    var currentActive;
    $(document).ready(function(){

        // Handler event - Отображение результатов ajax запрос
        $(".pagination").click(function(event){
            event.preventDefault();
            var old = "#page-" + currentPage;
            var page = $(event.target).text();

            switch (page){
                case "Previous": {
                    if (currentPage != 1){
                        --currentPage;
                    }
                    else{
                         return;
                    }
                       
                    break;
                }
                case "Next": {
                    if (currentPage != count){
                        ++currentPage;
                    }
                    else{
                        return;
                    }

                    break;
                }
                default: {
                    if (currentPage == page){
                        return;
                    }
                    currentPage = +page;
                }
            }

            $(old).removeClass("active");
            currentActive = event.target;
            var actived = "#page-" + currentPage;

            $("#news-grid").html("<div class=\"text-center bg-white\"><div class=\"spinner-border \"></div></div>");

            $.ajax({
            url: '{{ route("post-ajax-news-pagination") }}',
            type: "POST",
            data:  {
                page: currentPage
            },
            headers: {
                'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (data) {
                $("#news-grid").html(data);
                $(actived).addClass("active");
            },
            error: function (msg) {
                $("#news-grid").html("<div class=\"alert alert-warning\">" +
                "<strong>Success!</strong> Ошибка!<br/>" +
                "status: " + msg.status + " " +
                msg.responseText + "</div>");
            }

        });
        });

        $("#page-1").trigger('click');
    });
</script>   
@endsection
