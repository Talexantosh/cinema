@extends('layouts.front-main-layout')
@php 
use  App\Models\Admin\AdPage;
$seoblock = AdPage::find($pageId);
@endphp
@section('meta')
    <meta name="description" content="{{optional($seoblock->seoblock)->description}}">
    <meta name="keywords" content="{{optional($seoblock->seoblock)->keywords}}">
@endsection

@section('style')
<!--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css">-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>-->
<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>-->
<style>
/* Make the image fully responsive */
.carousel-inner img {
    width: 100%;
    height: 100%;
}
</style>
@endsection

@section('content')
    <div style="text-align: center; margin-bottom: -155px;">
        <div class="main-div" style="background-color: #f7f6f6;">
            <!-- Top slader -->
            <!-- Slideshow container -->
            @if($banner->top_slader_status) 
            <div id="topCarousel" class="carousel slide" data-ride="carousel" data-interval="{{($banner->top_slader_speed + 1) * 5 * 1000}}" >

                <!-- Indicators -->
                <ul class="carousel-indicators">
                
                    <li data-target="#topCarousel" data-slide-to="0" class="active"></li>
                    @for($i = 1; $i < $banner->top_slader_count; $i++)
                        <li data-target="#topCarousel" data-slide-to="{{$i}}"></li>
                    @endfor
                </ul>

                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($topSladerGallery as $index => $slade)
                        @if($index == 0)
                            <div class="carousel-item active">
                                <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="1000" height="190">
                            </div>
                            @continue
                        @endif
                        <div class="carousel-item">
                            <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="1000" height="190">
                        </div>
                    @endforeach
                </div>

                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#topCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#topCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
                </div>
                <br>
                <br>
            @endif
            <!-- main -->
            <h2 style="margin-top: 50px;">{{ __('Смотрите сегодня')}}, {{ today()->format('d.m') }}</h2>
            <div style="margin-bottom: 50px; width: 90%; margin: 0 auto; text-align: left;">
                <div class="row">
                    @foreach($currentMovie as $index => $movie)
                    @if (is_null($movie) || $movie->disabled)
                        @continue
                    @endif
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{ route('front-page-movie', $movie->id) }}">
                                    <img src="{{ asset($movie->main_image_url) }}"  class="img-thumbnail" alt="{{ $movie->text->string_rus }}" height="70" />
                                    <p class="rus text-secondary">{{ $movie->text->string_rus }}</p>
                                    <p class="ua text-secondary">{{ $movie->text->string_ua }}</p>
                                </a>
                                <form action="{{ route('front-main-index') }}" method="get">
                                    <button type="submit" class="btn btn-success">{{ __('Купить билет')}}</button>
                                </form>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <h2>{{ __('Смотрите скоро')}}</h2>
            <div style="margin-bottom: 50px; width: 90%; text-align: left; margin: 0 auto;">
                <div class="row">
                    @foreach($soonMovie as $movie)
                    @if (is_null($movie->_movie) || $movie->_movie->disabled)
                        @continue
                    @endif
                    @php
                        $rus = $movie->_movie->parseDescriptionRus();
                        $ua = $movie->_movie->parseDescriptionUa();
                    @endphp
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{ route('front-page-movie', $movie->_movie->id) }}">
                                    <img src="{{ asset($movie->_movie->main_image_url) }}"  class="img-thumbnail" alt="{{ $movie->_movie->text->striing_rus }}" height="70" />
                                    <p class="rus">{{ $movie->_movie->text->string_rus }}</p>
                                    <h5 class="rus text-success">{{ (array_key_first($rus['release']) !== null)? $rus['release'][array_key_first($rus['release'])]: "" }}</h5>
                                    <p class="ua">{{ $movie->_movie->text->string_ua }}</p>
                                    <h5 class="ua text-success">{{ (array_key_first($ua['release']) !== null)? $ua['release'][array_key_first($ua['release'])]: "" }}</h5>
                                </a>
                            </div>
                        </div> 
                    </div>
                @endforeach
                </div>
            </div>

            @if($banner->bottom_slader_status)
            <div class="container mt-3" style="margin-bottom: 50px">
                <h2>{{ __('Новости и акции')}}</h2>
                <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="{{($banner->bottom_slader_speed + 1) * 5 * 1000}}" >

                <!-- Indicators -->
                <ul class="carousel-indicators">
                   
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    @for($i = 1; $i < $banner->bottom_slader_count; $i++)
                        <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                    @endfor
                </ul>
                
                <!-- The slideshow -->
                <div class="carousel-inner">
                    @foreach($bottomSladerGallery as $index => $slade)
                    @if($index == 0)
                    <div class="carousel-item active">
                        <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="1000" height="190">
                    </div>
                    @continue
                    @endif
                    <div class="carousel-item">
                        <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="1000" height="190">
                    </div>
                    @endforeach
                </div>
                
                <!-- Left and right controls -->
                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
                </div>
            </div>
            @endif  
            <h4 class="text-center">SEO текст</h4>
            <div class="text-left">
                <div class="rus">{{optional($seoblock->_description)->content_rus}}</div>
                <div class="ua">{{optional($seoblock->_description)->content_ua}}</div>
            </div>
        </div>
    </div>

@endsection
