@extends('layouts.front-main-layout')

@section('meta')
@endsection

@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-9">
    <div style="margin-bottom: -100px;">
        <div class="main-div" style="background-color: #161f27;">
            <!-- main --> 
            @if ($promotion->enabled)
            <div style="margin-bottom: 50px; width: 90%; margin: 0 auto; text-align: left; background-color: #161f27;">
                <div class="row">
                    <div class="col">
                        <h2 class="rus text-white">{{ $promotion->_name->string_rus }}</h2>
                        <h2 class="ua text-white">{{ $promotion->_name->string_ua }}</h2>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        @php $orgs = $promotion->getOrgs(); @endphp
                        @foreach($orgs['rus'] as $org)
                        <span class="rus rounded text-white mr-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                        @endforeach
                        @foreach($orgs['ua'] as $org)
                        <span class="ua rounded text-white mr-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                        @endforeach
                    </div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-6">
                        <!-- slider -->
                        <div id="myCarousel" class="carousel slide d-flex justify-content-end" data-ride="carousel"  data-interval="false">
                        <!-- Indicators -->
                        <ul class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <?php for($i = 1, $j = 2; $i < 5; $i++, $j++): ?>
                                <?php if($promotion->_gallery["item_url_$j"] == ""):
                                    continue; ?>
                                <?php endif; ?>
                                <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                            <?php endfor; ?>
                        </ul>
                        <!-- The slideshow -->
                        <div class="carousel-inner">
                            <?php for($i = 0, $j = 1; $i < 5; $i++, $j++): ?>
                                <?php if($promotion->_gallery["item_url_$j"] == ""):
                                    continue; ?>
                                <?php endif; ?>
                                <?php if($i == 0): ?>
                                <div class="carousel-item active">
                                    <img src="{{ asset($promotion->_gallery->item_url_1) }}" alt="" width="442" height="280">
                                </div>
                                <?php  continue; ?>
                                <?php endif; ?>
                                <div class="carousel-item">
                                    <img src="<?php echo asset($promotion->_gallery["item_url_$j"]) ?>" alt="" width="442" height="280">
                                </div>
                            <?php endfor; ?>
                        </div>
                        
                        <!-- Left and right controls -->
                        <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                            <span class="carousel-control-prev-icon"></span>
                        </a>
                        <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                            <span class="carousel-control-next-icon"></span>
                        </a>
                        </div>
                    </div>
                    <div class="col-6">
                        @php $content = $promotion->getDescriptionContent(); @endphp
                        @foreach($content['rus'] as $par)
                        <p class="rus text-muted">{!! $par !!}</p>
                        @endforeach
                        @foreach($content['ua'] as $par)
                        <p class="ua text-muted">{!! $par !!}</p>
                        @endforeach
                    </div>
                </div>       
            </div>
            @endif
        </div>
    </div>
    </div>
    <div class="col-3">
    <div style="margin-bottom: -100px;">
        <div style="margin-top: 50px;">
            <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
            </div>
        </div>
    </div>
    </div>
</div>
@endsection
