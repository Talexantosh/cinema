@extends('layouts.front-main-layout')

@section('meta')
@endsection

@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')
    <div style="text-align: center; margin-bottom: -100px;">
        <div class="main-div" style="background-color: #f7f6f6;">
            <!-- main -->
            <h2 style="margin-top: 50px;">{{ __('Наши кинотеатры')}}</h2>
            <div style="margin-bottom: 50px; width: 90%; margin: 0 auto; text-align: left;">
                <div class="row">
                    @foreach($cinemas as $index => $cinema)
                    <div class="col-6 text-center">
                        <div class="card">
                            <div class="card-body">
                                <a href="{{ route('front-cinema-index', $cinema->id) }}">
                                    <img src="{{ asset($cinema->logo_url) }}"  class="img-thumbnail" alt="{{ $cinema->_name->striing_rus }}" height="250" />
                                    <p class="rus text-secondary">{{ $cinema->_name->string_rus }}</p>
                                    <p class="ua text-secondary">{{ $cinema->_name->string_ua }}</p>
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
