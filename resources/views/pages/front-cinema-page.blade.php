@extends('layouts.front-main-layout')

@section('meta')
    <meta name="description" content="{{$cinema->seoblock->description}}">
    <meta name="keywords" content="{{$cinema->seoblock->keywords}}">
@endsection

@section('style')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@endsection

@section('content')
    <div>
        <div class="container" style="background-color: #f7f6f6; margin-top: 50px;">
            <!-- Top banner -->
            <div class="row">
                <div class="col-12 text-center">
                    <br/>
                    <img class="img-fluid" src="{{$cinema->img_url}}" alt="Top Banner">
                </div>
            </div>
            <br/><br/>
            <!-- main -->
            <div class="row">
                <div class="col-3">
                    <div class="row">
                        <div class="col">
                            <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                                <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                            </div>
                        </div>
                    </div>
                    <!-- side hall-->
                    <div class="row">
                        <div class="col">
                            <br/>
                            <div class="list-group">
                                @foreach($cinema->halls as $hall)
                                    <a href="{{ route('front-cinema-hall-index', [$hall->id]) }}" class="rus list-group-item list-group-item-action text-info">
                                        {{$hall->_name->string_rus}}
                                    </a>
                                    <a href="{{ route('front-cinema-hall-index', [$hall->id]) }}" class="ua list-group-item list-group-item-action text-info">
                                        {{$hall->_name->string_ua}}
                                    </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-9">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{$cinema->logo_url}}" class="rounded" alt="Logo image">
                        </div>
                        <div class="col-8 text-center">
                            <h2 class="rus">{{ $cinema->_name->string_rus }}</h2>
                            <h2 class="ua">{{ $cinema->_name->string_ua }}</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-justify">
                            <br/>
                            <p class="rus">{{$cinema->_description->text_rus}}</p>
                            <p class="ua">{{$cinema->_description->text_ua}}</p>
                            <br/>
                        </div>
                    </div>
                    <div class="row  d-flex justify-content-center">
                        <div class="col-12">
                            <h2 class="text-center">{{ __("Условия")}}</h2>
                        </div>
                        <div class="col-11 bg-dark text-justify">
                            <hr class="text-secondary"/>
                                <div class="rus">{!!$cinema->_conditions->text_rus!!}</div>
                                <div class="ua">{!!$cinema->_conditions->text_ua!!}</div>
                            </div>
                        </div>
                        <div class="row">
                        <div class="col-12" style="width: 90%;">
                            <div class="container mt-3" style="margin-bottom: 50px">
                                <h2 class="text-center">{{ __('Фотогалерея')}}</h2>
                                <div id="myCarousel" class="carousel slide " data-interval="false">

                                    <!-- Indicators -->
                                    <ul class="carousel-indicators">
                                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                            @foreach($cinema->gallery as $i => $item)
                                                @if($i == 0)
                                                    @continue
                                                @endif
                                                <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                            @endforeach
                                    </ul>
                
                                    <!-- The slideshow -->
                                    <div class="carousel-inner">
                                        @foreach($cinema->gallery as $index => $slade)
                                        @if($index == 0)
                                        <div class="carousel-item active">
                                            <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="840" height="560">
                                        </div>
                                        @continue
                                        @endif
                                        <div class="carousel-item">
                                            <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="840" height="560">
                                        </div>
                                        @endforeach
                                    </div>
                
                                    <!-- Left and right controls -->
                                    <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                    </a>
                                    <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                    </a>                    
                                </div>
                            </div>
                        </div>
                    </div>
                        
                        </div>
                        
                </div>
            </div>

        </div>
                   
    </div>

@endsection
