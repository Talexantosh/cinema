@extends('layouts.front-main-layout')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{asset('dist/css/booking.css')}}" />
@endsection

@section('style')
<style>
    .share-size {
        height: fit-content;
    }
</style>
@endsection

@section('content')
<br/>
<div class="container" style="min-height: 480px; background-color:rgb(255,255,255);">
    <div class="row">
        <div class="col-12" style="height: 10%;"> 
        <br/> 
            @if(session()->has('status'))
                <div class="center" style="margin: 0 auto; margin-top: 30px; width: 40%;">
                    <div class="alert alert-success">
                        <ul>
                            <li>{{session()->get('status')}}</li>
                        </ul>
                    </div>
                </div>
            @endif 
            @php session()->forget('status');; @endphp
            <br/>        
        </div>
    </div>
    <div class="row" >
        <div class="col-9">
            <!-- todo -->
            @if (!isset($model))
                 redirect()->abort(503);
            @endif
            <div class="row" style="width: 90%; margin: 0 auto">
                <div class="col-3" >
                    <img width="90%" src="{{asset($model->_linkMovie->main_image_url)}}"/>
                </div>
                <div class="col-9" >
                    <div class="row">
                        <div class="col-12">
                            <div style="padding: 5px; color: white; width: max-content; background-color: #b63d26; border-radius: 10px;">
                                <h5 class="rus">{{optional($model->_movie())->string_rus}}</h5>
                                <h5 class="ua">{{optional($model->_movie())->string_ua}}</h5>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div style="margin-top: 10px;">
                                <span style="color: #746f6e">{{date('d.m.Y', strtotime("today")) }}</span>&nbsp;
                                <span style="color: #746f6e">{{ $model->begin_time }}</span>>&nbsp;
                                <span class="rus" style="color: #746f6e">Зал: {{ optional($model->_hall())->string_rus }}</span>
                                <span class="ua" style="color: #746f6e">Зала: {{ optional($model->_hall())->string_ua }}</span>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-1 fs-6 lh-1" >
                            ЦЕНА В ГРН:
                        </div>
                        <div class="col-1" >
                            <div style="padding: 5px 9px 0px 9px; color: white; font-weight: bolder; background-color: #f9c835; width: min-content; height: 36px;">
                            {{$model->price_ticket}}
                            </div>
                        </div>
                        <div class="col-3" >
                            ЗАБРОНИРОВАНО:
                        </div>
                        <div class="col-1" >
                            <div style="padding: 10px; color: white; font-weight: bolder; background: url(<?php echo asset('dist/img/book.png')?>); width: 36px; height: 36px;">
                            </div>
                        </div>
                        <div class="col-2 fs-6 lh-1" >
                            ВАШИ БИЛЕТЫ:
                        </div>
                        <div class="col-3" >
                            <div class="row" style="border: 5px solid yellow">
                                <div class="col">
                                    БИЛЕТОВ: <span id='count_' style="color: #b75b46">0</span>
                                </div>
                                <div class="col">
                                    СУММА: <span id='sum_' style="color: #b75b46">0</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form action="{{route('book-ticket-form', ['schedule' => $model->id, 'user_id' => Auth::guard('customer')->id()])}}" enctype="multipart/form-data" method="post">
            @csrf
            <div class="row">
                @php
                    $places = json_decode($model->beginBooking()->booking_schema, true);
                @endphp
                <div class="col-12">
                    <br/><br/>
                    <div style="margin: 0 auto; height: 400px;">
                        <h5 class="text-center">ЭКРАН</h5>
                        <hr class="rounded_underline" style="margin: 0px 10px 100px 80px;"/>
                        @foreach($places as $number => $row)
                            <div class="fs-6 lh-1" style="float: left; width: 30px;">
                                РЯД {{$number}}
                            </div>
                            <div style="width: max-content; margin: 0 auto;">
                            @foreach($row as $num => $place)
                                @if($place)
                                    <div class="inavailable_place">

                                    </div>
                                @else
                                    <div class="place" target-id="{{$number}}_checkbox_{{$num + 1}}">
                                        <input type="checkbox" name="{{$number}}[{{$num + 1}}]" id="{{$number}}_checkbox_{{$num + 1}}" style="display: none;"/>
                                        {{$num+1}}
                                    </div>
                                @endif
                            @endforeach
                            </div>
                            <div style="height: 50px;"></div>
                        @endforeach
                            
                    </div>
                </div>
            </div>
            <br/><br/><br/>
            <div class="row">
                <div class="col-12">
                    <p class="text-danger rus">Стоимость услуги бронирования - 3 гривни за каждое место</p>
                    <p class="text-danger ua">Вартість послуги бронювання – 3 гривні за кожне місце</p>
                    <br>
                    <p class="rus fs-6">ЗАБРОНИРОВАННЫЕ БИЛЕТЫ НУЖНО ВЫКУПИТЬ В КАССЕ КИНОТЕАТРА НЕ ПОЗДНЕЕ ЧЕМ ЗА ПОЛЧАСА ДО НАЧАЛА СЕАНСА</p>
                    <p class="ua fs-6">ЗАБРОНОВАНІ КВИТКИ ПОТРІБНО ВИКУПИТИ У КАСI КИНОТЕАТРА НЕ ПІЗНІШЕ НІЖ ЗА ПІВГОДИНИ ДО ПОЧАТКУ СЕАНСУ</p>
                    @if (Auth::guard('customer')->user())
                        @if(strtotime($model->begin_time ) - time() > 9000 )
                        <div class="text-center">
                            <button type="submit" id="button_book" style="margin: 0 auto; margin-top: 10px;" class="btn btn-outline-secondary">Забронировать</button>
                        </div>
                        @else
                            <p class="text-danger rus">Бронирование окончено!</p>
                            <p class="text-danger ua">Бронювання закінчено!</p>
                        @endif
                    @else
                        <p class="text-danger rus">Чтобы бронировать билеты - авторизируйтесь!</p>
                        <p class="text-danger ua">Щоб бронювати квитки – авторизуйтесь!</p>
                    @endif   
                </div>
            </div>
            </form>
            <br/><br/>
        </div>
        <div class="col-3">
            <!-- todo реклама -->
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                        <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@if (Auth::guard('customer')->user() && strtotime($model->begin_time ) - strtotime('now') > 1800 )
<script>
    $(function(){
        $('div.place').on('click', function() {
                        var div = $(this);
                        var target = $('#' + div.attr('target-id'));
                        if(div.css('background-color') === 'rgb(238, 128, 51)')
                        {
                            div.css('background-image', 'url(<?php echo asset('dist/img/book.png')?>)');
                            div.css('color', 'transparent');
                            div.css('background-color', 'rgb(218, 218, 218)');
                            target.prop('checked', true);
                        }
                        else {
                            div.css('background-color', 'rgb(238, 128, 51)');
                            div.css('color', 'white');
                            div.css('background-image', '');
                            target.prop('checked', false);
                        }
                        $('#count_').html(countPlaces());
                            $('#sum_').html(countPlaces() * (parseFloat("<?php echo $model->price_ticket?>") + 3));
                    })
                    function countPlaces (){
                        var l = document.getElementsByClassName('place');
                        let place = 0;
                        for(let i=0;i<l.length;i++){
                            if(window.getComputedStyle(l[i]).backgroundColor === 'rgb(218, 218, 218)'){
                                place++;
                            }
                        }
                        if (place !== 0)
                        {
                            $('#button_book').css('display', 'block');
                        }
                        else
                        {
                            $('#button_book').css('display', 'none');
                        }
                        return place;
                    }
    });
</script>
                @endif 
@endsection