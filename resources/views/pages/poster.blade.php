@extends('layouts.front-main-layout')

@section('meta')
@endsection

@section('style')
<style>
.bg-custom{
    background: #171e28;
}
.poster-a{
    text-align: center;
    font-size: 17px;
    font-weight: 600;
    border-bottom: 1px solid gray;
}
.arrow-left-active {
    width:0;
    height:0;
    border-top:30px solid transparent;
    border-bottom:30px solid transparent;
    border-left:30px solid #e6e6e6;
    position: relative;
    left: 97%;
    z-index: 1;
}
.arrow-button-active {
    float:left;
    height:60px;
    background:#e6e6e6;
    width: 100%;
    line-height:60px;
    font-weight:bold;
}
.arrow-left {
    width:0;
    height:0;
    border-top:30px solid transparent;
    border-bottom:30px solid transparent;
    border-left:30px solid #f7f7f7;
    float:right;
}
.arrow-button {
    float:left;
    height:60px;
    background:#f7f7f7;
    width:213.5px;
    line-height:60px;
    font-weight:bold;
}
</style>
@endsection

@section('content')
<div class="row" style="margin-top: 50px;width: 102%;">
        <div class="col-2" style="left: 20px; ">
            <div>
                <script type="text/javascript">
                    document.write(
                        '<a href=\"{{route("front-posters-index")}}\" class=\"poster-a\" style=\"color: black; margin-bottom: 50px\"><div class=\"arrow-button-active\" style=\"margin-right: -10px\">{{ __("Афиша")}}</div><div class=\"arrow-left-active\"></div></a>' +
                        '<a href=\"{{route("front-soon-index")}}\" class="poster-a\" style=\"color: black; margin-bottom: 50px\"><div class=\"arrow-button\">{{ __("Скоро")}}</div></a>');
                </script>
            </div>
        </div>
        <div class="col-9">
            <div style="background-color: #161f27;">
                <div class="row">
                @foreach($movies as $movie)
                    @if (!$movie->_movie->disabled)
                    <div class="col-3">
                        <div class="card">
                            <div class="card-body bg-custom">
                                <p class="text-center" style="color: white; font-size: 10px;">{{ __("Сейчас в кино")}}</p>
                                <div>
                                    <a href="{{route('front-page-movie', $movie->_movie->id)}}">
                                        <img src="{{ asset($movie->_movie->main_image_url) }}"  class="img-thumbnail" alt="{{ $movie->_movie->text->striing_rus }}" height="70" />
                                        <p class="rus">{{$movie->_movie->text->string_rus}}</p>
                                        <p class="ua">{{$movie->_movie->text->string_ua}}</p>
                                    </a>
                                    @php $rus = $movie->_movie->parseDescriptionRus(); 
                                         $ua = $movie->_movie->parseDescriptionUa();
                                    @endphp
                                    @if(is_array($rus['additional']))
                                    <table class="rus table table-borderless table-sm" style="color: white; font-size: 10px;">
                                        <tbody>
                                            @php $additional = (array_key_first($rus['additional']) !== null)? $rus['additional'][array_key_first($rus['additional'])]: []; @endphp
                                            
                                            @foreach($additional as $key => $value)
                                                @if ($loop->index % 2 == 0)
                                                    <tr>
                                                        <td>{{$value}}</td>
                                                @else
                                                        <td>{{$value}}</td>
                                                    </tr>
                                                @endif
                                            @if ($loop->index >= 11)
                                                @break
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                    @if(is_array($ua['additional']))
                                    <table class="ua table table-borderless table-sm" style="color: white; font-size: 10px;">
                                        <tbody>
                                            @php $additional = (array_key_first($ua['additional']) !== null)? $ua['additional'][array_key_first($ua['additional'])]: []; @endphp

                                            @foreach($additional as $value)
                                                @if ($loop->index % 2 == 0)
                                                    <tr>
                                                        <td>{{$value}}</td>
                                                @else
                                                        <td>{{$value}}</td>
                                                    </tr>
                                                @endif
                                            @if ($loop->index >= 11)
                                                @break
                                            @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>
@endsection