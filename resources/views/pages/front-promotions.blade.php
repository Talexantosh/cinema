@extends('layouts.front-main-layout')

@section('meta')
@endsection

@section('style')
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-9">
    <div style="text-align: center; margin-bottom: -100px;">
        <div class="main-div" style="background-color: #f7f6f6;">
            <!-- main -->
            <h2 style="margin-top: 50px;">{{ __('Акции и скидки')}}</h2>
            <div style="margin-bottom: 50px; width: 90%; margin: 0 auto; text-align: left; background-color: #161f27;">
                <div class="row">
                    @foreach($promotions as $index => $promotion)
                    @if ($promotion->enabled)
                    <div class="col-6">
                        <div class="card">
                            <div class="card-body" style="background-color: #161f27;">
                                <a href="{{ route('front-promotion-index', $promotion->id) }}">
                                    <img src="{{ asset($promotion->_gallery->item_url_1) }}"  class="img-thumbnail" alt="{{ $promotion->_name->string_rus }}" height="250" />
                                </a>
                                <a href="{{ route('front-promotion-index', $promotion->id) }}">
                                    <span class="rus">{{ $promotion->_name->string_rus }}</span>
                                    <span class="ua">{{ $promotion->_name->string_ua }}</span>
                                </a>
                                <div>
                                    @php $orgs = $promotion->getOrgs(); @endphp
                                    <span class="rounded text-white" style="background-color: rgb(5,5,5);">{{ date("d.m.Y", strtotime($promotion->date_publish)) }}</span>
                                    @foreach($orgs['rus'] as $org)
                                    <span class="rus rounded text-white ml-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                                    @endforeach
                                    @foreach($orgs['ua'] as $org)
                                    <span class="ua rounded text-white ml-4" style="background-color: rgb(230,0,230);">{{ $org }}</span>
                                    @endforeach
                                </div>
                                <div>
                                    @php $content = $promotion->getDescriptionContent(); @endphp
                                    @foreach($content['rus'] as $par)
                                    <p class="rus text-muted">{!! $par !!}...</p>
                                    @break
                                    @endforeach
                                    @foreach($content['ua'] as $par)
                                    <p class="ua text-muted">{!! $par !!}...</p>
                                    @break
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-3">
    <div style="margin-top: 50px;">
        <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
            <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
        </div>
    </div>
    </div>
</div>
@endsection
