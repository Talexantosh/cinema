@extends('layouts.front-main-layout')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('style')
<style>
    .share-size {
        height: fit-content;
    }
</style>
@endsection

@section('content')
<br/>
<div class="container">
    <div class="row">
        <div class="col-12" style="height: 10%;">           
        </div>
    </div>
    <div class="row">
        <div class="col-12 justify-content-*-center bg-dark">
            <div class="row flex-nowrap">
                <div class="col-3 justify-content-*-end bg-dark">
                    <div class="row text-light">
                        <div class="col-5 share-size">
                            <div class="rus small">Показывать только:</div>
                            <div class="ua small">Показувати лише:</div>
                        </div>
                        <div class="col-2 d-flex align-items-end">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="type2D" value=" 2D " checked>
                                <label class="form-check-label rounded" for="type2D" style="background-color: rgb(128, 0, 128);">2D</label>
                            </div>
                        </div>
                        <div class="col-2 d-flex align-items-end">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="type3D" value="3D">
                                <label class="form-check-label rounded" for="type3D" style="background-color: rgb(0, 0, 255);"> 3D </label>
                            </div>
                        </div>
                        <div class="col-3 d-flex align-items-end">
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="checkbox" id="typeIMAX" value="3D">
                                <label class="form-check-label rounded" for="typeIMAX" style="background-color: rgb(255, 0, 0);"> IMAX </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col input-group m-0 p-1">
                    <button class="btn btn-primary" type="button" id="post-search-schedule">
                        <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg>
                    </button>
                </div>
                <div class="col input-group d-flex align-items-end m-0 p-1">
                    <select name="cinemaId" class="cinema_id share-size" >
                        @if (count($modelCinema) > 1)
                            <option class="ua" value="all" >Кінотеатри: Усі</option>
                            <option class="rus" value="all" selected>Кинотеатры: Все</option>
                        @endif
                        @foreach($modelCinema as $cinema)
                            <option class="ua" value="{{$cinema->id}}">{{$cinema->_name->string_ua}}</option>
                            <option class="rus" value="{{$cinema->id}}">{{$cinema->_name->string_rus}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col input-group d-flex align-items-end m-0 p-1">
                    <div class="row small pl-1 pr-3">
                        <div class="col-2 text-left"><span class="rus">С: </span><span class="ua">З: </span></div>
                        <div class="col-10 flex-nowrap">
                            <input type="date" id="begin-date" name="beginDate" class="share-size"
                            value="{{today()->format('Y-m-d')}}"
                            min="{{today()->format('Y-m-d')}}" max="{{today()->addDays(7)->format('Y-m-d')}}">
                        </div>
                    </div>
                    <div class="row small pr-3">
                        <div class="col-2 text-left">По:</div>
                        <div class="col-10 flex-nowrap"><input type="date" id="end-date" name="endDate" 
                            value="{{today()->format('Y-m-d')}}"
                            min="{{today()->format('Y-m-d')}}" max="{{today()->addDays(7)->format('Y-m-d')}}">
                        </div>
                    </div>
                </div>
                <div class="col input-group d-flex align-items-end m-0 p-1">
                    <select name="movieId" class="movie_id share-size" >
                        @if (count($modelMovie) > 1)
                            <option class="ua" value="all" >Фільми: Всі</option>
                            <option class="rus" value="all" selected>Фильмы: Все</option>
                        @endif
                        @foreach($modelMovie as $movie)
                            <option class="rus" value="{{$movie->_movie->id}}">{{$movie->_movie->text->string_rus}}</option>
                            <option class="ua" value="{{$movie->_movie->id}}">{{$movie->_movie->text->string_ua}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col input-group d-flex align-items-end m-0 p-1">
                    <div id="select-hall-id">
                        <select id="hall-id" name="hallId" class="hall_id" >
                            <option class="ua" value="all" >Зал: Усі</option>
                            <option class="rus" value="all" selected>Зал: Все</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row" style="min-height: 480px; background-color:rgb(255,255,255);">
        <div class="col-9" id="table-with-movie-schedules">
            <!-- todo вставить таблицу из layouts.schedule-table -->
            @include('layouts.schedule-table', ['collection' => $allSchedule])
        </div>
        <div class="col-3">
            <!-- todo реклама -->
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                        <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@php
    $option = [];
    foreach($modelCinema as $cinema){
        foreach($cinema->halls as $hall){
            $option[] = [
                "rus" => $hall->_name->string_rus,
                "ua" => $hall->_name->string_ua,
                "id" =>$hall->id
                ];
        }

        $defaultValues[$cinema->id] = $option;
        $option = [];
    }
@endphp

                        
<script>
   
    var default_values = <?php echo json_encode($defaultValues); ?>;
    $(document).ready(function(){
        // Handler event 1 - Отображение залов в зависимости от выбранного кинотеатра
        $('select.cinema_id').on('change', function(){
            var current = $(this).val();
            $('#select-hall-id').html('<select id="hall-id" name="hallId" class="hall_id" ></select>');
            var options = '';
            if (current === 'all'){
                options += '<option class="rus" value="all">Зал: Все</option>'; // fetch options
                options += '<option class="ua" value="all">Зал: Усі</option>'; // fetch options
            }
            else{
                $.each(default_values[current], function(index, values){
                    options += '<option class="rus" value="'+ values.id +'">'+ values.rus +'</option>'; // fetch options
                    options += '<option class="ua" value="'+ values.id +'">'+ values.ua +'</option>'; // fetch options
                });
                
                options += '<option class="rus" value="all">Зал: Все</option>'; // fetch options
                options += '<option class="ua" value="all">Зал: Усі</option>'; // fetch options
            }
            
            $('#hall-id').html(options); // add options
        });

        // Handler event 2 - Отображение результатов поиска через ajax запрос
        $("#post-search-schedule").click(function(){
            $("#table-with-movie-schedules").html("<div class=\"spinner-border\"></div>");
            
                var cinemaId = $('select.cinema_id option:selected').val(),
                    movieId = $('select.movie_id option:selected').val(),
                    hallId = $('select.hall_id option:selected').val(),
                    //beginDate = $('input[type="date"] #begin-date').val(), 
                    beginDate = $('#begin-date').val(), 
                    endDate =  $('#end-date').val(), 
                    type2D = + $('#type2D').prop( "checked" ),
                    type3D = + $('#type3D').prop( "checked" ),
                    typeImax = + $('#typeIMAX').prop( "checked" )

                $.ajax({
                url: '{{ route("post-ajax-movie-schedules") }}',
                type: "POST",
                data:  {
                    movieId: movieId,
                    cinemaId: cinemaId,
                    hallId: hallId,
                    beginDate: beginDate,
                    endDate: endDate,
                    type2D: type2D,
                    type3D: type3D,
                    typeImax: typeImax
                },
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                success: function (data) {
                    $("#table-with-movie-schedules").html(data);
                },
                error: function (msg) {
                    $("#table-with-movie-schedules").html("<div class=\"alert alert-warning\">" +
                    "<strong>Success!</strong> Извините, произошла ошибка.<br/>" +
                    "status: " + msg.status + " " +
                    msg.responseText + "</div>");
                }

            });
        });
    });
</script>   
 
@endsection