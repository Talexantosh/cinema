@extends('layouts.front-main-layout')

@section('meta')
    <meta name="description" content="{{$model->seoblock->description}}">
    <meta name="keywords" content="{{$model->seoblock->keywords}}">
@endsection

@section('style')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@endsection

@section('content')
<div class="container" style="background-color: #f7f6f6; margin-top: 50px;">
    <!-- Top banner -->
    <div class="row">
        <div class="col-12 text-center">
            <br/>
            <img class="img-fluid" src="{{$model->main_image_url}}" alt="Top Banner">
        </div>
    </div>
    <br/><br/>
    <!-- main -->
    <div class="row">
        <div class="col-9">
            <div class="row">
                <div class="col rus pl-5">
                    {!! optional($model->_description)->content_rus !!}
                </div>
                <div class="col ua pl-5">
                    {!! optional($model->_description)->content_ua !!}
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <hr/>
                    <h1 class="text-center">Галерея</h1>
                    <div class="d-flex justify-content-center">
                    <div class="card shadow">
                        <div class="card-body">
                             @include('layouts.carousel-gallery', ['model' => $model])
                        </div>
                    </div>
                    </div>
                    <br/><br/>
                </div>
            </div>                     
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                        <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>             
@endsection
