@extends('layouts.front-main-layout')

@section('meta')
    <meta name="description" content="{{$hall->seoblock->description}}">
    <meta name="keywords" content="{{$hall->seoblock->keywords}}">
@endsection

@section('style')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<style>
.box {
  white-space: nowrap; 
  overflow: hidden;
  text-overflow: clip;
}
</style>
@endsection

@section('content')
<div>
    <div class="container" style="background-color: #f7f6f6; margin-top: 50px;">
        <!-- Top banner -->
        <div class="row">
            <div class="col-12 text-center">
                <br/>
                <img class="img-fluid" src="{{$hall->img_url}}" alt="Top Banner">
            </div>
        </div>
        <br/><br/>
            <!-- main -->
        <div class="row">
            <!-- side panel -->
            <div class="col-3">
                <div class="row">
                    <div class="col">
                        <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                            <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="box col">
                        <br/>
                        @php $records = $hall->populateShowtime(); @endphp

                        <div class="list-group">
                            @foreach($records as $record)
                                <a href="{{ route('front-ticket-booking', ['cinemaId' => $hall->cinema->id, 'hallId' => $hall->id, 'movieId' => $record['movie']]) }}" class="rus list-group-item list-group-item-action text-info">
                                    {{$record["time"] . " " . $record["nameRus"] . " " . $record["price"]}}
                                </a>
                                <a href="{{ route('front-ticket-booking', ['cinemaId' => $hall->cinema->id, 'hallId' => $hall->id, 'movieId' => $record['movie']]) }}" }}" class="ua list-group-item list-group-item-action text-info">
                                    {{$record["time"] . " " . $record["nameUa"] . " " . $record["price"]}}
                                </a>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <!-- 2 column -->
            <div class="col-9">

                @php $contents = $hall->getDescriptions() @endphp
                <!-- main -->
                @foreach($contents["rus"] as $tags)
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="rus">{{ $tags["h"] }}</h2>
                    </div>
                </div>
                <br/>
                    @foreach($tags["p"] as $p)
                <div class="row">
                    <div class="col-12 text-justify">
                        <p class="rus">{{$p}}</p>
                    </div>
                </div>
                    @endforeach
                <br/>
                @endforeach

                @foreach($contents["ua"] as $tags)
                <div class="row">
                    <div class="col-12 text-center">
                        <h2 class="ua">{{ $tags["h"] }}</h2>
                    </div>
                </div>
                <br/>
                    @foreach($tags["p"] as $p)
                <div class="row">
                    <div class="col-12 text-justify">
                        <p class="ua">{{$p}}</p>
                    </div>
                </div>
                    @endforeach
                <br/>
                @endforeach
                
                <div class="row  d-flex justify-content-center">
                    <div class="col-12">
                        <h2 class="text-center">{{ __("Карта зала")}}</h2>
                        <div>
                            <img class="img-fluid" src="{{$hall->schema_url}}" alt="Schema hall">
                        </div>
                    </div>
                </div>
                <div class="row d-flex justify-content-center">
                    <div class="col-12" style="width: 90%;">
                        <div class="container mt-3" style="margin-bottom: 50px">
                            <h2 class="text-center">{{ __('Фотогалерея')}}</h2>
                            <div id="myCarousel" class="carousel slide" data-interval="false">

                                <!-- Indicators -->
                                <ul class="carousel-indicators">
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                        @foreach($hall->gallery as $i => $item)
                                            @if($i == 0)
                                                @continue
                                            @endif
                                            <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                        @endforeach
                                </ul>
            
                                <!-- The slideshow -->
                                <div class="carousel-inner">
                                    @foreach($hall->gallery as $index => $slade)
                                    @if($index == 0)
                                    <div class="carousel-item active">
                                        <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="840" height="560">
                                    </div>
                                    @continue
                                    @endif
                                    @if( $slade->url == "")
                                        @continue
                                    @endif
                                    <div class="carousel-item">
                                        <img src="{{ asset($slade->url) }}" alt="{{ $slade->text }}" width="840" height="560">
                                    </div>
                                    @endforeach
                                </div>
            
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>                    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>          
</div>

@endsection
