@extends('layouts.front-main-layout')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection

@section('style')
@endsection

@section('content')
<br/>
<div style="width: 80%; margin: 0 auto">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-center">
                    {!! $movie->trailer_url !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div style="width: 80%; margin: 0 auto">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="row" >
                        <div class="col-3">
                            <h6>{{ __('Расписание и билеты:') }}</h6>
                        </div>
                        <div class="col-2">
                            <span>
                                <label for="select-date">{{ __('Выберите дату') }}</label>
                                <input type="date" id="select-date" name="selectedDate"
                                    value="{{today()->format('Y-m-d')}}"
                                    min="{{today()->format('Y-m-d')}}" max="{{today()->addDays(7)->format('Y-m-d')}}">
                            </span>
                        </div>
                        <div class="col-5 input-group mb-3">
                            <select name="cinemaId" class="cinema_id" >
                                @if (count($modelCinema) > 1)
                                    <option class="rus" value="all">Все</option>
                                    <option class="ua" value="all">Усі</option>
                                @endif
                                @foreach($modelCinema as $cinema)
                                    <option class="rus" value="{{$cinema->id}}">{{$cinema->_name->string_rus}}</option>
                                    <option class="ua" value="{{$cinema->id}}">{{$cinema->_name->string_ua}}</option>
                                @endforeach
                            </select>
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="button" id="post-search-schedule">
                                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="search" class="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" height="16"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z"></path></svg>
                                </button>
                            </div>
                        </div>
                        <div class="col-2">
                            <span>
                                <div class="custom-control">
                                    <input type="checkbox" class="custom-control-input" id="type-2D" name="type2D" checked >
                                    <label class="custom-control-label" for="type-2D">2D</label>
                                </div>
                                <div class="custom-control">
                                    <input type="checkbox" class="custom-control-input" id="type-3D" name="type3D">
                                    <label class="custom-control-label" for="type-3D">3D</label>
                                </div>
                                <div class="custom-control">
                                    <input type="checkbox" class="custom-control-input" id="type-IMAX" name="typeImax">
                                    <label class="custom-control-label" for="type-IMAX">IMAX</label>
                                </div>
                            </span>
                        </div>
                    </div>
                    <div class="row" >
                        <div class="col-12">
                        <div id="table-with-movie-schedules" class="d-flex justify-content-center"></div>
                        </div>
                    </div>                
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var movieId = <?php echo $movie->id; ?>;
            $("#post-search-schedule").click(function(){
                $("#table-with-movie-schedules").html("<div class=\"spinner-border\"></div>");
                
                 var cinemaId = $('select.cinema_id option:selected').val(),
                     selectedDate = $('input[type="date"]').val(), 
                     type2D = + $('#type-2D').prop( "checked" ),
                     type3D = + $('#type-3D').prop( "checked" ),
                     typeImax = + $('#type-IMAX').prop( "checked" )

                 $.ajax({
                    url: '{{ route("post-ajax-movie-schedules") }}',
                    type: "POST",
                    data:  {
                        movieId: movieId,
                        cinemaId: cinemaId,
                        hallId: 'all',
                        beginDate: selectedDate,
                        endDate: selectedDate,
                        type2D: type2D,
                        type3D: type3D,
                        typeImax: typeImax
                    },
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function (data) {
                        $("#table-with-movie-schedules").html(data);
                    },
                    error: function (msg) {
                        $("#table-with-movie-schedules").html("<div class=\"alert alert-warning\">" +
                        "<strong>Success!</strong> Извините, произошла ошибка.<br/>" +
                        "status: " + msg.status + " " +
                        msg.responseText + "</div>");
                    }

                });
            });
        });
    </script>   
 <!-- movie card -->     
    <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row" >
                            <div class="col-6 d-flex justify-content-center">
                                <img src="{{ asset($movie->gallery[0]->url) }}"  class="img-thumbnail" alt="" style="width: 520px; height: 480px; object-fit: cover; object-position: 25% -25%;" />
                            </div>
                            
                            <div class="col-6">
                            @php
                                $rus = $movie->parseDescriptionRus();
                                $ua = $movie->parseDescriptionUa();
                            @endphp
                                    <h2 class="rus text-center">{{ $movie->text->string_rus }}</h2>
                                    <p class="rus"><?= $rus['main']['descript']; ?></p>
                                    <h2 class="ua text-center">{{ $movie->text->string_ua }}</h2>
                                    <p class="ua"><?= $ua['main']['descript']; ?></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <br/>
                            <!-- table -->
                            @if(is_array($rus['additional']))
                            <table class="rus table table-dark table-striped"><tbody>
                                @php $additional = (array_key_first($rus['additional']) !== null)? $rus['additional'][array_key_first($rus['additional'])]: []; @endphp
                                    @foreach($additional as $value)
                                        @if ($loop->index % 2 == 0)
                                            <tr>
                                                <td >{{$value}}</td>
                                        @else
                                                <td>{{$value}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                            @if(is_array($ua['additional']))
                            <table class="ua table table-dark table-striped"><tbody>
                                @php $additional = (array_key_first($ua['additional']) !== null)? $ua['additional'][array_key_first($ua['additional'])]: []; @endphp
                                    @foreach($additional as $value)
                                        @if ($loop->index % 2 == 0)
                                            <tr>
                                                <td >{{$value}}</td>
                                        @else
                                                <td>{{$value}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                </tbody>
                            </table>
                            @endif
                            </div>
                            <div class="col-2">
                            </div> 
                            <div class="col-4">
                            <!-- slider -->
                                <h2 class="d-flex justify-content-center">{{ __('Кадры и постеры')}}</h2>
                                <div id="myCarousel" class="carousel slide d-flex justify-content-end" data-ride="carousel"  data-interval="false">

                                <!-- Indicators -->
                                <ul class="carousel-indicators">
                                
                                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                                    @for($i = 1, $count = count($movie->gallery); $i < $count; $i++)
                                        <li data-target="#myCarousel" data-slide-to="{{$i}}"></li>
                                    @endfor
                                </ul>
                                
                                <!-- The slideshow -->
                                <div class="carousel-inner">
                                    @foreach($movie->gallery as $index => $slade)
                                    @if($index == 0)
                                    <div class="carousel-item active">
                                        <img src="{{ asset($slade->url) }}" alt="" width="340" height="190">
                                    </div>
                                    @continue
                                    @endif
                                    @if ($slade->url == "")
                                        @continue
                                    @endif
                                    <div class="carousel-item">
                                        <img src="{{ asset($slade->url) }}" alt="" width="340" height="190">
                                    </div>
                                    @endforeach
                                </div>
                                
                                <!-- Left and right controls -->
                                <a class="carousel-control-prev" href="#myCarousel" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#myCarousel" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>   
@endsection