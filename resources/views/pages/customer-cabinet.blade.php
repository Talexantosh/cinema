@extends('layouts.front-main-layout')

@section('meta')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dist/css/spinner-submit.css') }}" >  
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
@endsection

@section('style')

@endsection

@section('content')
<div style="background-color: rgb(255, 255, 255)!important; margin: 50px 50px 50px 50px;">
<br/><br/>
<h1 class="text-center">Страница Клиента</h1>
<br/><br/>
    <div>
    @if (isset($alert))
        <div class="alert alert-danger">
            <ul>
                <li>{{ $alert }}</li>
            </ul>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    </div>
    <div id="admin-customer-form ">
         @include('admin.layouts.customer-form')
    </div>
    <br/><br/>
</div>
   
<script type="text/javascript">
$(function(){
    $("#customer-form").on("submit", function(){
        event.preventDefault();
        function buttonHandle(disabled, removeClass){
            $("#submit-button").attr('disabled', disabled);
            if (removeClass)
                $(".submit-spinner").removeClass('submit-spinner_hide');
            else 
                $(".submit-spinner").addClass('submit-spinner_hide');
        };
        buttonHandle(true, true);
        $.ajax({
        url: "{{ route('front-customer-save-form', ['id' => $model->id]) }}",
        method: 'post',
        /*  headers: {
            'x-csrf-token': $('meta[name="csrf-token"]').attr('content')
        }, */
        data: $(this).serialize(),
        responseHandle: function(data){
            buttonHandle(false, false);
            $("#admin-customer-form").html(data);
        },        
        success: function(data){
            this.responseHandle(data);
        },
        error: function (data){
           this.responseHandle(data);
        }
        }).done();
    });
});
    
</script>
@endsection