@extends('layouts.front-main-layout')

@section('meta')
    <meta name="description" content="{{$seoblock->description}}">
    <meta name="keywords" content="{{$seoblock->keywords}}">
@endsection

@section('style')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@endsection

@section('content')
<div class="container" style="background-color: #f7f6f6; margin-top: 50px;">
    <br/>
    <h3 class="text-center rus">Контакты</h3>
    <h3 class="text-center ua">Контакти</h3>
    <br/>
    <!-- main -->
    <div class="row">
        <div class="col-9">
        @foreach($model as $item)
        @if(!$item->enabled)
        @continue
        @endif
        <hr/>
        <br/>
            <div class="row">
                <div class="col-3">
                    <h4 class="rus">{{optional($item->_name)->string_rus}}</h4>
                    <h4 class="ua">{{optional($item->_name)->string_ua}}</h4>
                </div>
                <div class="col-3">
                    <img src="{{ asset((optional($item->_gallery)->item_url_1 != '')? $item->_gallery->item_url_1: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" />
                </div>
                <div class="col-6">
                    <div class="rus">{{optional($item-> _descriptionText)->text_rus}}</div>
                    <div class="ua">{{optional($item-> _descriptionText)->text_ua}}</div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <img src="{{ asset((optional($item->_gallery)->item_url_2 != '')? $item->_gallery->item_url_2: 'dist/img/logo.jpg') }}" class="img-thumbnail bd-placeholder-img card-img-top" alt="$altText" height="57" width="100%" />
                </div>
                <div class="col-6">
                    {!! $item->_address->text_rus !!}
                </div>
            </div>
            <br/>
            @endforeach
        </div>
        <div class="col-3">
            <div class="row">
                <div class="col">
                    <div class="d-flex align-items-center justify-content-center bg-info" style="height: 300px">
                        <span class="text-danger"> {{ __("Здесь могла бы быть ваша реклама") }}</span>
                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>            
@endsection
